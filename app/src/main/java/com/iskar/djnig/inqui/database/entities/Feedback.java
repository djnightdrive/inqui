package com.iskar.djnig.inqui.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by karasinboots on 14.11.2017.
 */
@Entity
public class Feedback {
    @PrimaryKey(autoGenerate = true)
    private
    int feedback_id;

    private String feedback_type;
    private String feedback_path;
    private String design_id;
    private String student_id;
    private String concept_id;
    private int feedback_sync_flag;

    public Feedback(String feedback_type, String feedback_path, String design_id, String student_id, String concept_id, int feedback_sync_flag) {
        this.feedback_type = feedback_type;
        this.feedback_path = feedback_path;
        this.design_id = design_id;
        this.student_id = student_id;
        this.concept_id = concept_id;
        this.feedback_sync_flag = feedback_sync_flag;
    }

    public int getFeedback_id() {

        return feedback_id;
    }

    public void setFeedback_id(int feedback_id) {
        this.feedback_id = feedback_id;
    }

    public String getFeedback_type() {
        return feedback_type;
    }

    public void setFeedback_type(String feedback_type) {
        this.feedback_type = feedback_type;
    }

    public String getFeedback_path() {
        return feedback_path;
    }

    public void setFeedback_path(String feedback_path) {
        this.feedback_path = feedback_path;
    }

    public String getDesign_id() {
        return design_id;
    }

    public void setDesign_id(String design_id) {
        this.design_id = design_id;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getConcept_id() {
        return concept_id;
    }

    public void setConcept_id(String concept_id) {
        this.concept_id = concept_id;
    }

    public int getFeedback_sync_flag() {
        return feedback_sync_flag;
    }

    public void setFeedback_sync_flag(int feedback_sync_flag) {
        this.feedback_sync_flag = feedback_sync_flag;
    }
}
