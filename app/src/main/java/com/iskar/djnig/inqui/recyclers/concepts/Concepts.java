package com.iskar.djnig.inqui.recyclers.concepts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karasinboots on 07.12.2017.
 */

public class Concepts {
    private String conceptName;
    private List<Concepts> conceptsList = new ArrayList<>();
    public ConceptsCompletion conceptsCompletion = null;

    public Concepts(String conceptName) {
        this.conceptName = conceptName;
    }

    public String getConceptName() {
        return conceptName;
    }

    public void setConceptName(String conceptName) {
        this.conceptName = conceptName;
    }

    public List<Concepts> getConceptsList() {
        return conceptsList;
    }

    public void setConceptsList(List<Concepts> conceptsList) {
        this.conceptsList = conceptsList;
    }

    public void addConcept(String conceptName) {
        conceptsList.add(new Concepts(conceptName));
    }
}
