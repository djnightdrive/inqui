package com.iskar.djnig.inqui.recyclers.design;

import java.util.ArrayList;

/**
 * Created by karasinboots on 25.11.2017.
 */

public interface DesignsCompletion {
    void processFinish(ArrayList<Designs> designsList);
}
