package com.iskar.djnig.inqui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;

/**
 * Created by karasinboots on 27.11.2017.
 */
public class MyProgressDialog {

    private String string;
    private AlertDialog dialog;
    private Activity activity;


    public MyProgressDialog(@NonNull Context context, String string) {
        this.string = string;
        activity = (Activity) context;
    }

    public void show() {
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.progress, null);

        dialog = new AlertDialog.Builder(activity)
                .setView(v)
                .setCancelable(false)
                .create();

        TextView textView = v.findViewById(R.id.text_progress);
        textView.setText(string);
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

}