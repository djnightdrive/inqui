package com.iskar.djnig.inqui.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.StaticConstants;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Feedback;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.FeedbackBody;
import com.iskar.djnig.inqui.model.Response;
import com.iskar.djnig.inqui.onlineStorage.GoogleDrive;
import com.iskar.djnig.inqui.onlineStorage.GoogleDriveAsyncResponse;
import com.iskar.djnig.inqui.utils.NetworkManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by djnig on 11/14/2017.
 */

public class FeedTextFragment extends Fragment implements GoogleDriveAsyncResponse {

    @BindView(R.id.edit_feed_txt)
    EditText feedText;


    @BindView(R.id.button_send_feed_txt)
    Button sendFeed;

    @BindView(R.id.button_cancel_text)
    Button cancel;



   // Dropbox dropbox;
    GoogleDrive googleDrive;
    MyProgressDialog progress;
    private String conceptId;
    private String studentId;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.feedback_text, container, false);
        ButterKnife.bind(this, v);
        initView();
        conceptId = getArguments().getString("conceptId");
        studentId = getArguments().getString("studentId");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });
        sharedPreferences = getContext().getSharedPreferences("network", Context.MODE_PRIVATE);
        return v;
    }


    String mFilePath;

    private void initView() {

        sendFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File newFile = new File(Environment.getExternalStorageDirectory()
                        + "/Inqui/Feedback/Text/" + "Text " + new Date().toString() + ".txt");
                File mkdir = new File(Environment.getExternalStorageDirectory()
                        + "/Inqui/Feedback/Text/");
                mkdir.mkdirs();
                mFilePath = newFile.getPath();
                FileOutputStream stream = null;
                try {
                    stream = new FileOutputStream(newFile);
                    newFile.getParentFile().mkdirs();
                    newFile.createNewFile();
                    stream.write(feedText.getText().toString().getBytes());
                    if (NetworkManager.isConnected(getActivity())) {
                        editor = sharedPreferences.edit();
                        editor.putBoolean("sync", false);
                        editor.apply();
                        /*dropbox = new Dropbox();
                        dropbox.delegate = FeedTextFragment.this;*/
                        googleDrive = new GoogleDrive();
                        googleDrive.delegate = FeedTextFragment.this;
                        showProgress();
                        initDropbox();
                        //dropbox.execute(newFile.getAbsolutePath(), StaticConstants.FEED_TXT);
                        googleDrive.execute(newFile.getAbsolutePath(), StaticConstants.FEED_TXT);
                    } else {
                        putToDb(new Feedback("audio", mFilePath, ""
                                , getArguments().getString("studentId"),
                                getArguments().getString("conceptId"), 0));
                        editor = sharedPreferences.edit();
                        editor.putBoolean("sync", true);
                        editor.apply();
                        showSorry("Connection problem! Feedback will be uploaded when Internet appear!");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        stream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void initDropbox() {
            //dropbox.init();
            googleDrive.initGoogleDrive(getContext());

    }

    private void showSorry(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Feedback uploading")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
    }

    @Override
    public void processFinish(String path) {
        if (path != null) {
            FeedbackBody body = new FeedbackBody();
            body.setFeedBackPath(path);
            body.setFeedBackType("text");
            body.setConceptId(conceptId);
            body.setDesignId("");
            body.setStudentId(studentId);
            submitFeed(body);
            progress.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Feedback uploading")
                    .setMessage("Feedback was successfully uploaded to Google Drive")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    HomePageFragment homePageFragment = new HomePageFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.root_for_fragments, homePageFragment);
                                    transaction.commit();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            editor = sharedPreferences.edit();
            editor.putBoolean("sync", false);
            editor.apply();
            deleteFile();
        } else {
            editor = sharedPreferences.edit();
            editor.putBoolean("sync", true);
            editor.apply();
        }
    }

    private void deleteFile() {
        File file = new File(mFilePath);
        file.delete();
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Uploading text feedback..");
        progress.show();
    }

    private void putToDb(final Feedback feedback) {
        new Thread(new Runnable() {
            public void run() {
                AppDatabase.getInstance(getContext()).getFeedbackDao().insertFeedback(feedback);
            }
        }).start();
    }

    private void submitFeed(FeedbackBody body) {
        HttpHelper.getApi().submitFeedback(body).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("FeedBack Successfully added via ... text mode")) {
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }

}
