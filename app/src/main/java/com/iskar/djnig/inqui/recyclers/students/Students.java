package com.iskar.djnig.inqui.recyclers.students;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class Students {
    private String studentName;

    private List<Students> studentsList = new ArrayList<>();

    public Students(String name) {
        this.studentName = name;
    }

    public Students() {
    }

    public String getStudentName() {
        return studentName;
    }

    public List<Students> getStudents() {
        return studentsList;
    }

    public void addStudent(String studentName) {
        studentsList.add(new Students(studentName));
    }

    public void removeLastItem() {
        studentsList.remove(studentsList.size() - 1);
    }
}
