
package com.iskar.djnig.inqui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedbackBody implements Parcelable
{

    @SerializedName("studentId")
    @Expose
    private String studentId;
    @SerializedName("designId")
    @Expose
    private String designId;
    @SerializedName("conceptId")
    @Expose
    private String conceptId;
    @SerializedName("feedBackType")
    @Expose
    private String feedBackType;
    @SerializedName("feedBackPath")
    @Expose
    private String feedBackPath;
    public final static Parcelable.Creator<FeedbackBody> CREATOR = new Creator<FeedbackBody>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FeedbackBody createFromParcel(Parcel in) {
            return new FeedbackBody(in);
        }

        public FeedbackBody[] newArray(int size) {
            return (new FeedbackBody[size]);
        }

    }
    ;

    protected FeedbackBody(Parcel in) {
        this.studentId = ((String) in.readValue((String.class.getClassLoader())));
        this.designId = ((String) in.readValue((String.class.getClassLoader())));
        this.conceptId = ((String) in.readValue((String.class.getClassLoader())));
        this.feedBackType = ((String) in.readValue((String.class.getClassLoader())));
        this.feedBackPath = ((String) in.readValue((String.class.getClassLoader())));
    }

    public FeedbackBody() {
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getDesignId() {
        return designId;
    }

    public void setDesignId(String designId) {
        this.designId = designId;
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }

    public String getFeedBackType() {
        return feedBackType;
    }

    public void setFeedBackType(String feedBackType) {
        this.feedBackType = feedBackType;
    }

    public String getFeedBackPath() {
        return feedBackPath;
    }

    public void setFeedBackPath(String feedBackPath) {
        this.feedBackPath = feedBackPath;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(studentId);
        dest.writeValue(designId);
        dest.writeValue(conceptId);
        dest.writeValue(feedBackType);
        dest.writeValue(feedBackPath);
    }

    public int describeContents() {
        return  0;
    }

}
