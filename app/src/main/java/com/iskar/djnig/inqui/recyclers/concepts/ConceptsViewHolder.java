package com.iskar.djnig.inqui.recyclers.concepts;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 07.12.2017.
 */

public class ConceptsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.name)
    TextView conceptName;

    public ConceptsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
