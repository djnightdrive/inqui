package com.iskar.djnig.inqui;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by karasinboots on 28.11.2017.
 */

public class MyApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
