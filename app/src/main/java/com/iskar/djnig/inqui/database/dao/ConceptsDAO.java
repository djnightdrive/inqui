package com.iskar.djnig.inqui.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.iskar.djnig.inqui.database.entities.Concepts;

import java.util.List;

/**
 * Created by karasinboots on 30.11.2017.
 */
@Dao
public interface ConceptsDAO {
    @Query("SELECT * FROM concepts")
    List<Concepts> getAll();

    @Query("DELETE FROM concepts")
    void clear();

    @Insert
    void insertConcept(Concepts concepts);

    @Update
    void updateConcept(Concepts concepts);

    @Delete
    void deleteConcept(Concepts concepts);
}