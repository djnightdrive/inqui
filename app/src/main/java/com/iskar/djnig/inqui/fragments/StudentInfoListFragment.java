package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Students;
import com.iskar.djnig.inqui.dialogs.ImageDialog;
import com.iskar.djnig.inqui.model.Student;
import com.iskar.djnig.inqui.recyclers.RecyclerViewClickListener;
import com.iskar.djnig.inqui.recyclers.design.Designs;
import com.iskar.djnig.inqui.recyclers.design.DesignsAdapter;
import com.iskar.djnig.inqui.utils.NetworkManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 28.11.2017.
 */

public class StudentInfoListFragment extends android.support.v4.app.Fragment implements RecyclerViewClickListener {
    @BindView(R.id.first_name)
    TextView firstName;
    @BindView(R.id.last_name)
    TextView lastName;
    @BindView(R.id.rollNumber)
    TextView rollNumber;
    @BindView(R.id.student_designs)
    RecyclerView studentDesigns;
    @BindView(R.id.button_cancel2)
    Button btnCancel;
    @BindView(R.id.schoolName_2)
    TextView schoolName;
    @BindView(R.id.className_2)
    TextView className;
    List<Designs> mDesigns = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    List<Student> mStudents = new ArrayList<>();
    String studentId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.student_info_list, container, false);
        ButterKnife.bind(this, v);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });
        studentId = getArguments().getString("studentid");
        Log.e("roll", getArguments().getString("studentid"));
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getStudents(getArguments().getString("classid"));
        getDesignsForStudent(getArguments().getString("classid"));
        linearLayoutManager = new LinearLayoutManager(getContext());
    }

    private void getDesignsForStudent(final String classId) {
        mDesigns.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Design> designs = AppDatabase.getInstance(getContext()).getDesignDao().getByClass(classId);
                Log.e("designs", String.valueOf(designs.size()));
                for (Design design : designs) {
                    List<String> rolls = design.getRollIds();
                    for (String roll : rolls) {
                        if (roll.equals(studentId)) {
                            Log.e("roll", studentId);
                            Log.e("roll", roll);
                            mDesigns.add(new Designs(design.getDesign_name(),
                                    design.getDesign_image(), design.getRollIds(), design.getApproved(),
                                    design.getDesign_id(), design.getComment()));
                        }
                    }
                }
                return mDesigns;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupRecycler((List<Designs>) o);
            }
        };
        asyncTask.execute();
    }

    private void setupRecycler(List<Designs> designs) {
        DesignsAdapter designsAdapter = new DesignsAdapter(designs, getContext(), this, 0, getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        studentDesigns.setLayoutManager(linearLayoutManager);
        studentDesigns.setAdapter(designsAdapter);
        designsAdapter.notifyDataSetChanged();

    }

    private void getStudents(final String classId) {
        mStudents.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Students> tempList = AppDatabase.getInstance(getContext()).getStudentsDao().getStudentByClass(classId);
                for (Students students : tempList) {
                    mStudents.add(new Student(students.getFirstName(), students.getLastName(),
                            students.getRollId(), students.getClassId(), students.getAdmissionNumber()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupText(mStudents);
            }
        };
        try {
            asyncTask.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    private void setupText(List<Student> students) {
        try {
            int id = getArguments().getInt("spinnerid");
            firstName.setText(students.get(id).getFirstName());
            lastName.setText(students.get(id).getLastName());
            rollNumber.setText(students.get(id).getRollNumber());
            schoolName.setText(getArguments().getString("schoolName"));
            className.setText(getArguments().getString("className"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void recyclerViewListClicked(View v, int position, String dropLink) {
        if (NetworkManager.isConnected(getContext())) {
            ImageDialog imageDialog = new ImageDialog();
            Bundle bundle = new Bundle();
            bundle.putString("view", dropLink);
            imageDialog.setArguments(bundle);
            imageDialog.show(getFragmentManager(), "image");
        }
    }
}

