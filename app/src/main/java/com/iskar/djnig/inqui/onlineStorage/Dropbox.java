package com.iskar.djnig.inqui.onlineStorage;

import android.os.AsyncTask;
import android.util.Log;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.iskar.djnig.inqui.StaticConstants.ACCESS_TOKEN;
import static com.iskar.djnig.inqui.StaticConstants.DESIGN;
import static com.iskar.djnig.inqui.StaticConstants.FEED_AUDIO;
import static com.iskar.djnig.inqui.StaticConstants.FEED_TXT;
import static com.iskar.djnig.inqui.StaticConstants.FEED_VIDEO;

/**
 * Created by karasinboots on 25.11.2017.
 */

public class Dropbox extends AsyncTask<String, Void, String> {
    private static DbxClientV2 client;
    public GoogleDriveAsyncResponse delegate = null;

    public Dropbox() {
    }


    public void init() throws DbxException {

        // Create Dropbox client
        DbxRequestConfig config = new DbxRequestConfig("Inqui", "en_US");
        if (client == null)
            client = new DbxClientV2(config, ACCESS_TOKEN);
        //Get files and folder metadata from Dropbox root directory
    }

    private String getFileName(String path) {
        if (null != path && path.length() > 0) {
            int endIndex = path.lastIndexOf("/");
            if (endIndex != -1) {
                return path.substring(endIndex + 1); // not forgot to put check if(endIndex != -1)
            }
        }
        return null;
    }


    @Override
    protected String doInBackground(String... strings) {
        switch (strings[1]) {

            case DESIGN:
                return uploadDesign(strings[0]);
            case FEED_TXT:
                return uploadTxt(strings[0]);
            case FEED_VIDEO:
                return uploadVideo(strings[0]);
            case FEED_AUDIO:
                return uploadAudio(strings[0]);
            default:
                return null;
        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    private String uploadAudio(String string) {
        try {  // Get files and folder metadata from Dropbox root directory
            InputStream in = new FileInputStream(new File(string));
            String path = "Design/" + getFileName(string);
            {
                FileMetadata metadata = client.files().uploadBuilder("/Feedback/Audio/" + getFileName(string))
                        .uploadAndFinish(in);
                Log.e("metadata", metadata.getName());
                return path;
            }
        } catch (DbxException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String uploadVideo(String string) {
        try {  // Get files and folder metadata from Dropbox root directory
            InputStream in = new FileInputStream(string);
            String path = "Feedback/Video/" + getFileName(string);
            {
                FileMetadata metadata = client.files().uploadBuilder("/Feedback/Video/" + getFileName(string))
                        .uploadAndFinish(in);
                Log.e("metadata", metadata.getName());
                return path;
            }
        } catch (DbxException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String uploadTxt(String string) {
        try {  // Get files and folder metadata from Dropbox root directory
            InputStream in = new FileInputStream(new File(string));
            String path = "Feedback/Text/" + getFileName(string);
            {
                FileMetadata metadata = client.files().uploadBuilder("/Feedback/Text/" + getFileName(string))
                        .uploadAndFinish(in);
                Log.e("metadata", metadata.getName());
                return path;
            }
        } catch (DbxException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String uploadDesign(String string) {
 /*       try {  // Get files and folder metadata from Dropbox root directory

            InputStream in = new FileInputStream(string.substring(7));
            String path = "Design/" + getFileName(string);
            {
                FileMetadata metadata = client.files().uploadBuilder("/Design/" + getFileName(string))
                        .uploadAndFinish(in);
                Log.e("metadata", metadata.getName());
                return path;
            }
        } catch (DbxException | IOException e) {
            e.printStackTrace();
            return null;
        }*/
       return null;
    }

    @Override
    protected void onPostExecute(String path) {
        delegate.processFinish(path);
    }
}

