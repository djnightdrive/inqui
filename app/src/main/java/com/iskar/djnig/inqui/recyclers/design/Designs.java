package com.iskar.djnig.inqui.recyclers.design;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Design;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class Designs implements Serializable {
    private String designName;
    private String designImage;
    private List<String> rolls;
    private String status;
    private List<Designs> designsList = new ArrayList<>();
    public DesignsCompletion designsCompletion = null;
    private String designComments;
    private String designId;
    private String dropboxLink;
    private boolean checkedStatus;
    private boolean reviewStatus;

    public Designs(String name, String uri, List<String> rolls, String status, String designId, String designComments) {
        this.designName = name;
        this.designImage = uri;
        this.rolls = rolls;
        this.status = status;
        this.designId = designId;
        this.designComments = designComments;
    }

    public Designs() {
    }

    public boolean getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(boolean reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public boolean isCheckedStatus() {
        return checkedStatus;
    }

    public void setCheckedStatus(boolean checkedStatus) {
        this.checkedStatus = checkedStatus;
    }

    public String getDropboxLink() {
        return dropboxLink;
    }

    public void setDropboxLink(String dropboxLink) {
        this.dropboxLink = dropboxLink;
    }

    public void addDesign(Designs design) {
        designsList.add(design);
    }

    public List<Designs> getDesignsList() {
        return designsList;
    }

    public void setDesignsList(List<Designs> designsList) {
        this.designsList = designsList;
    }

    public String getDesignComments() {
        return designComments;
    }

    public void setDesignComments(String designComments) {
        this.designComments = designComments;
    }

    public String getDesignId() {
        return designId;
    }

    public void setDesignId(String designId) {
        this.designId = designId;
    }

    public String getDesignName() {
        return designName;
    }

    public String getDesignImage() {
        return designImage;
    }

    public List<Designs> getDesigns() {
        return designsList;
    }

    public List<String> getRolls() {
        return rolls;
    }

    @SuppressLint("StaticFieldLeak")
    public void getDesignFromDb(final Context context) {
        designsList.clear();
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                designsCompletion.processFinish((ArrayList<Designs>) o);
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                for (Design design : AppDatabase.getInstance(context).getDesignDao().getAll()) {
                    designsList.add(new Designs(design.getDesign_name(),
                            design.getDesign_image(), design.getRollIds(), design.getApproved(),
                            design.getDesign_id(), design.getComment()));
                }
                return designsList;
            }
        };
        try {
            asyncTask.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SuppressLint("StaticFieldLeak")
    public void getDesignByStudent(final Context context, final String rollNumber) {
        //designsList.clear();

        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                designsCompletion.processFinish((ArrayList<Designs>) o);
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                for (Design design : AppDatabase.getInstance(context).getDesignDao().getByRoll(rollNumber)) {

                    designsList.add(new Designs(design.getDesign_name(),
                            design.getDesign_image(),
                            design.getRollIds(),
                            design.getApproved(),
                            design.getDesign_id(),
                            design.getComment()));
                }
                return designsList;
            }

        };
        asyncTask.execute();
    }

    public void clear() {
        designsList.clear();
    }
}
