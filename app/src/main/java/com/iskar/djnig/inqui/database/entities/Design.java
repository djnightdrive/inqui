package com.iskar.djnig.inqui.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.iskar.djnig.inqui.database.ListStringConverter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by karasinboots on 14.11.2017.
 */
@Entity
public class Design implements Serializable {


    @PrimaryKey(autoGenerate = true)
    private int id;

    private String design_name;
    private String design_image;
    private int design_sync_flag;
    private String design_id;
    private String conceptId;
    @TypeConverters({ListStringConverter.class})
    private List<String> rollIds;
    private String classId;
    private String schoolId;
    private String approved;
    private String comment;

    public Design(String design_name, String design_image, int design_sync_flag, String design_id,
                  String conceptId, List<String> rollIds, String classId, String schoolId, String approved, String comment) {
        this.design_name = design_name;
        this.design_image = design_image;
        this.design_sync_flag = design_sync_flag;
        this.design_id = design_id;
        this.conceptId = conceptId;
        this.rollIds = rollIds;
        this.classId = classId;
        this.schoolId = schoolId;
        this.approved = approved;
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getClassId() {
        return classId;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesign_id() {
        return design_id;
    }

    public void setDesign_id(String design_id) {
        this.design_id = design_id;
    }

    public String getDesign_name() {
        return design_name;
    }

    public void setDesign_name(String design_name) {
        this.design_name = design_name;
    }

    public String getDesign_image() {
        return design_image;
    }

    public void setDesign_image(String design_image) {
        this.design_image = design_image;
    }

    public int getDesign_sync_flag() {
        return design_sync_flag;
    }

    public void setDesign_sync_flag(int design_sync_flag) {
        this.design_sync_flag = design_sync_flag;
    }

    public List<String> getRollIds() {
        return rollIds;
    }

    public void setRollIds(List<String> rollIds) {
        this.rollIds = rollIds;
    }
}
