package com.iskar.djnig.inqui.data;

import java.util.ArrayList;

/**
 * Created by karasinboots on 23.11.2017.
 */

public class SchoolsData {
    ArrayList<SchoolsData> schoolsData = new ArrayList<>();

    private String schoolName;
    private String schoolId;
    private ArrayList<String> classes;
    private ArrayList<String> classesId;

    public SchoolsData(String schoolName, String schoolId, ArrayList<String> classes, ArrayList<String> classesId) {
        this.schoolName = schoolName;
        this.schoolId = schoolId;
        this.classes = classes;
        this.classesId = classesId;
    }

    public SchoolsData() {
    }

    public void clear() {
        schoolsData.clear();
    }

    public void addSchoolsData(SchoolsData schoolsData) {
        this.schoolsData.add(schoolsData);
    }

    public ArrayList<SchoolsData> getSchools() {
        return schoolsData;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public ArrayList<String> getClasses() {
        return classes;
    }

    public ArrayList<String> getClassesId() {
        return classesId;
    }
}
