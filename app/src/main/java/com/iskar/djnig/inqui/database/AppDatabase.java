package com.iskar.djnig.inqui.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.iskar.djnig.inqui.database.dao.ClassesDAO;
import com.iskar.djnig.inqui.database.dao.ConceptsDAO;
import com.iskar.djnig.inqui.database.dao.DesignDAO;
import com.iskar.djnig.inqui.database.dao.FeedbackDAO;
import com.iskar.djnig.inqui.database.dao.LinksDAO;
import com.iskar.djnig.inqui.database.dao.SchoolsDAO;
import com.iskar.djnig.inqui.database.dao.StudentsDAO;
import com.iskar.djnig.inqui.database.dao.UserDAO;
import com.iskar.djnig.inqui.database.entities.Classes;
import com.iskar.djnig.inqui.database.entities.Concepts;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Feedback;
import com.iskar.djnig.inqui.database.entities.Links;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.database.entities.Students;
import com.iskar.djnig.inqui.database.entities.User;

/**
 * Created by karasinboots on 14.11.2017.
 */

@Database(entities = {Design.class, Classes.class, Feedback.class, User.class, Schools.class, Students.class, Concepts.class, Links.class}, version = 17)
public abstract class AppDatabase extends RoomDatabase {

    private static volatile AppDatabase instance;

    public abstract UserDAO getUserDao();

    public abstract DesignDAO getDesignDao();

    public abstract LinksDAO getLinksDao();

    public abstract ClassesDAO getClassesDao();

    public abstract ConceptsDAO getConceptsDao();

    public abstract FeedbackDAO getFeedbackDao();

    public abstract SchoolsDAO getSchoolsDao();

    public abstract StudentsDAO getStudentsDao();

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context) {

        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                "app_db").fallbackToDestructiveMigration().
        allowMainThreadQueries().build();
    }
}