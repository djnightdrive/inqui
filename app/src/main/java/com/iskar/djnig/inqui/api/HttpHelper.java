package com.iskar.djnig.inqui.api;

import android.content.Context;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by djnig on 11/27/2017.
 */

public class HttpHelper {

    //private static String token = "46798286-2ae2-4329-8491-942b9a4c1629";
    private static String token;
    private static String contentType = "application/json";
    private static Context context;

    public static OkHttpClient client() {
        System.out.println("TOKEN: " + token);
        return new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Content-Type", contentType)
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).connectTimeout(1000, TimeUnit.MILLISECONDS)
                .readTimeout(1000, TimeUnit.MILLISECONDS).build();
    }

    public static Service getApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .client(HttpHelper.client())
                .baseUrl(Service.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(Service.class);
    }

    public static void setToken(String token) {
        System.out.println("Set token: " + token);
        HttpHelper.token = token;
    }

    public static void setContext(Context context) {
        HttpHelper.context = context;
    }
}
