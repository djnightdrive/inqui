package com.iskar.djnig.inqui.recyclers.materials;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.model.Material;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class MaterialsAdapter extends RecyclerView.Adapter<MaterialsViewHolder> {
    private List<Materials> materialsList;
    private List<Material> matNames;
    private Context mContext;

    public MaterialsAdapter(List<Materials> materialsList, Context context, List<Material> existMaterials) {
        this.materialsList = materialsList;
        this.mContext = context;
        this.matNames = existMaterials;
    }

    @Override
    public MaterialsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.materials_item, parent, false);
        return new MaterialsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MaterialsViewHolder holder, final int position) {
        Materials item = materialsList.get(position);
        holder.materialCount.setTag(position);
        holder.materialName.setTag(position);
        List<String> names = new ArrayList<>();
        for (Material material : matNames) {
            names.add(material.getMaterialName());
        }
        holder.materialName.setAdapter(new SpinnerAdapter(mContext, R.layout.spinner_item,
                names.toArray(new String[names.size()])));
        holder.materialName.setSelection(item.getPosition());
        holder.materialName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position1, long id) {
                materialsList.set((int) holder.materialName.getTag(), new Materials(holder.materialName.getSelectedItem().toString(),
                        Integer.parseInt(holder.materialCount.getSelectedItem().toString()), position1, holder.materialCount.getSelectedItemPosition()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.materialCount.setAdapter(new SpinnerAdapter(mContext, R.layout.spinner_item,
                mContext.getResources().getStringArray(R.array.spinner_items)));
        holder.materialCount.setSelection(item.getQuantityPosition());
        holder.materialCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position1, long id) {
                materialsList.set((int) holder.materialCount.getTag(), new Materials(holder.materialName.getSelectedItem().toString(),
                        Integer.parseInt(holder.materialCount.getSelectedItem().toString()), holder.materialName.getSelectedItemPosition(), position1));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.removeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialsList.remove(position);
                notifyDataSetChanged();
                //matNames.remove(position);
                //notifyItemRemoved(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return materialsList.size();
    }
}
