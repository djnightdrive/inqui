package com.iskar.djnig.inqui.recyclers.students;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class StudentsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.student_first_and_last)
    TextView studentFirstAndLast;


    @BindView(R.id.student_roll)
    TextView studentRoll;

    public StudentsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
