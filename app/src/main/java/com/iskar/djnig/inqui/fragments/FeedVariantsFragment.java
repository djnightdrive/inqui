package com.iskar.djnig.inqui.fragments;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.utils.FragmentUtil;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by djnig on 11/14/2017.
 */

public class FeedVariantsFragment extends Fragment {

    Bundle mBundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.feed_variants, container, false);
        ButterKnife.bind(this, v);
        requestCameraPermission();
        mBundle = new Bundle();
        mBundle.putString("conceptId", getArguments().getString("conceptId"));
        mBundle.putString("studentId", getArguments().getString("studentId"));
        return v;
    }

    @OnClick(R.id.button_feed_audio)

    public void feedAudio() {
        FeedAudioFragment fr = new FeedAudioFragment();
        fr.setArguments(mBundle);
        FragmentUtil.changeFragment(fr, this);
    }

    @OnClick(R.id.button_feed_text)
    public void feedText() {
        FeedTextFragment fr = new FeedTextFragment();
        fr.setArguments(mBundle);
        FragmentUtil.changeFragment(fr, this);
    }

    @OnClick(R.id.button_feed_video)
    public void feedVideo() {
        if (permissionGranted()) {
            FeedVideoCaptureFragment fr = new FeedVideoCaptureFragment();
            fr.setArguments(mBundle);
            FragmentUtil.changeFragment(fr, this);
        } else requestCameraPermission();
    }

    private void requestCameraPermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(getActivity(), perms))
            EasyPermissions.requestPermissions(this, "Permission needed to perform video capture!", 1, perms);
    }

    private boolean permissionGranted() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        return EasyPermissions.hasPermissions(getActivity(), perms);
    }
}
