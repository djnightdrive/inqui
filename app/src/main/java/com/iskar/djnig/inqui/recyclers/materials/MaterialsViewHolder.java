package com.iskar.djnig.inqui.recyclers.materials;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.iskar.djnig.inqui.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class MaterialsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.spinner_materials)
    Spinner materialName;
    @BindView(R.id.material_count)
    Spinner materialCount;
    @BindView(R.id.button_remove_material_item)
    ImageButton removeItem;

    public MaterialsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
