package com.iskar.djnig.inqui.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by karasinboots on 12.12.2017.
 */
@Entity
public class Links {
    @PrimaryKey(autoGenerate = true)
    private
    int id;

    private String designId;
    private String dropLink;

    public Links(String designId, String dropLink) {
        this.designId = designId;
        this.dropLink = dropLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesignId() {
        return designId;
    }

    public void setDesignId(String designId) {
        this.designId = designId;
    }

    public String getDropLink() {
        return dropLink;
    }

    public void setDropLink(String dropLink) {
        this.dropLink = dropLink;
    }
}
