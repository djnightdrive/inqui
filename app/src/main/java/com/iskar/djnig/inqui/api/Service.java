package com.iskar.djnig.inqui.api;

import com.iskar.djnig.inqui.model.Approvation;
import com.iskar.djnig.inqui.model.Concept;
import com.iskar.djnig.inqui.model.DesignModel;
import com.iskar.djnig.inqui.model.FeedbackBody;
import com.iskar.djnig.inqui.model.FeedbackResponse;
import com.iskar.djnig.inqui.model.Material;
import com.iskar.djnig.inqui.model.MaterialBody;
import com.iskar.djnig.inqui.model.Response;
import com.iskar.djnig.inqui.model.School;
import com.iskar.djnig.inqui.model.Student;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by djnig on 11/27/2017.
 */

public interface Service {

    String BASE_URL = "http://default-environment.2wqimehrvh.ap-south-1.elasticbeanstalk.com/";

    @GET("students/{classId}")
    Call<List<Student>> getStudents(@Path("classId") String classId);

    @GET("designs/all/{classId}")
    Call<List<DesignModel>> getDesigns(@Path("classId") String classId);

    @GET("schools/all")
    Call<List<School>> getSchools();

    @GET("concepts/all")
    Call<List<Concept>> getConcepts();

    @PUT("design/submit")
    Call<Response> submitDesign(@Body List<DesignModel> design);

    @PUT("design/acknowledgedesign")
    Call<Response> submitApprovation(@Body List<Approvation> approvations);

    @PUT("feedback/submit")
    Call<Response> submitFeedback(@Body FeedbackBody body);

    @GET("feedback/all")
    Call<List<FeedbackResponse>> getAllFeedbacks();

    @GET("feedback/{designId}")
    Call<List<FeedbackResponse>> getFeedbacksByDesignId(@Path("designId") String designId);

    @GET("concepts/{conceptId}/materials")
    Call<List<Material>> getMaterialsByConcept(@Path("conceptId") String conceptId);

    @PUT("concepts/{conceptId}/materials")
    Call<Response> putMaterials(@Path("conceptId") String conceptId, @Body MaterialBody body);

}
