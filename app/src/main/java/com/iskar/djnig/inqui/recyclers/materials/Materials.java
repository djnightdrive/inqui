package com.iskar.djnig.inqui.recyclers.materials;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class Materials {
    private String materialName;
    private int materialQuantity;
    private int position;
    private int quantityPosition;
    private List<Materials> materialsList = new ArrayList<>();

    public Materials(String name, int quantity, int position, int quantityPosition) {
        this.materialName = name;
        this.materialQuantity = quantity;
        this.position = position;
        this.quantityPosition = quantityPosition;
    }

    public Materials() {
    }

    public int getPosition() {
        return position;
    }

    public int getQuantityPosition() {
        return quantityPosition;
    }

    public void setQuantityPosition(int quantityPosition) {
        this.quantityPosition = quantityPosition;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getMaterialName() {
        return materialName;
    }

    public int getMaterialQuantity() {
        return materialQuantity;
    }

    public List<Materials> getMaterials() {
        return materialsList;
    }

    public void addMaterial(String materialName, int materialQuantity, int position, int quantityPosition) {
        materialsList.add(new Materials(materialName, materialQuantity, position, quantityPosition));
    }

    public void removeLastItem() {
        materialsList.remove(materialsList.size() - 1);
    }
}
