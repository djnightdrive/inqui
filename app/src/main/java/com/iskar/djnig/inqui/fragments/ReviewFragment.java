package com.iskar.djnig.inqui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Approvation;
import com.iskar.djnig.inqui.model.Response;
import com.iskar.djnig.inqui.recyclers.RecyclerViewClickListener;
import com.iskar.djnig.inqui.recyclers.design.Designs;
import com.iskar.djnig.inqui.recyclers.design.DesignsAdapter;
import com.iskar.djnig.inqui.utils.NetworkManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by karasinboots on 13.12.2017.
 */

public class ReviewFragment extends Fragment implements RecyclerViewClickListener {
    @BindView(R.id.designs_recycler)
    RecyclerView designRecycler;
    @BindView(R.id.button_cancel)
    Button cancel;
    @BindView(R.id.reviewSelected)
    Button review;
    DesignsAdapter designsAdapter;
    MyProgressDialog progress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.review_fragment, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupRecycler((Designs) getArguments().getSerializable("mDesigns"));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Designs> list = designsAdapter.getDesignsList();
                List<Approvation> approvations = new ArrayList<>();
                for (Designs designs : list) {
                   // if (designs.getReviewStatus()) {
                    if (designs.getStatus().equals("Approve")) {
                        Approvation approvation = new Approvation();
                        approvation.setInstructorAction("Approve");
                        designs.setStatus("Approve");
                        Log.e("status", designs.getStatus());
                        approvation.setDesignId(designs.getDesignId());
                        Log.e("id", designs.getDesignId());
                        if (designs.getDesignComments() != null) {
                            approvation.setDesignComments(designs.getDesignComments());
                            Log.e("comment", designs.getDesignComments() + "");
                        } else {
                            approvation.setDesignComments("Approved");
                            designs.setDesignComments("Approved");
                            Log.e("comment", designs.getDesignComments() + "");
                        }
                        approvations.add(approvation);
                        updateStatus(designs);
                    //} else {
                    } else if(designs.getStatus().equals("Rejected")){
                        Approvation approvation = new Approvation();
                        approvation.setInstructorAction("Reject");
                        designs.setStatus("Reject");
                        Log.e("status", designs.getStatus());
                        approvation.setDesignId(designs.getDesignId());
                        Log.e("id", designs.getDesignId());
                        if (designs.getDesignComments() != null) {
                            approvation.setDesignComments(designs.getDesignComments());
                            Log.e("comment", designs.getDesignComments() + "");
                        } else {
                            approvation.setDesignComments("Rejected");
                            designs.setDesignComments("Rejected");
                            Log.e("comment", designs.getDesignComments() + "");
                        }
                        approvations.add(approvation);
                        updateStatus(designs);
                    }
                    else {
                        Approvation approvation = new Approvation();
                        approvation.setInstructorAction("In review");
                        designs.setStatus("In review");
                        Log.e("status", designs.getStatus());
                        approvation.setDesignId(designs.getDesignId());
                        Log.e("id", designs.getDesignId());
                        if (designs.getDesignComments() != null) {
                            approvation.setDesignComments(designs.getDesignComments());
                            Log.e("comment", designs.getDesignComments() + "");
                        } else {
                            approvation.setDesignComments("In review");
                            designs.setDesignComments("In review");
                            Log.e("comment", designs.getDesignComments() + "");
                        }
                        approvations.add(approvation);
                        updateStatus(designs);
                    }
                }
                submitMultiReview(approvations);
            }
        });
    }

    private void submitMultiReview(List<Approvation> approvations) {
        if (NetworkManager.isConnected(getContext())) {
            showProgress();
            HttpHelper.getApi().submitApprovation(approvations).enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    if (response.isSuccessful()) {
                        if ((response.body()).getMessage().contains("acknowledged")) {
                            progress.dismiss();
                            Log.e("message_approvation", response.body().getMessage());
                            showDialog("Concepts successfully acknowledged!");
                            int count = getFragmentManager().getBackStackEntryCount();
                            if (count > 0)
                                getFragmentManager().popBackStack();
                        }
                    }
                }

                @Override
                public void onFailure(Call<com.iskar.djnig.inqui.model.Response> call, Throwable t) {
                    progress.dismiss();
                }
            });
        } else {
            showDialog("Can't submit without internet!");
        }
    }


    private void updateStatus(final Designs design) {
        new Thread(new Runnable() {
            public void run() {
                Design design1 = AppDatabase.getInstance(getContext()).getDesignDao().getById(design.getDesignId());
                design1.setApproved(design.getStatus());
                design1.setComment(design.getDesignComments());
                AppDatabase.getInstance(getContext()).getDesignDao().update(design1);
            }
        }).start();
    }

    public void setupRecycler(Designs designs) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        designsAdapter = new DesignsAdapter(designs.getDesigns(), getContext(), this, 2, this.getActivity());
        designRecycler.setLayoutManager(linearLayoutManager);
        designRecycler.setAdapter(designsAdapter);
        designsAdapter.notifyDataSetChanged();
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Uploading design..");
        progress.show();
    }

    @Override
    public void recyclerViewListClicked(View v, int position, String string) {

    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Design approvation")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
    }
}
