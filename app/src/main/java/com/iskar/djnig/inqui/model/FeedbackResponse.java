
package com.iskar.djnig.inqui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedbackResponse implements Parcelable
{

    @SerializedName("designId")
    @Expose
    private String designId;
    @SerializedName("feedBackType")
    @Expose
    private String feedBackType;
    @SerializedName("instructorId")
    @Expose
    private String instructorId;
    @SerializedName("conceptId")
    @Expose
    private String conceptId;
    @SerializedName("studentId")
    @Expose
    private String studentId;
    @SerializedName("feedBackPath")
    @Expose
    private String feedBackPath;
    public final static Parcelable.Creator<FeedbackResponse> CREATOR = new Creator<FeedbackResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FeedbackResponse createFromParcel(Parcel in) {
            return new FeedbackResponse(in);
        }

        public FeedbackResponse[] newArray(int size) {
            return (new FeedbackResponse[size]);
        }

    }
    ;

    protected FeedbackResponse(Parcel in) {
        this.designId = ((String) in.readValue((String.class.getClassLoader())));
        this.feedBackType = ((String) in.readValue((String.class.getClassLoader())));
        this.instructorId = ((String) in.readValue((String.class.getClassLoader())));
        this.conceptId = ((String) in.readValue((String.class.getClassLoader())));
        this.studentId = ((String) in.readValue((String.class.getClassLoader())));
        this.feedBackPath = ((String) in.readValue((String.class.getClassLoader())));
    }

    public FeedbackResponse() {
    }

    public String getDesignId() {
        return designId;
    }

    public void setDesignId(String designId) {
        this.designId = designId;
    }

    public String getFeedBackType() {
        return feedBackType;
    }

    public void setFeedBackType(String feedBackType) {
        this.feedBackType = feedBackType;
    }

    public String getInstructorId() {
        return instructorId;
    }

    public void setInstructorId(String instructorId) {
        this.instructorId = instructorId;
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getFeedBackPath() {
        return feedBackPath;
    }

    public void setFeedBackPath(String feedBackPath) {
        this.feedBackPath = feedBackPath;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(designId);
        dest.writeValue(feedBackType);
        dest.writeValue(instructorId);
        dest.writeValue(conceptId);
        dest.writeValue(studentId);
        dest.writeValue(feedBackPath);
    }

    public int describeContents() {
        return  0;
    }

}
