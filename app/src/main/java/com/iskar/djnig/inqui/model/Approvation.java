package com.iskar.djnig.inqui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by karasinboots on 10.12.2017.
 */

public class Approvation implements Parcelable {

    @SerializedName("instructorAction")
    @Expose
    private String instructorAction;

    @SerializedName("designComments")
    @Expose
    private String designComments;

    @SerializedName("designID")
    @Expose
    private String designId;
    public final static Parcelable.Creator<Approvation> CREATOR = new Creator<Approvation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Approvation createFromParcel(Parcel in) {
            return new Approvation(in);
        }

        public Approvation[] newArray(int size) {
            return (new Approvation[size]);
        }

    };

    private Approvation(Parcel in) {
        this.designComments = ((String) in.readValue((String.class.getClassLoader())));
        this.designId = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Approvation() {
    }

    public String getInstructorAction() {
        return instructorAction;
    }

    public void setInstructorAction(String instructorAction) {
        this.instructorAction = instructorAction;
    }

    public String getDesignComments() {
        return designComments;
    }

    public void setDesignComments(String designComments) {
        this.designComments = designComments;
    }

    public String getDesignId() {
        return designId;
    }

    public void setDesignId(String designId) {
        this.designId = designId;
    }

    public static Creator<Approvation> getCREATOR() {
        return CREATOR;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(instructorAction);
        dest.writeValue(designComments);
        dest.writeValue(designId);
    }

    public int describeContents() {
        return 0;
    }

}
