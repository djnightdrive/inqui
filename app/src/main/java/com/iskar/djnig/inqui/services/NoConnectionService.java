package com.iskar.djnig.inqui.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.StaticConstants;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Feedback;
import com.iskar.djnig.inqui.model.DesignModel;
import com.iskar.djnig.inqui.model.FeedbackBody;
import com.iskar.djnig.inqui.model.Response;
import com.iskar.djnig.inqui.onlineStorage.GoogleDrive;
import com.iskar.djnig.inqui.onlineStorage.GoogleDriveAsyncResponse;
import com.iskar.djnig.inqui.utils.NetworkManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class NoConnectionService extends Service

{
    public static final String DESIGN = "design";
    Context mContext;
    private static BroadcastReceiver mConnectionReceiver;
    boolean registered = false;
    Upload upload;
    static SharedPreferences sharedPreferences;
    static SharedPreferences.Editor editor;
    private static boolean firstConnect = true;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        mContext = getApplicationContext();
        sharedPreferences = mContext.getSharedPreferences("network", MODE_PRIVATE);
        registerConnectionReceiver();
        return START_REDELIVER_INTENT;
    }

    private void registerConnectionReceiver() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mConnectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetInfo != null) {
                    if (firstConnect) {
                        if (NetworkManager.isConnected(mContext)) {
                            Log.i("APP_TAG", "Mobile - CONNECTED");
                            upload = new Upload(getApplicationContext());
                            initDropbox();
                            upload.execute();
                        }
                        firstConnect = false;
                    }
                } else {
                    firstConnect = true;
                }
            }
        };

        if (!registered) {
            registerReceiver(mConnectionReceiver, filter);
            registered = true;
        }
    }

    private void initDropbox() {
        try {
            upload.init();
        } catch (DbxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (registered) {
            try {
                unregisterReceiver(mConnectionReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
            registered = false;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void notifySuccess() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "my_channel_01";// The id of the channel.
            CharSequence name = "Inqui";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            Notification.Builder builder = new Notification.Builder(this, "")
                    .setContentTitle("Inqui")
                    .setContentText("Uploading successful")
                    .setChannelId("my_channel_01")
                    .setSmallIcon(R.drawable.ic_upload);
            Notification notification = builder.build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            notificationManager.notify(1, notification);
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Uploading successful")
                    .setSmallIcon(R.drawable.ic_upload)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            Notification notification = builder.build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(1, notification);
        }
    }

    public class Upload extends AsyncTask<String, Void, String> {
        private Context mContext;

        public Upload(Context context) {
            mContext = context;
        }

        private DbxClientV2 client;
        public GoogleDriveAsyncResponse delegate = null;
        public GoogleDrive googleDrive;

        public void init() throws DbxException {
            // Create Dropbox client
            DbxRequestConfig config = new DbxRequestConfig("Inqui", "en_US");
            client = new DbxClientV2(config, StaticConstants.ACCESS_TOKEN);
            googleDrive = new GoogleDrive();
            googleDrive.initGoogleDrive(getApplicationContext());
        }

        private String getFileName(String path) {
            if (null != path && path.length() > 0) {
                int endIndex = path.lastIndexOf("/");
                if (endIndex != -1) {
                    return path.substring(endIndex + 1);
                }
            }
            return null;
        }

        private void deleteFiles(String path) {
            File file = new File(path);
            file.delete();
        }

        @Override
        protected String doInBackground(String... strings) {
            if (sharedPreferences.getBoolean("sync", false)) {
                getUnsyncedDesigns();
                uploadFeedback();
            }
            return null;
        }


        private void getUnsyncedDesigns() {
            List<Design> unsynced = AppDatabase.getInstance(mContext).getDesignDao().getUnsynced();
            Log.e("designs", String.valueOf(unsynced.size()));
            List<DesignModel> designModels = new ArrayList<>();
            for (Design design : unsynced) {
                DesignModel designModel = new DesignModel();
                designModel.setClassId(design.getClassId());
                designModel.setConceptId(design.getConceptId());
                designModel.setDesignName(design.getDesign_name());
                designModel.setFilePath(design.getDesign_image());
                Log.e("feedback path", design.getDesign_image());
                designModel.setStudentList(design.getRollIds());
                designModel.setSchoolId(design.getSchoolId());
                designModels.add(designModel);
                uploadDesign(design.getDesign_image());
            }
            submitDesign(designModels, unsynced);
        }

        private void uploadFeedback() {
            List<Feedback> unsynced = AppDatabase.getInstance(mContext).getFeedbackDao().getUnsynced();
            for (Feedback feedback : unsynced) {
                try {
                    InputStream in = new FileInputStream(feedback.getFeedback_path());
                    {
                        FileMetadata metadata = client.files().uploadBuilder("/Feedback/" + feedback.getFeedback_type() + "/" + getFileName(feedback.getFeedback_path()))
                                .uploadAndFinish(in);
                        if(feedback.getFeedback_path().equals("audio"))
                            googleDrive.execute(feedback.getFeedback_path(), StaticConstants.FEED_AUDIO_AFTEROFFLINE);
                        else if(feedback.getFeedback_path().equals("video")) {
                            googleDrive.execute(feedback.getFeedback_path(), StaticConstants.FEED_VIDEO_AFTEROFFLINE);
                        } else if(feedback.getFeedback_path().equals("text")) {
                            googleDrive.execute(feedback.getFeedback_path(), StaticConstants.FEED_TXT_AFTEROFFLINE);
                        }
                        //deleteFiles(feedback.getFeedback_path());
                        submitFeedback(feedback);
                        Log.e("metadata", metadata.getName());
                    }
                } catch (DbxException | IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void uploadDesign(String string) {
            try {
                InputStream in = new FileInputStream(Environment.getExternalStorageDirectory() + "/Inqui/" + string);
                {
                    /*FileMetadata metadata = client.files().uploadBuilder("/Design/" + getFileName(string))
                            .uploadAndFinish(in);
                    Log.e("metadata", metadata.getName());*/
                    googleDrive.execute(Environment.getExternalStorageDirectory() + "/Inqui/" + string, StaticConstants.DESIGN_SERVICE);
                    //deleteFiles(Environment.getExternalStorageDirectory() + "/Inqui/" + string);
                }
            } catch (/*DbxException |*/ IOException e) {
                e.printStackTrace();
            }
        }

        private void updateUnsyncedDesign(final Design design) {
            new Thread(new Runnable() {
                public void run() {
                    AppDatabase.getInstance(mContext).getDesignDao().delete(design);
                    if (AppDatabase.getInstance(mContext).getDesignDao().getById(design.getDesign_id()) == null) {
                        Log.e("deleted", design.getDesign_name());
                    }
                    editor = sharedPreferences.edit();
                    editor.putBoolean("sync", false);
                    editor.apply();
                }
            }).start();

        }

        private void updateUnsyncedFeedback(final Feedback feedback) {
            new Thread(new Runnable() {
                public void run() {
                    AppDatabase.getInstance(mContext).getFeedbackDao().deleteFeedback(feedback);
                    editor = sharedPreferences.edit();
                    editor.putBoolean("sync", false);
                    editor.apply();
                }
            }).start();

        }

        public void submitFeedback(final Feedback feedback) {
            FeedbackBody body = new FeedbackBody();
            body.setStudentId(feedback.getStudent_id());
            body.setFeedBackType(feedback.getFeedback_type());
            body.setFeedBackPath(feedback.getFeedback_path());
            Log.e("feedback path", feedback.getFeedback_path());
            body.setDesignId("");
            body.setConceptId(feedback.getConcept_id());
            HttpHelper.getApi().submitFeedback(body).enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    if (response.isSuccessful()) {
                        if ((response.body()).getMessage().contains("Successfully")) {
                            updateUnsyncedFeedback(feedback);
                        }
                        notifySuccess();
                    }
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {

                }
            });
        }


        private void submitDesign(final List<DesignModel> designModels, final List<Design> designs) {
            HttpHelper.getApi().submitDesign(designModels).enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    if (response.isSuccessful()) {
                        if ((response.body()).getMessage().contains("Successfully")) {
                            for (Design design : designs) {
                                Log.e("submitting from service", design.getDesign_name());
                                updateUnsyncedDesign(design);

                            }
                            notifySuccess();
                        }
                    }
                }

                @Override
                public void onFailure(Call<com.iskar.djnig.inqui.model.Response> call, Throwable t) {
                }
            });
        }


        @Override
        protected void onPostExecute(String path) {

        }
    }

}