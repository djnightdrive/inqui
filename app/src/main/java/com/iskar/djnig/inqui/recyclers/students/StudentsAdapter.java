package com.iskar.djnig.inqui.recyclers.students;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.model.Student;

import java.util.List;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class StudentsAdapter extends RecyclerView.Adapter<StudentsViewHolder> {
    private List<Student> studentsList;

    public StudentsAdapter(List<Student> studentsList) {
        this.studentsList = studentsList;
    }

    @Override
    public StudentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.class_list_item, parent, false);
        return new StudentsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final StudentsViewHolder holder, final int position) {
        Student item = studentsList.get(position);
        holder.studentFirstAndLast.setText(item.getFirstName()+" "+item.getLastName());
        holder.studentRoll.setText("Roll: "+item.getRollNumber());
    }

    @Override
    public int getItemCount() {
        return studentsList.size();
    }
}
