
package com.iskar.djnig.inqui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Material implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("materialName")
    @Expose
    private String materialName;
    public final static Creator<Material> CREATOR = new Creator<Material>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Material createFromParcel(Parcel in) {
            return new Material(in);
        }

        public Material[] newArray(int size) {
            return (new Material[size]);
        }

    }
    ;

    protected Material(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.materialName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Material() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(materialName);
    }

    public int describeContents() {
        return  0;
    }

}
