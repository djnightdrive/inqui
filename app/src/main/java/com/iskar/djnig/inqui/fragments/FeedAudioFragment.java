package com.iskar.djnig.inqui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.StaticConstants;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Feedback;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.FeedbackBody;
import com.iskar.djnig.inqui.model.Response;
import com.iskar.djnig.inqui.onlineStorage.GoogleDrive;
import com.iskar.djnig.inqui.onlineStorage.GoogleDriveAsyncResponse;
import com.iskar.djnig.inqui.utils.NetworkManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import br.com.safety.audio_recorder.AudioListener;
import br.com.safety.audio_recorder.AudioRecordButton;
import br.com.safety.audio_recorder.AudioRecording;
import br.com.safety.audio_recorder.RecordingItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by djnig on 11/14/2017.
 */

public class FeedAudioFragment extends Fragment implements GoogleDriveAsyncResponse {

    @BindView(R.id.record_button)
    AudioRecordButton mRecButton;

    @BindView(R.id.listener_text)
    View mListenerText;

    @BindView(R.id.button_cancel_audio)
    Button cancel;

    @BindView(R.id.button_send_audio)
    Button mButtonSend;

    private RecordingItem mRecord;

    private String filePath;

    //Dropbox dropbox;
    GoogleDrive googleDrive;
    MyProgressDialog progress;

    private String conceptId;
    private String studentId;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.feedback_audio, container, false);
        ButterKnife.bind(this, v);

        conceptId = getArguments().getString("conceptId");
        studentId = getArguments().getString("studentId");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });
        sharedPreferences = getContext().getSharedPreferences("network", Context.MODE_PRIVATE);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        requestPermissionRecord();
        setupViews();
    }

    private void setupViews() {

        mListenerText.setVisibility(View.INVISIBLE);
        mButtonSend.setActivated(false);
        mRecButton.setOnAudioListener(new AudioListener() {
            @Override
            public void onStop(RecordingItem recordingItem) {
                mRecord = recordingItem;
                mListenerText.setVisibility(View.VISIBLE);
                mButtonSend.setActivated(true);

            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(), "Recording cancelled..", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }
        });

        mListenerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mRecord != null) {
                    new AudioRecording(getActivity()).play(mRecord);
                }

            }
        });

    }

    @OnClick(R.id.button_send_audio)
    void sendAudio() {

        if (mRecord != null && mListenerText.getVisibility() == View.VISIBLE) {
            send();
        } else {
            Toast.makeText(getActivity(), "You did not record audio", Toast.LENGTH_SHORT).show();
        }

    }

    private void send() {
        if (moveAudio(mRecord)) {
            if (!NetworkManager.isConnected(getActivity())) {
                putToDb(new Feedback("audio", filePath, ""
                        , getArguments().getString("studentId"),
                        getArguments().getString("conceptId"), 0));
                editor = sharedPreferences.edit();
                editor.putBoolean("sync", true);
                editor.apply();
                showSorry("Connection problem! Feedback will be uploaded when Internet appear!");
            } else {
                editor = sharedPreferences.edit();
                editor.putBoolean("sync", false);
                editor.apply();
                //dropbox = new Dropbox();
                //dropbox.delegate = FeedAudioFragment.this;
                googleDrive = new GoogleDrive();
                googleDrive.delegate = FeedAudioFragment.this;
                initDropbox();
                showProgress();
                //dropbox.execute(filePath, StaticConstants.FEED_AUDIO);
                googleDrive.execute(filePath, StaticConstants.FEED_AUDIO);

            }
        } else {
            Toast.makeText(getActivity(), "Something went wrong..", Toast.LENGTH_SHORT).show();
        }
    }

    private void showSorry(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Feedback uploading")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
    }

    private void putToDb(final Feedback feedback) {
        new Thread(new Runnable() {
            public void run() {
                AppDatabase.getInstance(getContext()).getFeedbackDao().insertFeedback(feedback);
            }
        }).start();
    }

    private void initDropbox() {
        //dropbox.init();
        googleDrive.initGoogleDrive(getContext());

    }

    @OnClick(R.id.button_cancel_audio)
    void cancel() {

    }

    private void requestPermissionRecord() {
        String[] perms = {Manifest.permission.RECORD_AUDIO};
        if (!EasyPermissions.hasPermissions(getActivity(), perms))
            EasyPermissions.requestPermissions(this, "Permission needed to record audio feedback!", 13242, perms);
    }

    boolean moveAudio(RecordingItem item) {

        File old = new File(item.getFilePath());
        File newFile = new File(Environment.getExternalStorageDirectory()
                + "/Inqui/Feedback/Audio" + item.getName());
        filePath = Environment.getExternalStorageDirectory()
                + "/Inqui/Feedback/Audio" + item.getName();
        File mkdir = new File(Environment.getExternalStorageDirectory()
                + "/Inqui/Feedback/Audio");
        mkdir.mkdirs();
        FileChannel inChannel = null;
        FileChannel outChannel = null;
        try {
            newFile.getParentFile().mkdirs();
            newFile.createNewFile();
            inChannel = new FileInputStream(old).getChannel();
            outChannel = new FileOutputStream(newFile).getChannel();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (Exception e) {
            e.printStackTrace();
        } finally

        {
            try {
                if (inChannel != null) {
                    inChannel.close();
                }
                if (outChannel != null)
                    outChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return newFile.exists();
    }

    @Override
    public void processFinish(String path) {
        if (path != null) {
            FeedbackBody body = new FeedbackBody();
            body.setStudentId(studentId);
            body.setFeedBackType("audio");
            body.setFeedBackPath(path);
            body.setDesignId("");
            body.setConceptId(conceptId);
            submitFeed(body);
            progress.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Feedback uploading")
                    .setMessage("Feedback was successfully uploaded to Google Drive")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    HomePageFragment homePageFragment = new HomePageFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.root_for_fragments, homePageFragment);
                                    transaction.commit();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            editor = sharedPreferences.edit();
            editor.putBoolean("sync", false);
            editor.apply();
            deleteFile();
        } else {
            editor = sharedPreferences.edit();
            editor.putBoolean("sync", true);
            editor.apply();
        }

    }

    private void submitFeed(FeedbackBody body) {
        HttpHelper.getApi().submitFeedback(body).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("FeedBack Successfully added via ... audio mode")) {
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }

    private void deleteFile() {
        File file = new File(filePath);
        file.delete();
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Uploading audio feedback..");
        progress.show();
    }
}
