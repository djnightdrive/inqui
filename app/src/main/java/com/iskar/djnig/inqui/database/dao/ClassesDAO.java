package com.iskar.djnig.inqui.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.iskar.djnig.inqui.database.entities.Classes;

import java.util.List;

/**
 * Created by karasinboots on 30.11.2017.
 */

@Dao
public interface ClassesDAO {

    @Query("SELECT * FROM classes")
    List<Classes> getAll();

    @Query("DELETE FROM classes")
    void clear();

    @Query("SELECT * FROM classes WHERE classId LIKE :classId")
    Classes getByClass(String classId);

    @Insert
    void insert(Classes classes);

    @Update
    void update(Classes classes);

    @Delete
    void delete(Classes classes);
}