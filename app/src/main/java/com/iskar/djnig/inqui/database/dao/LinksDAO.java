package com.iskar.djnig.inqui.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.iskar.djnig.inqui.database.entities.Links;

import java.util.List;

/**
 * Created by karasinboots on 12.12.2017.
 */
@Dao
public interface LinksDAO {
    @Query("SELECT * FROM links")
    List<Links> getAll();

    @Query("SELECT * FROM links WHERE designId LIKE :designId")
    Links getByDesignId(String designId);

    @Query("DELETE FROM links")
    void clear();

    @Insert
    void insertLinks(Links links);

    @Update
    void updateLinks(Links links);

    @Delete
    void deleteLinks(Links links);
}
