
package com.iskar.djnig.inqui.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class School implements Parcelable
{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("classes")
    @Expose
    private List<Class> classes = null;
    public final static Parcelable.Creator<School> CREATOR = new Creator<School>() {


        @SuppressWarnings({
            "unchecked"
        })
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        public School[] newArray(int size) {
            return (new School[size]);
        }

    }
    ;

    protected School(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.classes, (com.iskar.djnig.inqui.model.Class.class.getClassLoader()));
    }

    public School(String name, String id, List<Class> classes) {
        this.name = name;
        this.id = id;
        this.classes = classes;
    }

    public School(List<Class> classes) {
        this.classes = classes;
    }

    public School() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Class> getClasses() {
        return classes;
    }

    public void setClasses(List<Class> classes) {
        this.classes = classes;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(id);
        dest.writeList(classes);
    }

    public int describeContents() {
        return  0;
    }

}
