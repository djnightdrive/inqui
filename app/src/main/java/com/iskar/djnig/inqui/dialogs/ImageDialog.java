package com.iskar.djnig.inqui.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.common.io.Files;
import com.iskar.djnig.inqui.R;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import butterknife.ButterKnife;

/**
 * Created by karasinboots on 03.12.2017.
 */

public class ImageDialog extends DialogFragment {

    final String LOG_TAG = "myLogs";
    private ZoomageView zoomageView;
    private String dropLink;

    public void googleDriveLoadImage() {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GoogleCredential googleCredential = null;
                    Drive googleService;
                    String[] SCOPES = {DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE, DriveScopes.DRIVE_FILE};
                    try {
                        googleCredential = GoogleCredential.fromStream(getResources().openRawResource(R.raw.inquilab2b9e83128b90)).createScoped(Arrays.asList(SCOPES));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    HttpTransport transport = AndroidHttp.newCompatibleTransport();
                    JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
                    googleService = new com.google.api.services.drive.Drive.Builder(
                            transport, jsonFactory, googleCredential)
                            .setApplicationName("InquilabServiceAccount")
                            .build();
                    final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    googleService.files().get(dropLink)
                            //.setQ("name contains '" + title + "'")
                            .executeMediaAndDownloadTo(outputStream);
                    byte[] bitmapdata = outputStream.toByteArray();
                    final File file = File.createTempFile("temp", "jpeg");
                    Files.write(bitmapdata, file);

                    //Picasso.with(context).load(link).resize(300, 300).centerCrop().into(holder.design);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(outputStream != null)
                                Picasso.with(getContext()).load(file).resize(1080, 1920).centerInside().into(zoomageView);
                            else
                                Picasso.with(getContext()).load(R.drawable.image_not_available_300).resize(1080, 1920).centerInside().into(zoomageView);
                        }
                    });
                        /*if (dropLink != null && !dropLink.equals("exception"))
                            Picasso.with(getContext()).load(dropLink)
                                    .resize(1000, 1000).centerCrop().into(design);
                        else Picasso.with(getContext()).load(R.drawable.image_not_available_300).resize(300, 300)
                                .centerCrop().into(design);*/
                }
                catch (Exception e) {

                }
            }
        });
        thread.start();


    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        View v = inflater.inflate(R.layout.custom_fullimage_dialog, null);
        ButterKnife.bind(this, v);
        dropLink = getArguments().getString("view");
        try {
            zoomageView = v.findViewById(R.id.myZoomageView);
            googleDriveLoadImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

}

