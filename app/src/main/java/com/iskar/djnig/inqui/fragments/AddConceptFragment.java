package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.api.AsyncResponse;
import com.iskar.djnig.inqui.api.CallApi;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.recyclers.concepts.Concepts;
import com.iskar.djnig.inqui.recyclers.concepts.ConceptsAdapter;
import com.iskar.djnig.inqui.utils.FragmentUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 26.11.2017.
 */

public class AddConceptFragment extends Fragment implements AsyncResponse {

    @BindView(R.id.cancel_concept)
    Button cancel;

    @BindView(R.id.add_concept)
    Button addConcept;

    @BindView(R.id.add_concept_edit)
    EditText editText;

    @BindView(R.id.recycler_concept)
    RecyclerView concepts;

    ArrayList<com.iskar.djnig.inqui.recyclers.concepts.Concepts> conceptsData = new ArrayList<>();
    CallApi callApi;
    MyProgressDialog progress;
    LinearLayoutManager linearLayoutManager;
    boolean exist = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_concept_layout, container, false);
        ButterKnife.bind(this, v);
        callApi = new CallApi("add_concept", getContext());
        callApi.delegate = this;
        getConcepts();
        addConcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().length() > 0) {
                    for (Concepts concept : conceptsData) {
                        if (concept.getConceptName().toLowerCase().equals(editText.getText().toString().toLowerCase())) {
                            exist = true;

                        }
                    }
                    if (!exist) {
                        callApi.execute(editText.getText().toString());
                        showProgress();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Add concept")
                                .setMessage("Concept already exist!")
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }

                } else editText.setError("Write concept name!");

            }
        });
        linearLayoutManager = new LinearLayoutManager(v.getContext());
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentUtil.attachFragmentWithoutBS(new HomePageFragment(), getActivity());
            }
        });
        return v;
    }

    private void getConcepts() {

        fillConceptsFromDb();

    }


    private void fillConceptsFromDb() {
        conceptsData.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<com.iskar.djnig.inqui.database.entities.Concepts> concepts = AppDatabase.getInstance(getContext()).getConceptsDao().getAll();
                for (com.iskar.djnig.inqui.database.entities.Concepts concept : concepts) {
                    conceptsData.add(new com.iskar.djnig.inqui.recyclers.concepts.Concepts(concept.getConceptName()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupRecycler(conceptsData);
            }
        };
        asyncTask.execute();
    }

    private void setupRecycler(ArrayList<com.iskar.djnig.inqui.recyclers.concepts.Concepts> conceptsData) {
        ConceptsAdapter conceptsAdapter = new ConceptsAdapter(conceptsData);
        concepts.setLayoutManager(linearLayoutManager);
        concepts.setAdapter(conceptsAdapter);
        conceptsAdapter.notifyDataSetChanged();
    }

    @Override
    public void processFinish(String apiAction, StringBuilder serverResponse) {

        try {
            JSONObject json = new JSONObject(serverResponse.toString());
            String response = json.getString("message");
            if (Objects.equals(response, "Successfully added")) {
                progress.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Add concept")
                        .setMessage("Concept was successfully added!")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
                getConcepts();
            } else {
                progress.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Add concept")
                        .setMessage("Something went wrong!")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        } catch (JSONException e) {
            progress.dismiss();
            e.printStackTrace();
        }
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Adding concept..");
        progress.show();
    }
}
