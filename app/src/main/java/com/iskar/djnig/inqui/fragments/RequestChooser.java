package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Concepts;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Class;
import com.iskar.djnig.inqui.model.Concept;
import com.iskar.djnig.inqui.model.School;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 10.12.2017.
 */

public class RequestChooser extends Fragment {
    @BindView(R.id.button_view_info)
    Button viewInfo;

    @BindView(R.id.spinner_school)
    Spinner spinnerSchool;

    @BindView(R.id.spinner_class)
    Spinner spinnerClassroom;
    @BindView(R.id.spinner_concept)
    Spinner spinnerConcept;
    @BindView(R.id.spinner_approved)
    Spinner spinnerDesigns;
    MyProgressDialog progress;
    List<School> mSchools = new ArrayList<>();
    List<String> classNames = new ArrayList<>();
    Context mContext;
    List<Concept> mConcepts = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.request_chooser, container, false);
        ButterKnife.bind(this, v);
        String[] schoolsArray = getResources().getStringArray(R.array.spinner_schools);
        final String[] classesArray = getResources().getStringArray(R.array.spinner_classes);
        String[] concepts = getResources().getStringArray(R.array.spinner_concepts);
        String[] designs = {"APPROVED DESIGNS"};
        spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                schoolsArray));
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                classesArray));
        spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                concepts));
        spinnerDesigns.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                designs));

        viewInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bundle bundle = new Bundle();
                    bundle.putString("schoolName", mSchools.get(spinnerSchool.getSelectedItemPosition()).getName());
                    bundle.putString("conceptName", mConcepts.get(spinnerConcept.getSelectedItemPosition()).getName());
                    bundle.putString("conceptId", mConcepts.get(spinnerConcept.getSelectedItemPosition()).getId());
                    bundle.putString("className", classNames.get(spinnerClassroom.getSelectedItemPosition()));
                    bundle.putString("designName", spinnerDesigns.getSelectedItem().toString());
                    RequestMaterialsFragment requestMaterialsFragment = new RequestMaterialsFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    requestMaterialsFragment.setArguments(bundle);
                    transaction.replace(R.id.root_for_fragments, requestMaterialsFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return v;
    }

    private void getDesignsByConcept(final String id) {

        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    List<Design> list;
                    if (id.equals("0"))
                        list = AppDatabase.getInstance(getContext()).getDesignDao().getByClass(
                                mSchools.get(spinnerSchool.getSelectedItemPosition())
                                        .getClasses().get(spinnerClassroom.getSelectedItemPosition()).getId());
                    else
                        list = AppDatabase.getInstance(getContext()).getDesignDao().getByConceptApproved(id,
                                mSchools.get(spinnerSchool.getSelectedItemPosition())
                                        .getClasses().get(spinnerClassroom.getSelectedItemPosition()).getId());

                    return list;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                List<String> list = new ArrayList<>();
                try {
                    for (Design design : (List<Design>) o) {
                        list.add(design.getDesign_name());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                fillDesignsSpinner(list);
            }
        };
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void fillDesignsSpinner(List<String> list) {
        try {
            spinnerDesigns.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    list.toArray(new String[list.size()])));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        fillFromDb();
        fillConceptsFromDb();
    }

    private void fillConceptsFromDb() {
        mConcepts.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Concepts> concepts = AppDatabase.getInstance(mContext).getConceptsDao().getAll();
                for (Concepts concept : concepts) {
                    mConcepts.add(new Concept(concept.getConceptName(), concept.getConceptId()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillConceptSpinner();
            }
        };
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void fillFromDb() {
        mSchools.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Schools> schools = AppDatabase.getInstance(mContext).getSchoolsDao().getAll();
                //List<String> classNames;
                List<String> classIds;
                for (Schools tempSchool : schools) {
                    List<Class> classList = new ArrayList<>();
                    classNames = tempSchool.getClasses();
                    classIds = tempSchool.getClassesId();
                    for (int i = 0; i < classNames.size(); i++) {
                        classList.add(new Class(classNames.get(i), classIds.get(i)));
                    }
                    mSchools.add(new School(tempSchool.getSchoolName(), tempSchool.getSchoolId(), classList));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillSchoolSpinners();
            }
        };
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void fillConceptSpinner() {
        try {
            ArrayList<String> concepts = new ArrayList<>();
            concepts.clear();
            concepts.add("All concepts");
            for (Concept cl : mConcepts) {
                concepts.add(cl.getName());
            }
            spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    concepts.toArray(new String[concepts.size()])));
            spinnerConcept.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 0) {
                        getDesignsByConcept("0");
                    } else
                        getDesignsByConcept(mConcepts.get(i - 1).getId());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    getDesignsByConcept("0");
                }
            });
            Bundle arguments = getArguments();
            if (arguments != null && arguments.containsKey("spinners")) {
                spinnerConcept.setSelection(arguments.getIntArray("spinners")[2]);
            } else spinnerConcept.setSelection(0);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void fillSchoolSpinners() {
        final ArrayList<String> schoolNames = new ArrayList<>();
        try {
            for (School sc : mSchools) {
                schoolNames.add(sc.getName());
            }
            // Declaring an Adapter and initializing it to the data pump
            spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    schoolNames.toArray(new String[schoolNames.size()])));
            // Setting OnItemClickListener to the Spinner
            spinnerSchool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fillClassesSpinner(mSchools.get(position).getClasses());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void fillClassesSpinner(final List<Class> classesList) {
        ArrayList<String> sections = new ArrayList<>();
        for (Class cl : classesList) {
            sections.add(cl.getSection());
        }
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                sections.toArray(new String[sections.size()])));
        // Setting OnItemClickListener to the Spinner
        spinnerClassroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getDesignsByConcept("0");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
