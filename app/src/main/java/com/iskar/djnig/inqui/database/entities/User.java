package com.iskar.djnig.inqui.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by karasinboots on 14.11.2017.
 */
@Entity
public class User {
    @PrimaryKey(autoGenerate = true)
    private
    int user_id;

    private String email;
    private String password;
    private String name;
    private String school_name;
    private String class_name;
    private String drop_folder;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getDrop_folder() {
        return drop_folder;
    }

    public void setDrop_folder(String drop_folder) {
        this.drop_folder = drop_folder;
    }
}
