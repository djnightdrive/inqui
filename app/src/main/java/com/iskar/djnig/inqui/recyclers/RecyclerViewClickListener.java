package com.iskar.djnig.inqui.recyclers;

import android.view.View;

/**
 * Created by karasinboots on 03.12.2017.
 */

public interface RecyclerViewClickListener {

    void recyclerViewListClicked(View v, int position, String string);
}