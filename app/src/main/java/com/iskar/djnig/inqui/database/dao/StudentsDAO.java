package com.iskar.djnig.inqui.database.dao;

/**
 * Created by karasinboots on 30.11.2017.
 */

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.iskar.djnig.inqui.database.entities.Students;

import java.util.List;

@Dao
public interface StudentsDAO {

    @Insert
    void insert(Students student);

    @Update
    void update(Students... studentss);

    @Delete
    void delete(Students... studentss);

    @Query("SELECT * FROM students")
    List<Students> getAllStudents();

    @Query("SELECT * FROM students WHERE rollId IS :rollId")
    Students getStudentByRoll(String rollId);

    @Query("SELECT * FROM students WHERE classId LIKE :classId")
    List<Students> getStudentByClass(String classId);

    @Query("DELETE FROM students")
    void clear();


}