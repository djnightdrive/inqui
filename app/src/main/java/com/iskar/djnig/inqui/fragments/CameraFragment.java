package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.WhiteBalance;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.TedBottomPicker;
import pub.devrel.easypermissions.EasyPermissions;


public class CameraFragment extends Fragment {
    @BindView(R.id.camera)
    CameraView mCameraView;

    @BindView(R.id.take_picture)
    ImageButton mCameraButton;

    @BindView(R.id.open_gallery)
    ImageView mOpenGallery;

    private boolean cameraBusy = false;
    private String mFilePath;
    private String mPreviousImage = "empty";
    MyProgressDialog progress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.camera_fragment, container, false);
        ButterKnife.bind(this, v);
        setupCamera();
        setPreview();
        mOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGallery();

            }
        });
        if (!cameraBusy)
            mCameraButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayMetrics metrics = new DisplayMetrics();
                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    mCameraView.startAutoFocus(metrics.widthPixels / 2, metrics.heightPixels / 2);
                    cameraBusy = true;

                }
            });
        return v;
    }


    private void setPreview() {
        // Find the last picture
        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE
        };
        final Cursor cursor = getActivity().getContentResolver()
                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                        null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        // Put it in the image view
        if (cursor != null)
            if (cursor.moveToFirst()) {
                String imageLocation = cursor.getString(1);
                File imageFile = new File(imageLocation);
                if (imageFile.exists()) {
                    Picasso.with(getContext())
                            .load(imageFile)
                            .resize(100, 100)
                            .centerCrop()
                            .into(mOpenGallery);
                }
                cursor.close();
            }
    }

    private void showGallery() {
        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(getActivity())
                .setTitle(getString(R.string.select_from_gallery))
                .showCameraTile(false)
                .showGalleryTile(false)
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        if (new File(uri.getPath()).exists())
                            cutImage(uri);
                        else
                            Toast.makeText(getContext(), "Please, select another image!", Toast.LENGTH_SHORT).show();
                    }
                }).create();
        tedBottomPicker.show(getFragmentManager());
    }

    private void updateStorageDb(Uri uri) {
        try {
            MediaScannerConnection.scanFile(getContext(), new String[]{uri.getPath()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            scanIntent.setData(uri);
            getActivity().sendBroadcast(scanIntent);
        } else {
            final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, uri);
            getActivity().sendBroadcast(intent);
        }
    }

    private void setupCamera() {
        mCameraView.setCropOutput(true);
        mCameraView.setFitsSystemWindows(true);
        mCameraView.setWhiteBalance(WhiteBalance.AUTO);
        mCameraView.addCameraListener(new CameraListener() {
            @Override
            public void onCameraError(CameraException error) {
                Log.e("camera error", error.getMessage() + "");
            }

            @Override
            public void onFocusEnd(boolean successful, PointF point) {
                mCameraView.capturePicture();
            }

            @Override
            public void onPictureTaken(final byte[] picture) {
                if (mPreviousImage.equals("empty")) {
                    Log.e("picture ", "taken");
                    String fileName = ("/"
                            + getArguments().getString("schoolName") + "_"
                            + getArguments().getString("className") + "_"
                            + getArguments().getString("conceptName") + "_"
                            + getArguments().getString("designName") + "_"
                            + System.currentTimeMillis() + ".jpg")
                            .replaceAll("\\s+", "_");
                    mFilePath = Environment.getExternalStorageDirectory() + "/Inqui/Design" + fileName;
                    File tempFile = new File(Environment.getExternalStorageDirectory() + "/Inqui/Design");
                    tempFile.mkdirs();
                    File file = new File(mFilePath);
                    try {
                        try (FileOutputStream fos = new FileOutputStream(mFilePath)) {
                            fos.write(picture);
                            fos.close();
                        }
                        Uri uri = Uri.parse("file://" + mFilePath);
                        cutImage(uri);
                    } catch (IOException e) {
                        Log.e("ERROR", "Cannot write to " + file, e);
                    } finally {
                        cameraBusy = false;
                    }
                } else {
                    String fileName = ("/"
                            + getArguments().getString("schoolName") + "_"
                            + getArguments().getString("className") + "_"
                            + getArguments().getString("conceptName") + "_"
                            + getArguments().getString("designName") + "_"
                            + System.currentTimeMillis() + ".jpg")
                            .replaceAll("\\s+", "_");
                    mFilePath = Environment.getExternalStorageDirectory() + "/Inqui/Design" + fileName;
                    File tempFile = new File(Environment.getExternalStorageDirectory() + "/Inqui/Design");
                    tempFile.mkdirs();
                    File file = new File(mFilePath);
                    try {
                        try (FileOutputStream fos = new FileOutputStream(mFilePath)) {
                            fos.write(picture);
                            fos.close();
                        }
                        Uri uri = Uri.parse("file://" + mFilePath);
                        cutImage(uri);
                    } catch (IOException e) {
                        Log.e("ERROR", "Cannot write to " + file, e);
                    } finally {
                        cameraBusy = false;
                    }
                }
            }
        });
    }

    private void cutImage(Uri uri) {
        Intent intent = new Intent(getActivity(), ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, 1);
        intent.putExtra("image_uri", uri);
        startActivityForResult(intent, 99);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 99) {
            if (resultCode == Activity.RESULT_OK) {
                if (mPreviousImage.equals("empty")) {
                    final Uri resultUri = data.getParcelableExtra(ScanConstants.SCANNED_RESULT);
                    setPreview();
                    mPreviousImage = resultUri.getPath();
                    Log.e("first filepath", mPreviousImage);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Design uploading")
                            .setMessage("Do you want to capture next image?")
                            .setCancelable(false)
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                            .setNegativeButton("No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            Bundle bundle = new Bundle();
                                            bundle.putSerializable("design", getArguments().getSerializable("design"));
                                            bundle.putString("design_uri", String.valueOf(resultUri));
                                            bundle.putString("classid", getArguments().getString("classid"));
                                            bundle.putString("schoolId", getArguments().getString("schoolId"));
                                            bundle.putString("conceptId", getArguments().getString("conceptId"));
                                            bundle.putString("designId", getArguments().getString("designId"));
                                            bundle.putString("schoolName", getArguments().getString("schoolName"));
                                            bundle.putString("className", getArguments().getString("className"));
                                            bundle.putString("conceptName", getArguments().getString("conceptName"));
                                            bundle.putString("designName", getArguments().getString("designName"));
                                            bundle.putIntArray("spinners", getArguments().getIntArray("spinners"));
                                            UploadDesignFragment uploadDesignFragment = new UploadDesignFragment();
                                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                            uploadDesignFragment.setArguments(bundle);
                                            transaction.replace(R.id.root_for_fragments, uploadDesignFragment);
                                            transaction.commit();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    showProgress();
                    try {
                        final Uri resultUri = data.getParcelableExtra(ScanConstants.SCANNED_RESULT);
                        setPreview();
                        @SuppressLint("StaticFieldLeak") AsyncTask asyncTask = new AsyncTask() {
                            @Override
                            protected Object doInBackground(Object[] objects) {
                                Bitmap bmp1, bmp2;
                                bmp1 = BitmapFactory.decodeFile(mPreviousImage);
                                bmp2 = BitmapFactory.decodeFile(resultUri.getPath());
                                int width = bmp1.getWidth();
                                if (width < bmp2.getWidth())
                                    width = bmp2.getWidth();
                                Bitmap bmOverlay = Bitmap.createBitmap(width, bmp1.getHeight() + bmp2.getHeight(), Bitmap.Config.ARGB_8888);
                                Canvas canvas = new Canvas(bmOverlay);
                                canvas.drawBitmap(bmp1, 0, 0, null);
                                canvas.drawBitmap(bmp2, 0, bmp1.getHeight(), null);
                                try {
                                    bmOverlay.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(resultUri.getPath()));
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                File file = new File(mPreviousImage);
                                file.delete();
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Object o) {
                                progress.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Design uploading")
                                        .setMessage("Do you want to capture next image?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                        mPreviousImage = resultUri.getPath();
                                                    }
                                                })
                                        .setNegativeButton("No",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                        updateStorageDb(resultUri);
                                                        Bundle bundle = new Bundle();
                                                        bundle.putSerializable("design", getArguments().getSerializable("design"));
                                                        bundle.putString("design_uri", String.valueOf(resultUri));
                                                        bundle.putString("classid", getArguments().getString("classid"));
                                                        bundle.putString("schoolId", getArguments().getString("schoolId"));
                                                        bundle.putString("conceptId", getArguments().getString("conceptId"));
                                                        bundle.putString("designId", getArguments().getString("designId"));
                                                        bundle.putString("schoolName", getArguments().getString("schoolName"));
                                                        bundle.putString("className", getArguments().getString("className"));
                                                        bundle.putString("conceptName", getArguments().getString("conceptName"));
                                                        bundle.putString("designName", getArguments().getString("designName"));
                                                        bundle.putIntArray("spinners", getArguments().getIntArray("spinners"));
                                                        UploadDesignFragment uploadDesignFragment = new UploadDesignFragment();
                                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                        uploadDesignFragment.setArguments(bundle);
                                                        transaction.replace(R.id.root_for_fragments, uploadDesignFragment);
                                                        transaction.commit();
                                                    }
                                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        };
                        asyncTask.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progress.dismiss();
                    }
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Invalid result")
                        .setMessage("Do you want to try again?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        HomePageFragment homePageFragment = new HomePageFragment();
                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                        transaction.replace(R.id.root_for_fragments, homePageFragment);
                                        transaction.commit();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Merging images...");
        progress.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCameraView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        mCameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCameraView.destroy();
    }
}
