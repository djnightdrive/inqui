package com.iskar.djnig.inqui.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.iskar.djnig.inqui.database.ListStringConverter;

import java.util.List;

/**
 * Created by karasinboots on 23.11.2017.
 */
@Entity
public class Schools {
    @PrimaryKey(autoGenerate = true)
    private
    int id;

    private String schoolName;
    private String schoolId;
    @TypeConverters({ListStringConverter.class})
    private List<String> classes;
    @TypeConverters({ListStringConverter.class})
    private List<String> classesId;

    public Schools(String schoolName, String schoolId, List<String> classes, List<String> classesId) {
        this.schoolName = schoolName;
        this.schoolId = schoolId;
        this.classes = classes;
        this.classesId = classesId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }

    public List<String> getClassesId() {
        return classesId;
    }

    public void setClassesId(List<String> classesId) {
        this.classesId = classesId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
