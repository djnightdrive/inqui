package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.common.io.Files;
import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.database.entities.Students;
import com.iskar.djnig.inqui.dialogs.ImageDialog;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Approvation;
import com.iskar.djnig.inqui.recyclers.approve.Approves;
import com.iskar.djnig.inqui.recyclers.approve.ApprovesAdapter;
import com.iskar.djnig.inqui.recyclers.design.Designs;
import com.iskar.djnig.inqui.utils.NetworkManager;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by djnig on 11/14/2017.
 */

public class ApproveDesignFragment extends Fragment {
    @BindView(R.id.button_approve)
    Button approve;
    @BindView(R.id.button_reject)
    Button reject;
    @BindView(R.id.button_in_review)
    Button inReview;
    @BindView(R.id.design)
    ImageView design;
    @BindView(R.id.approv2)
    ImageView approveImage;
    @BindView(R.id.design_name)
    TextView designName;
    @BindView(R.id.approvesRecycler)
    RecyclerView approvesRecycler;
    @BindView(R.id.edit_feed_txt)
    EditText editText;
    @BindView(R.id.back)
    Button back;
    @BindView(R.id.comment)
    TextView comment;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    ViewGroup container;
    Designs designs;
    LinearLayoutManager linearLayoutManager;
    MyProgressDialog progress;
    String dropLink;
    SharedPreferences sharedPreferences;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.approve_design, container, false);
        ButterKnife.bind(this, v);
        this.container = container;
        sharedPreferences = getContext().getSharedPreferences("design", Context.MODE_PRIVATE);
        return v;
    }

    private void getClasses(final int position) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    List<String> rolls = designs.getRolls();
                    List<Approves> approves = new ArrayList<>();
                    for (String roll : rolls) {
                        Students student = AppDatabase.getInstance(getContext()).getStudentsDao().getStudentByRoll(roll);
                        Log.e("roll", roll);
                        Schools schools = AppDatabase.getInstance(getContext()).getSchoolsDao().getByClass("%" + student.getClassId() + "%");
                        Log.e("classId", student.getClassId());
                        String student_name = student.getFirstName() + " " + student.getLastName();
                        Log.e("name", student.getFirstName() + " " + student.getLastName());
                        String schoolName = schools.getSchoolName();
                        Log.e("school name", schools.getSchoolName());
                        String className = AppDatabase.getInstance(getContext()).getClassesDao().getByClass(student.getClassId()).getClassName();
                        Log.e("class_icon name", className);
                        if (student.getClassId().equals(sharedPreferences.getString("class_icon", "")))
                            approves.add(new Approves(student_name, schoolName, className, roll));
                    }
                    return approves;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                setupRecycler((List<Approves>) o);
            }
        };
        asyncTask.execute();
    }

    int position;

    public void googleDriveLoadImage() {

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        GoogleCredential googleCredential = null;
                        Drive googleService;
                        String[] SCOPES = {DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE, DriveScopes.DRIVE_FILE};
                        try {
                            googleCredential = GoogleCredential.fromStream(getResources().openRawResource(R.raw.inquilab2b9e83128b90)).createScoped(Arrays.asList(SCOPES));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        HttpTransport transport = AndroidHttp.newCompatibleTransport();
                        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
                        googleService = new com.google.api.services.drive.Drive.Builder(
                                transport, jsonFactory, googleCredential)
                                .setApplicationName("InquilabServiceAccount")
                                .build();
                        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        googleService.files().get(dropLink)
                                //.setQ("name contains '" + title + "'")
                                .executeMediaAndDownloadTo(outputStream);
                        byte[] bitmapdata = outputStream.toByteArray();
                        final File file = File.createTempFile("temp", "jpeg");
                        Files.write(bitmapdata, file);

                        //Picasso.with(context).load(link).resize(300, 300).centerCrop().into(holder.design);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(outputStream != null)
                                Picasso.with(getContext()).load(file)
                                        .resize(1000, 1000).centerCrop().into(design);
                                else Picasso.with(getContext()).load(R.drawable.image_not_available_300).resize(300, 300)
                                        .centerCrop().into(design);
                            }
                        });
                        /*if (dropLink != null && !dropLink.equals("exception"))
                            Picasso.with(getContext()).load(dropLink)
                                    .resize(1000, 1000).centerCrop().into(design);
                        else Picasso.with(getContext()).load(R.drawable.image_not_available_300).resize(300, 300)
                                .centerCrop().into(design);*/
                    }
                    catch (Exception e) {

                    }
                }
            });
            thread.start();


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        position = getArguments().getInt("position");
        designs = (Designs) getArguments().getSerializable("design");
        dropLink = getArguments().getString("dropLink");
        getClasses(position);
        linearLayoutManager = new LinearLayoutManager(view.getContext());
        googleDriveLoadImage();
        /*if (dropLink != null && !dropLink.equals("exception"))
            Picasso.with(getContext()).load(dropLink)
                    .resize(1000, 1000).centerCrop().into(design);
        else Picasso.with(getContext()).load(R.drawable.image_not_available_300).resize(300, 300)
                .centerCrop().into(design);*/
        if (designs.getStatus().equals("Approve")) {
            Picasso.with(getContext()).load(R.drawable.approved).resize(300, 300).centerInside().into(approveImage);
            back.setVisibility(View.VISIBLE);
            comment.setVisibility(View.VISIBLE);
            comment.setText(designs.getDesignComments() + "");
            relativeLayout.setVisibility(View.INVISIBLE);
            approve.setVisibility(View.INVISIBLE);
            reject.setVisibility(View.INVISIBLE);
            inReview.setVisibility(View.INVISIBLE);
        } else if (designs.getStatus().equals("Reject")) {
            Picasso.with(getContext()).load(R.drawable.rejected).resize(300, 300).centerInside().into(approveImage);
            back.setVisibility(View.VISIBLE);
            comment.setVisibility(View.VISIBLE);
            comment.setText(designs.getDesignComments() + "");
            relativeLayout.setVisibility(View.INVISIBLE);
            approve.setVisibility(View.INVISIBLE);
            reject.setVisibility(View.INVISIBLE);
            inReview.setVisibility(View.INVISIBLE);
        } else if(designs.getStatus().equals("In review")) {
            Picasso.with(getContext()).load(R.drawable.in_review).resize(300, 300).centerInside().into(approveImage);
            back.setVisibility(View.INVISIBLE);
            comment.setVisibility(View.INVISIBLE);
            //comment.setText(designs.getDesignComments() + "");
            relativeLayout.setVisibility(View.VISIBLE);
            approve.setVisibility(View.VISIBLE);
            reject.setVisibility(View.VISIBLE);
            inReview.setVisibility(View.VISIBLE);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });
        designName.setText(designs.getDesignName());
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                designs.setStatus("Approve");
                if (editText.getText().length() > 0) {
                    showProgress();
                    designs.setDesignComments(editText.getText().toString());
                    updateStatus(designs);
                    if (NetworkManager.isConnected(getContext())) {
                        Approvation approvation = new Approvation();
                        approvation.setDesignComments(editText.getText().toString());
                        approvation.setDesignId(designs.getDesignId());
                        approvation.setInstructorAction("Approve");
                        List<Approvation> approvations = new ArrayList<>();
                        approvations.add(approvation);
                        HttpHelper.getApi().submitApprovation(approvations).enqueue(new Callback<com.iskar.djnig.inqui.model.Response>() {
                            @Override
                            public void onResponse(Call<com.iskar.djnig.inqui.model.Response> call, Response<com.iskar.djnig.inqui.model.Response> response) {
                                if (response.isSuccessful()) {
                                    if ((response.body()).getMessage().contains("acknowledged")) {
                                        progress.dismiss();
                                        Log.e("message_approvation", response.body().getMessage());
                                        showDialog("Concept successfully approved!");
                                        int count = getFragmentManager().getBackStackEntryCount();
                                        if (count > 0)
                                            getFragmentManager().popBackStack();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<com.iskar.djnig.inqui.model.Response> call, Throwable t) {
                                progress.dismiss();
                            }
                        });
                    } else {
                        showDialog("Can't approve without internet!");
                    }
                } else editText.setError("Write comments!");

            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().length() > 0) {
                    showProgress();
                    designs.setStatus("Reject");
                    designs.setDesignComments(editText.getText().toString());
                    updateStatus(designs);
                    if (NetworkManager.isConnected(getContext())) {
                        Approvation approvation = new Approvation();
                        approvation.setDesignComments(editText.getText().toString());
                        approvation.setDesignId(designs.getDesignId());
                        approvation.setInstructorAction("Reject");
                        List<Approvation> approvations = new ArrayList<>();
                        approvations.add(approvation);
                        HttpHelper.getApi().submitApprovation(approvations).enqueue(new Callback<com.iskar.djnig.inqui.model.Response>() {
                            @Override
                            public void onResponse(Call<com.iskar.djnig.inqui.model.Response> call, Response<com.iskar.djnig.inqui.model.Response> response) {
                                if (response.isSuccessful()) {
                                    if ((response.body()).getMessage().contains("acknowledged")) {
                                        Log.e("message_approvation", response.body().getMessage());
                                        Log.e("approvation code", String.valueOf(response.code()));
                                        progress.dismiss();
                                        showDialog("Concept succsesfully rejected!");
                                        int count = getFragmentManager().getBackStackEntryCount();
                                        if (count > 0)
                                            getFragmentManager().popBackStack();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<com.iskar.djnig.inqui.model.Response> call, Throwable t) {
                                progress.dismiss();
                            }
                        });
                    } else {
                        showDialog("Can't reject without internet!");
                    }
                } else editText.setError("Write comments!");
            }
        });

        inReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    showProgress();
                    designs.setStatus("In review");
                    designs.setDesignComments(editText.getText().toString());
                    updateStatus(designs);
                    if (NetworkManager.isConnected(getContext())) {
                        Approvation approvation = new Approvation();
                        approvation.setDesignComments(editText.getText().toString());
                        approvation.setDesignId(designs.getDesignId());
                        approvation.setInstructorAction("In review");
                        List<Approvation> approvations = new ArrayList<>();
                        approvations.add(approvation);
                        HttpHelper.getApi().submitApprovation(approvations).enqueue(new Callback<com.iskar.djnig.inqui.model.Response>() {
                            @Override
                            public void onResponse(Call<com.iskar.djnig.inqui.model.Response> call, Response<com.iskar.djnig.inqui.model.Response> response) {
                                if (response.isSuccessful()) {
                                    if ((response.body()).getMessage().contains("acknowledged")) {
                                        Log.e("message_approvation", response.body().getMessage());
                                        Log.e("approvation code", String.valueOf(response.code()));
                                        progress.dismiss();
                                        showDialog("Concept succsesfully marked as 'In review'!");
                                        int count = getFragmentManager().getBackStackEntryCount();
                                        if (count > 0)
                                            getFragmentManager().popBackStack();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<com.iskar.djnig.inqui.model.Response> call, Throwable t) {
                                progress.dismiss();
                            }
                        });
                    } else {
                        showDialog("Can't select in review without internet!");
                    }
            }
        });
        design.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (NetworkManager.isConnected(getContext())) {
                    ImageDialog imageDialog = new ImageDialog();
                    Bundle bundle = new Bundle();
                    if (dropLink == null || dropLink.equals("exception"))
                        bundle.putString("view", "exception");
                    else
                        bundle.putString("view", dropLink);
                    imageDialog.setArguments(bundle);
                    imageDialog.show(getFragmentManager(), "image");
                }
            }
        });

    }

    private void updateStatus(final Designs design) {
        new Thread(new Runnable() {
            public void run() {
                Design design1 = AppDatabase.getInstance(getContext()).getDesignDao().getById(designs.getDesignId());
                design1.setApproved(design.getStatus());
                design1.setComment(design.getDesignComments());
                AppDatabase.getInstance(getContext()).getDesignDao().update(design1);
            }
        }).start();
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Uploading design..");
        progress.show();
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Design approvation")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
    }

    private void setupRecycler(List<Approves> approves) {
        ApprovesAdapter approvesAdapter = new ApprovesAdapter(approves);
        approvesRecycler.setLayoutManager(linearLayoutManager);
        approvesRecycler.setAdapter(approvesAdapter);
        approvesAdapter.notifyDataSetChanged();
    }
}
