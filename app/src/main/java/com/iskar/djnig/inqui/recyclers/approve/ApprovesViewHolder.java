package com.iskar.djnig.inqui.recyclers.approve;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class ApprovesViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.school)
    TextView school;

    @BindView(R.id.class_name)
    TextView class_name;

    @BindView(R.id.roll)
    TextView roll;

    public ApprovesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
