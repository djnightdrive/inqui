package com.iskar.djnig.inqui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.fragments.AddConceptFragment;
import com.iskar.djnig.inqui.fragments.AddStudentFragment;
import com.iskar.djnig.inqui.fragments.DesignSelectorFragment;
import com.iskar.djnig.inqui.fragments.HomePageFragment;
import com.iskar.djnig.inqui.fragments.SubmitDesignFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String USERNAME = "username";

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.navig_view)
    NavigationView navigationView;

    private SharedPreferences sPref;
    private View editUsername;
    private TextView username;
    private EditText usernameEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        ButterKnife.bind(this);
        sPref = getSharedPreferences("signin_temp", MODE_PRIVATE);
        Fragment homeFragment = new HomePageFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, homeFragment);
        transaction.commit();
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);
        LinearLayout logoutLayout = headerview.findViewById(R.id.nav_logout);
        editUsername = headerview.findViewById(R.id.nav_edit_username);
        setOnClickToView(createDialog());
        username = headerview.findViewById(R.id.nav_username);
        username.setText(sPref.getString(USERNAME, "username"));
        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sPref.edit();
                editor.putBoolean("sign_in_flag", false);
                editor.putString("token", null);
                editor.commit();
                drawer.closeDrawer(Gravity.START);
                Intent intent = new Intent(DrawerActivity.this, StartActivity.class);
                startActivity(intent);
            }
        });
    }

    private AlertDialog createDialog() {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.change_username, null);
        usernameEdit = view.findViewById(R.id.change_edittext);

        return new AlertDialog.Builder(this)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (isValidUsername(usernameEdit.getText().toString())) {
                            changeUsername(usernameEdit.getText().toString());
                        } else {
                            usernameEdit.setError("Input other username");
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setView(view)
                .setTitle("Input new username")
                .create();
    }

    private void changeUsername(String s) {
        username.setText(s);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(USERNAME, s);
        editor.apply();
    }

    private boolean isValidUsername(String username) {

        if (username.isEmpty()) {
            //add some other
            return false;
        }
        return true;
    }

    private void setOnClickToView(final AlertDialog dialog) {
        editUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int count = getFragmentManager().getBackStackEntryCount();
            if (count == 0) {
                super.onBackPressed();
            } else {
                getFragmentManager().popBackStack();
            }
        }
    }

    @OnClick(R.id.burger_button)
    void openDrawer() {
        drawer.openDrawer(Gravity.START);
    }

    void home() {
        HomePageFragment homePageFragment = new HomePageFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, homePageFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    void addConcept() {
        AddConceptFragment addConceptFragment = new AddConceptFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, addConceptFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    void addStudent() {
        AddStudentFragment addStudentFragment = new AddStudentFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, addStudentFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    void viewDesign() {
        DesignSelectorFragment designSelectorFragment = new DesignSelectorFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments,designSelectorFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    void uploadDesign() {
        SubmitDesignFragment submitDesignFragment = new SubmitDesignFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, submitDesignFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                home();
                break;
            case R.id.nav_add_concept:
                addConcept();
                break;
            case R.id.nav_add_student:
                addStudent();
                break;
            case R.id.nav_upload_design:
                uploadDesign();
                break;
            case R.id.nav_view_designs:
                viewDesign();
                break;
        }
        drawer.closeDrawer(Gravity.START);
        return false;
    }
}

