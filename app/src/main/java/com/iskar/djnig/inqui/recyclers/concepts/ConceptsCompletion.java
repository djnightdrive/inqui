package com.iskar.djnig.inqui.recyclers.concepts;

import java.util.ArrayList;

/**
 * Created by karasinboots on 25.11.2017.
 */

public interface ConceptsCompletion {
    void processFinish(ArrayList<Concepts> designsList);
}
