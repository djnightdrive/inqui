
package com.iskar.djnig.inqui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Class implements Parcelable
{

    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("id")
    @Expose
    private String id;
    public final static Parcelable.Creator<Class> CREATOR = new Creator<Class>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Class createFromParcel(Parcel in) {
            return new Class(in);
        }

        public Class[] newArray(int size) {
            return (new Class[size]);
        }

    };

    public Class(String section, String id) {
        this.section = section;
        this.id = id;
    }

    protected Class(Parcel in) {
        this.section = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Class() {
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(section);
        dest.writeValue(id);
    }

    public int describeContents() {
        return  0;
    }

}
