package com.iskar.djnig.inqui.recyclers.design;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.recyclers.RecyclerViewClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class DesignsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.design_name)
    TextView designName;
    @BindView(R.id.design)
    ImageView design;
    @BindView(R.id.approv)
    ImageView approve;
    @BindView(R.id.checkBox)
    CheckBox checkBox;
    @BindView(R.id.advanced)
    LinearLayout advanced;
    @BindView(R.id.approveRadio)
    RadioButton approveRadio;
    @BindView(R.id.rejectRadio)
    RadioButton rejectRadio;
    @BindView(R.id.inReviewRadio)
    RadioButton inReviewRadio;
    @BindView(R.id.comment)
    EditText comment;
    private static RecyclerViewClickListener itemListener = null;

    public DesignsViewHolder(View itemView, RecyclerViewClickListener itemListener) {
        super(itemView);
        this.itemListener = itemListener;
        ButterKnife.bind(this, itemView);
    }

}
