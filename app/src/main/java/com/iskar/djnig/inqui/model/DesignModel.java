
package com.iskar.djnig.inqui.model;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesignModel implements Parcelable {

    @SerializedName("designName")
    @Expose
    private String designName;
    @SerializedName("schoolId")
    @Expose
    private String schoolId;
    @SerializedName("classId")
    @Expose
    private String classId;
    @SerializedName("conceptId")
    @Expose
    private String conceptId;
    @SerializedName("filePath")
    @Expose
    private String filePath;
    @SerializedName("studentList")
    @Expose
    private List<String> studentList = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("designComments")
    @Expose
    private String designComments;
    @SerializedName("designId")
    @Expose
    private String designId;
    public final static Parcelable.Creator<DesignModel> CREATOR = new Creator<DesignModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DesignModel createFromParcel(Parcel in) {
            return new DesignModel(in);
        }

        public DesignModel[] newArray(int size) {
            return (new DesignModel[size]);
        }

    };

    protected DesignModel(Parcel in) {
        this.designName = ((String) in.readValue((String.class.getClassLoader())));
        this.schoolId = ((String) in.readValue((String.class.getClassLoader())));
        this.classId = ((String) in.readValue((String.class.getClassLoader())));
        this.conceptId = ((String) in.readValue((String.class.getClassLoader())));
        this.filePath = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.studentList, (java.lang.String.class.getClassLoader()));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.designComments = ((String) in.readValue((String.class.getClassLoader())));
        this.designId = ((String) in.readValue((String.class.getClassLoader())));
    }

    public String getDesignId() {
        return designId;
    }

    public void setDesignId(String designId) {
        this.designId = designId;
    }

    public DesignModel() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesignComments() {
        return designComments;
    }

    public void setDesignComments(String designComments) {
        this.designComments = designComments;
    }

    public String getDesignName() {
        return designName;
    }

    public void setDesignName(String designName) {
        this.designName = designName;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<String> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<String> studentList) {
        this.studentList = studentList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(designName);
        dest.writeValue(schoolId);
        dest.writeValue(classId);
        dest.writeValue(conceptId);
        dest.writeValue(filePath);
        dest.writeList(studentList);
    }

    public int describeContents() {
        return 0;
    }

}
