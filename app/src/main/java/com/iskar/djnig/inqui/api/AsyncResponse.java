package com.iskar.djnig.inqui.api;

/**
 * Created by karasinboots on 21.11.2017.
 */

public interface AsyncResponse {
    void processFinish(String apiAction, StringBuilder serverResponse);
}
