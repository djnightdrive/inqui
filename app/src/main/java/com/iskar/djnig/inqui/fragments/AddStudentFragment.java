package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.api.AsyncResponse;
import com.iskar.djnig.inqui.api.CallApi;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;
import com.iskar.djnig.inqui.utils.FragmentUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 26.11.2017.
 */

public class AddStudentFragment extends Fragment implements AsyncResponse {
    @BindView(R.id.add_st_name)
    EditText addStudentName;
    @BindView(R.id.add_st_last_name)
    EditText addStudentLastName;
    @BindView(R.id.add_student_roll_number)
    EditText addStudentRoll;
    @BindView(R.id.add_admission_number)
    EditText addStudentAdmissionNumber;
    @BindView(R.id.button_add_student)
    Button addStudent;
    @BindView(R.id.button_cancel_student)
    Button cancelStudent;
    @BindView(R.id.spinner_school)
    Spinner spinnerSchool;
    @BindView(R.id.spinner_class)
    Spinner spinnerClassroom;

    CallApi callApiAdd;
    MyProgressDialog progress;
    List<Schools> mSchools = new ArrayList<>();
    AddStudentFragment delegator = this;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_student, container, false);
        ButterKnife.bind(this, v);
        fillFromDb();
        String[] schoolsArray = getResources().getStringArray(R.array.spinner_schools);
        String[] classesArray = getResources().getStringArray(R.array.spinner_classes);
        spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                schoolsArray));
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                classesArray));
        addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (fieldsOk()) {
                        callApiAdd = new CallApi("add_student", getContext());
                        callApiAdd.delegate = delegator;
                        String studentName = addStudentName.getText().toString();
                        String studentLastName = addStudentLastName.getText().toString();
                        String studentRoll = addStudentRoll.getText().toString();
                        String admissionNumber = addStudentAdmissionNumber.getText().toString();
                        String studentClass = mSchools.get(spinnerSchool.getSelectedItemPosition()).getClassesId().get(spinnerClassroom.getSelectedItemPosition());
                        callApiAdd.execute(studentName, studentLastName, studentRoll, studentClass, admissionNumber);
                        showProgress();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cancelStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentUtil.changeFragment(new HomePageFragment(), AddStudentFragment.this);
            }
        });
        return v;
    }

    private boolean fieldsOk() {
        if (TextUtils.isEmpty(addStudentName.getText())) {
            addStudentName.setError("Fill field!");
            return false;
        }
        if (TextUtils.isEmpty(addStudentLastName.getText())) {
            addStudentLastName.setError("Fill field!");
            return false;
        }
        if (TextUtils.isEmpty(addStudentRoll.getText())) {
            addStudentRoll.setError("Fill field!");
            return false;
        }
        if(TextUtils.isEmpty(addStudentAdmissionNumber.getText())) {
            addStudentAdmissionNumber.setError("Fill field!");
            return false;
        }

        return true;
    }

    @Override
    public void processFinish(String apiAction, StringBuilder serverResponse) {
        switch (apiAction) {
            case "add_student":
                addStudent(serverResponse);
                progress.dismiss();
                break;
            case "get_schools":
                //  getSchool(serverResponse);
                break;
        }

    }

    private void fillFromDb() {
        mSchools.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Schools> schools = AppDatabase.getInstance(getContext()).getSchoolsDao().getAll();
                List<String> classNames;
                List<String> classIds;
                for (Schools tempSchool : schools) {
                    classNames = tempSchool.getClasses();
                    classIds = tempSchool.getClassesId();
                    mSchools.add(new Schools(tempSchool.getSchoolName(), tempSchool.getSchoolId(), classNames, classIds));
                }
                Log.d("AddStudent", "Size: " + mSchools.size());
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillSchoolSpinners();
            }
        };
        asyncTask.execute();
    }

    private void addStudent(StringBuilder serverResponse) {
        try {
            JSONObject json;
            String response;
            if (serverResponse.toString().contains("errorMessage")) {
                json = new JSONObject(serverResponse.toString());
                response = json.getString("errorMessage");
                showDialog(response);
            } else {
                if (serverResponse.toString().contains("message")) {
                    json = new JSONObject(serverResponse.toString());
                    response = json.getString("message");
                    showDialog(response);
                }
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    private void fillSchoolSpinners() {
        final ArrayList<String> schoolNames = new ArrayList<>();
        try {
            for (Schools sc : mSchools) {
                schoolNames.add(sc.getSchoolName());
            }
            spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    schoolNames.toArray(new String[schoolNames.size()])));
            spinnerSchool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fillClassesSpinner(mSchools.get(position).getClasses());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void fillClassesSpinner(List<String> classesList) {
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                classesList.toArray(new String[classesList.size()])));
        spinnerClassroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Adding student..");
        progress.show();
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add student")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
