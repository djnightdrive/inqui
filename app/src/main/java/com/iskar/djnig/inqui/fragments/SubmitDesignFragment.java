package com.iskar.djnig.inqui.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Concepts;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Class;
import com.iskar.djnig.inqui.model.Concept;
import com.iskar.djnig.inqui.model.School;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by djnig on 11/14/2017.
 */


public class SubmitDesignFragment extends Fragment {

    @BindView(R.id.spinner_school)
    Spinner spinnerSchool;

    @BindView(R.id.spinner_class)
    Spinner spinnerClassroom;

    @BindView(R.id.spinner_concept)
    Spinner spinnerConcept;

    @BindView(R.id.button_upload)
    Button buttonUpload;

    @BindView(R.id.add_design_edit)
    EditText designNameEdit;
    List<School> mSchools = new ArrayList<>();
    List<Concept> mConcepts = new ArrayList<>();
    MyProgressDialog progress;
    Context mContext;
    boolean isFirstRun = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.submit_design, container, false);
        ButterKnife.bind(this, v);
        mSchools.clear();
        mConcepts.clear();
        requestCameraPermission();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permissionGranted()) {
                    openCameraFragment();
                } else requestCameraPermission();
            }
        });

        String[] schools = getResources().getStringArray(R.array.spinner_schools);
        String[] classrooms = getResources().getStringArray(R.array.spinner_classes);
        String[] concepts = getResources().getStringArray(R.array.spinner_concepts);
        spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                schools));
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                classrooms));
        spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                concepts));
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("spinners")) {
            isFirstRun = false;
        }

            fillFromDb();
            fillConceptsFromDb();

    }


    private void fillFromDb() {
        mSchools.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Schools> schools = AppDatabase.getInstance(mContext).getSchoolsDao().getAll();
                List<String> classNames;
                List<String> classIds;

                for (Schools tempSchool : schools) {
                    List<Class> classList = new ArrayList<>();
                    classNames = tempSchool.getClasses();
                    classIds = tempSchool.getClassesId();
                    for (int i = 0; i < classNames.size(); i++) {
                        classList.add(new Class(classNames.get(i), classIds.get(i)));
                    }
                    mSchools.add(new School(tempSchool.getSchoolName(), tempSchool.getSchoolId(), classList));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillSchoolSpinners();
            }
        };
        asyncTask.execute();
    }

    private void fillConceptsFromDb() {
        mConcepts.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Concepts> concepts = AppDatabase.getInstance(mContext).getConceptsDao().getAll();
                for (Concepts concept : concepts) {
                    mConcepts.add(new Concept(concept.getConceptName(), concept.getConceptId()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillConceptSpinner();
            }
        };
        asyncTask.execute();
    }

    private void fillSchoolSpinners() {
        final ArrayList<String> schoolNames = new ArrayList<>();
        schoolNames.clear();
        try {
            for (School sc : mSchools) {
                schoolNames.add(sc.getName());
            }
            // Declaring an Adapter and initializing it to the data pump
            spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    schoolNames.toArray(new String[schoolNames.size()])));
            Bundle arguments = getArguments();
            if (arguments != null && arguments.containsKey("spinners")) {
                spinnerSchool.setSelection(arguments.getIntArray("spinners")[0]);
                fillClassesSpinner(mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses());
            }
            // Setting OnItemClickListener to the Spinner
            spinnerSchool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fillClassesSpinner(mSchools.get(position).getClasses());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void fillClassesSpinner(List<Class> classesList) {
        ArrayList<String> sections = new ArrayList<>();
        sections.clear();
        for (Class cl : classesList) {
            sections.add(cl.getSection());
        }
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                sections.toArray(new String[sections.size()])));
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("spinners"))
            spinnerClassroom.setSelection(arguments.getIntArray("spinners")[1]);
    }

    private Design getDesignData() {
        String design_name = designNameEdit.getText().toString();
        String design_image = null;
        String design_id = null;
        String classId = mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses()
                .get(spinnerClassroom.getSelectedItemPosition()).getId();
        String schoolId = mSchools.get(spinnerSchool.getSelectedItemPosition()).getId();
        String conceptId = mConcepts.get(spinnerConcept.getSelectedItemPosition()).getId();
        String approved = "";
        String comment = "";
        int sync_flag = 0;
        return new Design(design_name, design_image, sync_flag, design_id, conceptId, null,
                classId, schoolId, approved, comment);
    }

    private void openCameraFragment() {
        try {
            if (fieldsOk()) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("design", getDesignData());
                bundle.putString("conceptId", mConcepts.get(spinnerConcept.getSelectedItemPosition()).getId());
                bundle.putString("schoolId", mSchools.get(spinnerSchool.getSelectedItemPosition()).getId());
                bundle.putString("classid", mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses()
                        .get(spinnerClassroom.getSelectedItemPosition()).getId());
                bundle.putString("schoolName", mSchools.get(spinnerSchool.getSelectedItemPosition()).getName());
                bundle.putString("className", mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses()
                        .get(spinnerClassroom.getSelectedItemPosition()).getSection());
                bundle.putString("conceptName", mConcepts.get(spinnerConcept.getSelectedItemPosition()).getName());
                bundle.putString("designName", designNameEdit.getText().toString());
                int[] spinners = {spinnerSchool.getSelectedItemPosition(), spinnerClassroom.getSelectedItemPosition(), spinnerConcept.getSelectedItemPosition()};
                bundle.putIntArray("spinners", spinners);
                CameraFragment cameraFragment = new CameraFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                cameraFragment.setArguments(bundle);
                transaction.replace(R.id.root_for_fragments, cameraFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean fieldsOk() {
        if (TextUtils.isEmpty(designNameEdit.getText())) {
            designNameEdit.setError("Fill field!");
            return false;
        }
        return true;
    }

    private void requestCameraPermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(getActivity(), perms))
            EasyPermissions.requestPermissions(this, "Permission needed to perform design capture!", 1, perms);
    }

    private boolean permissionGranted() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        return EasyPermissions.hasPermissions(getActivity(), perms);
    }

    private void fillConceptSpinner() {
        try {
            ArrayList<String> concepts = new ArrayList<>();
            concepts.clear();
            for (Concept cl : mConcepts) {
                concepts.add(cl.getName());
            }
            spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    concepts.toArray(new String[concepts.size()])));
            Bundle arguments = getArguments();
            if (arguments != null && arguments.containsKey("spinners")) {
                spinnerConcept.setSelection(arguments.getIntArray("spinners")[2]);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
