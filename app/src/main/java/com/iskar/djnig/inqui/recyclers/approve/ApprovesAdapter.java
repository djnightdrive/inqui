package com.iskar.djnig.inqui.recyclers.approve;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iskar.djnig.inqui.R;

import java.util.List;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class ApprovesAdapter extends RecyclerView.Adapter<ApprovesViewHolder> {
    private List<Approves> approvesList;

    public ApprovesAdapter(List<Approves> approvesList) {
        this.approvesList = approvesList;
    }

    @Override
    public ApprovesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.approve_item, parent, false);
        return new ApprovesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ApprovesViewHolder holder, final int position) {
        Approves item = approvesList.get(position);
        holder.name.setText(item.getStudentName());
        holder.class_name.setText(item.getClassName());
        holder.school.setText(item.getSchoolName());
        holder.roll.setText(item.getRollNumber());
    }

    @Override
    public int getItemCount() {
        if (approvesList != null)
            return approvesList.size();
        else return 0;
    }
}
