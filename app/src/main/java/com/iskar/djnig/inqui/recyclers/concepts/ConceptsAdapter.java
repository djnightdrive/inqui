package com.iskar.djnig.inqui.recyclers.concepts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iskar.djnig.inqui.R;

import java.util.List;

/**
 * Created by karasinboots on 07.12.2017.
 */

public class ConceptsAdapter extends RecyclerView.Adapter<ConceptsViewHolder> {
    private List<Concepts> conceptsList;

    public ConceptsAdapter(List<Concepts> conceptsList) {
        this.conceptsList = conceptsList;
    }

    @Override
    public ConceptsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.concepts_item, parent, false);
        return new ConceptsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ConceptsViewHolder holder, final int position) {
        Concepts item = conceptsList.get(position);
        holder.conceptName.setText(item.getConceptName());
    }

    @Override
    public int getItemCount() {
        return conceptsList.size();
    }
}


