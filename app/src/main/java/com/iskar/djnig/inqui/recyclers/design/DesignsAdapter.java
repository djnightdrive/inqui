package com.iskar.djnig.inqui.recyclers.design;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.common.io.Files;
import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.recyclers.RecyclerViewClickListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class DesignsAdapter extends RecyclerView.Adapter<DesignsViewHolder> {
    private static RecyclerViewClickListener itemListener;
    private List<Designs> designsList;
    private Context context;
    private Activity activity;
    private int multi;
    private File file;

    private GoogleCredential googleCredential;
    private Drive googleService;
    private static final String[] SCOPES = {DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE, DriveScopes.DRIVE_FILE};


    public DesignsAdapter(List<Designs> designsList, Context context, RecyclerViewClickListener itemListener, int multi, Activity activity) {
        this.designsList = designsList;
        this.context = context;
        DesignsAdapter.itemListener = itemListener;
        this.multi = multi;
        this.activity = activity;
        initGoogleDrive();
    }

    private void initGoogleDrive() {
        try {
            googleCredential = GoogleCredential.fromStream(context.getResources().openRawResource(R.raw.inquilab2b9e83128b90)).createScoped(Arrays.asList(SCOPES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        googleService = new com.google.api.services.drive.Drive.Builder(
                transport, jsonFactory, googleCredential)
                .setApplicationName("InquilabServiceAccount")
                .build();
    }

    public List<Designs> getDesignsList() {
        return designsList;
    }

    public void setDesignsList(List<Designs> designsList) {
        this.designsList = designsList;
    }

    @Override
    public DesignsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_design_item, parent, false);
        return new DesignsViewHolder(v, itemListener);
    }

    @Override
    public void onBindViewHolder(final DesignsViewHolder holder, final int position) {
        final Designs item = designsList.get(position);//TODO: CHECKBOX HERE
        switch (multi) {
            case 0:
                holder.checkBox.setVisibility(View.INVISIBLE);
                holder.advanced.setVisibility(View.GONE);
                break;
            case 1:
                holder.advanced.setVisibility(View.GONE);
                if (!item.getStatus().equals("Submitted"))
                    holder.checkBox.setVisibility(View.INVISIBLE);
                else holder.checkBox.setVisibility(View.VISIBLE);
                if(item.getStatus().equals("In review"))
                    holder.checkBox.setVisibility(View.VISIBLE);
                holder.checkBox.setChecked(designsList.get(holder.getAdapterPosition()).isCheckedStatus());
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        designsList.get(holder.getAdapterPosition()).setCheckedStatus(b);
                    }
                });
                break;
            case 2:
                holder.comment.setTag(position);
                holder.comment.setText(item.getDesignComments());
                holder.comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        designsList.get(holder.getAdapterPosition()).setDesignComments(editable.toString());
                    }
                });
                //holder.approveRadio.setTag(position);
               /* if (item.getReviewStatus())
                    holder.approveRadio.setChecked(true);
                holder.approveRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked())
                            item.setReviewStatus(true);
                    }
                });
                holder.rejectRadio.setTag(position);
                if (!item.getReviewStatus())
                    holder.rejectRadio.setChecked(true);
                holder.rejectRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked())
                            item.setReviewStatus(false);
                    }
                });*/
                holder.approveRadio.setTag(position);
                if (item.getStatus().equals("Approve"))
                    holder.approveRadio.setChecked(true);
                holder.approveRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked())
                            item.setStatus("Approve");
                    }
                });
                holder.rejectRadio.setTag(position);
                if (item.getStatus().equals("Rejected"))
                    holder.rejectRadio.setChecked(true);
                holder.rejectRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked())
                            item.setStatus("Rejected");
                    }
                });
                holder.inReviewRadio.setTag(position);
                if(item.getStatus().equals("In review"))
                    holder.inReviewRadio.setChecked(true);
                holder.inReviewRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked())
                            item.setStatus("In review");
                    }
                });
                holder.checkBox.setChecked(item.isCheckedStatus());
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        designsList.get(holder.getAdapterPosition()).setCheckedStatus(b);
                    }
                });
                break;
        }
        holder.designName.setTag(position);
        holder.designName.setText(item.getDesignName());
        switch (item.getStatus()) {
            case "Approve":
                Picasso.with(context).load(R.drawable.approved).resize(300, 300).centerInside().into(holder.approve);
                break;
            case "Reject":
                Picasso.with(context).load(R.drawable.rejected).resize(300, 300).centerInside().into(holder.approve);
                break;
            case "In review":
                Picasso.with(context).load(R.drawable.in_review).resize(300, 300).centerInside().into(holder.approve);
                break;
            default:
                Picasso.with(context).load(R.drawable.circle).resize(300, 300).centerInside().into(holder.approve);
                break;
        }

        try {
            final String link = getLink(item.getDesignId());
            if (link != null) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        try {
                            String title = item.getDesignImage().substring(item.getDesignImage().lastIndexOf('/') + 1/*, item.getDesignImage().length()-5*/);
                            googleService.files().get(link)
                                    //.setQ("name contains '" + title + "'")
                                    .executeMediaAndDownloadTo(outputStream);
                            byte[] bitmapdata = outputStream.toByteArray();
                            file = File.createTempFile("temp", "jpeg");
                            Files.write(bitmapdata, file);

                            //Picasso.with(context).load(link).resize(300, 300).centerCrop().into(holder.design);
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Picasso.with(context).load(file).resize(300, 300).centerCrop().into(holder.design);
                                    //Picasso.with(context).load(R.drawable.image_not_available_300).resize(300, 300).into(holder.design);
                                }
                            });
                            Log.e("link adapter", link);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();

            } else
                Picasso.with(context).load(R.drawable.image_not_available_300).resize(300, 300).into(holder.design);
            holder.design.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemListener.recyclerViewListClicked(null, position, link);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getLink(final String designId) throws InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Callable<String> callable = new Callable<String>() {
            @Override
            public synchronized String call() throws InterruptedException {
                String link = AppDatabase.getInstance(context).getLinksDao().getByDesignId(designId).getDropLink();
                Log.e("link adapter", link + "");
                return link;
            }
        };
        String link;
        try {
            link = executor.submit(callable).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
        executor.shutdown();
        return link;
    }

    @Override
    public int getItemCount() {
        return designsList.size();
    }

}
