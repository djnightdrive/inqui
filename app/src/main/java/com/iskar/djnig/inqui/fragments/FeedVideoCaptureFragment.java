package com.iskar.djnig.inqui.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.StaticConstants;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Feedback;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.FeedbackBody;
import com.iskar.djnig.inqui.model.Response;
import com.iskar.djnig.inqui.onlineStorage.GoogleDrive;
import com.iskar.djnig.inqui.onlineStorage.GoogleDriveAsyncResponse;
import com.iskar.djnig.inqui.utils.NetworkManager;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Facing;
import com.otaliastudios.cameraview.SessionType;
import com.otaliastudios.cameraview.VideoQuality;
import com.otaliastudios.cameraview.WhiteBalance;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by karasinboots on 24.11.2017.
 */

public class FeedVideoCaptureFragment extends Fragment implements GoogleDriveAsyncResponse {
    @BindView(R.id.feedbackCamera)
    CameraView mCameraView;

    @BindView(R.id.take_video)
    ImageButton take_video;

    @BindView(R.id.toggle_camera)
    ImageButton changeCamera;

    @BindView(R.id.button_send_video)
    Button sendVideo;

    @BindView(R.id.button_cancel_video)
    Button cancel;
    boolean captureInProgress = false;
    MyProgressDialog progress;

    //Dropbox dropbox;
    GoogleDrive googleDrive;
    boolean taken = false;
    String mFilePath;
    private String conceptId;
    private String studentId;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.feed_video, container, false);
        ButterKnife.bind(this, v);
        setupCamera();
        sharedPreferences = getContext().getSharedPreferences("network", Context.MODE_PRIVATE);
        conceptId = getArguments().getString("conceptId");
        studentId = getArguments().getString("studentId");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });

        changeCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (captureInProgress) {
                    return;
                }

                if (mCameraView.getFacing() == Facing.FRONT) {
                    mCameraView.setFacing(Facing.BACK);
                } else {
                    mCameraView.setFacing(Facing.FRONT);
                }

            }
        });
        sendVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVideo();
            }
        });
        take_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (captureInProgress) {
                    mCameraView.stopCapturingVideo();
                    captureInProgress = false;
                } else {
                    File tempFile = new File(Environment.getExternalStorageDirectory() + "/Inqui/Feedback/Video");
                    tempFile.mkdirs();
                    File file = new File(tempFile.getPath(),
                            String.valueOf(System.currentTimeMillis()) + ".mp4");
                    // Full version
                    mCameraView.startCapturingVideo(file);
                    mCameraView.addCameraListener(new CameraListener() {
                        @Override
                        public void onVideoTaken(File video) {
                            Toast.makeText(getContext(), "Recording complete", Toast.LENGTH_SHORT).show();
                            mFilePath = video.getPath();
                            taken = true;
                        }
                    });
                    Toast.makeText(getContext(), "Recording started", Toast.LENGTH_SHORT).show();
                    captureInProgress = true;
                }
            }
        });
        return v;
    }

    private void setupCamera() {
        mCameraView.setSessionType(SessionType.VIDEO);
        mCameraView.setVideoQuality(VideoQuality.HIGHEST);
        mCameraView.setWhiteBalance(WhiteBalance.AUTO);

    }

    @Override
    public void onResume() {
        super.onResume();
        mCameraView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        mCameraView.stop();
    }

    private void putToDb(final Feedback feedback) {
        new Thread(new Runnable() {
            public void run() {
                AppDatabase.getInstance(getContext()).getFeedbackDao().insertFeedback(feedback);
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCameraView.destroy();
    }

    private void sendVideo() {
        if (taken) {
            if (!NetworkManager.isConnected(getActivity())) {
                putToDb(new Feedback("audio", mFilePath, ""
                        , getArguments().getString("studentId"),
                        getArguments().getString("conceptId"), 0));
                editor = sharedPreferences.edit();
                editor.putBoolean("sync", true);
                editor.apply();
                showSorry("Connection problem! Feedback will be uploaded when Internet appear!");
            } else {
                editor = sharedPreferences.edit();
                editor.putBoolean("sync", false);
                editor.apply();
                /*dropbox = new Dropbox();
                dropbox.delegate = FeedVideoCaptureFragment.this;*/
                googleDrive = new GoogleDrive();
                googleDrive.delegate = FeedVideoCaptureFragment.this;
                initDropbox();
                showProgress();
                //dropbox.execute(mFilePath, StaticConstants.FEED_VIDEO);
                googleDrive.execute(mFilePath, StaticConstants.FEED_VIDEO);
            }
            taken = false;
        }
    }

    private void showSorry(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Feedback uploading")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
    }

    @Override
    public void processFinish(String path) {
        if (path != null) {
            FeedbackBody body = new FeedbackBody();
            body.setStudentId(studentId);
            body.setDesignId("");
            body.setConceptId(conceptId);
            body.setFeedBackType("video");
            body.setFeedBackPath(path);
            submitFeed(body);
            progress.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Feedback uploading")
                    .setMessage("Feedback was successfully uploaded to Google Drive")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    HomePageFragment homePageFragment = new HomePageFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.root_for_fragments, homePageFragment);
                                    transaction.commit();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            editor = sharedPreferences.edit();
            editor.putBoolean("sync", false);
            editor.apply();
            deleteFile();
        } else {
            editor = sharedPreferences.edit();
            editor.putBoolean("sync", true);
            editor.apply();
        }
    }

    private void deleteFile() {
        File file = new File(mFilePath);
        file.delete();
    }

    private void showProgress() {
        progress = new MyProgressDialog(getContext(), "Uploading video feedback..");
        progress.show();
    }

    private void initDropbox() {
            //dropbox.init();
            googleDrive.initGoogleDrive(getContext());

    }

    private void submitFeed(FeedbackBody body) {
        HttpHelper.getApi().submitFeedback(body).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("FeedBack Successfully added via ... video mode")) {
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }
}
