package com.iskar.djnig.inqui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.api.AsyncResponse;
import com.iskar.djnig.inqui.api.CallApi;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends AppCompatActivity implements AsyncResponse {

    @BindView(R.id.login_email)
    TextInputEditText loginEmail;
    @BindView(R.id.login_pass)
    TextInputEditText loginPass;
    @BindView(R.id.button_sign_in)
    Button buttonSignIn;
    SharedPreferences sPref;
    String mUsername;
    MyProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        sPref = getSharedPreferences("signin_temp", MODE_PRIVATE);
        if (sPref.getBoolean("sign_in_flag", false)) {
            Intent intent = new Intent(SignInActivity.this, DrawerActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.button_sign_in)
    public void onClick(View v) {
        if (isValidEmail(loginEmail.getText()) && loginPass.getText().length() > 0) {
            mUsername = loginEmail.getText().toString();
            CallApi callApi = new CallApi("login", loginEmail.getText().toString(), loginPass.getText().toString(), getApplicationContext());
            callApi.delegate = this;
            callApi.execute();
            showProgress();
        } else {
            loginEmail.setError("Not valid email or password");
        }
    }

    private void showProgress() {
        progress = new MyProgressDialog(this, "Login process...");
        progress.show();
    }

    public boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void processFinish(String apiAction, StringBuilder result) {
        String token = getToken(result);
        if (token != null) {
            SharedPreferences.Editor editor = sPref.edit();
            editor.putBoolean("sign_in_flag", true);
            editor.putString("token", token);
            editor.putString("username", mUsername);
            editor.apply();
            progress.dismiss();
        } else {
            progress.dismiss();
            Toast.makeText(getApplicationContext(), "Login fail!", Toast.LENGTH_SHORT).show();
        }

        if (sPref.getBoolean("sign_in_flag", false)) {
            Intent intent = new Intent(SignInActivity.this, DrawerActivity.class);
            startActivity(intent);
        }
    }

    private String getToken(StringBuilder result) {
        try {
            String token;
            if (result.toString().contains("\"token\"")) {
                JSONObject json = new JSONObject(result.toString());
                token = json.getString("token");
                return token;
            } else {
                JSONObject json = new JSONObject(result.toString());
                token = json.getString("errorMessage");
                Toast.makeText(getApplicationContext(), token, Toast.LENGTH_LONG).show();
                return null;
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No Internet or server is not available", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        }
    }
}

