package com.iskar.djnig.inqui.database;

import android.arch.persistence.room.TypeConverter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by karasinboots on 30.11.2017.
 */

public class ListStringConverter {
    @TypeConverter
    public static List<String> toList(String string) {
        List<String> list = Arrays.asList(string.split(","));
        return list;
    }

    @TypeConverter
    public static String toString(List<String> list) {
        String string = "";
        for (String substring : list) {
            string += substring + ",";
        }
        return string;
    }
}
