package com.iskar.djnig.inqui.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by karasinboots on 30.11.2017.
 */
@Entity
public class Concepts {
    @PrimaryKey(autoGenerate = true)
    private
    int id;

    public Concepts() {
    }

    private String conceptName;
    private String conceptId;

    public Concepts(String name, String id) {
        this.conceptName = name;
        this.conceptId = id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConceptName() {
        return conceptName;
    }

    public void setConceptName(String conceptName) {
        this.conceptName = conceptName;
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }


}

