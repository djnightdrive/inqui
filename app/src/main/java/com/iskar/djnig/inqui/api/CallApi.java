package com.iskar.djnig.inqui.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by karasinboots on 20.11.2017.
 */

public class CallApi extends AsyncTask<String, Long, StringBuilder> {

    private static final String ACTION_GET_CONCEPTS = "concepts/all";
    private static final String ACTION_ADD_STUDENT = "student/add";
    private static final String METHOD_PUT = "PUT";
    private static final String METHOD_POST = "POST";
    private static final String METHOD_GET = "GET";
    private static final String BASE_URL = "http://default-environment.2wqimehrvh.ap-south-1.elasticbeanstalk.com/";
    private static final String CONTENT_TYPE = "application/json";
    private static final String ACTION_LOGIN = "login";
    private static final String ACTION_GET_SCHOOLS = "schools/all";
    private static final String AUTH_TYPE = "Bearer";
    private static final String ACTION_ADD_CONCEPT = "concept/add";

    private boolean mSuccess = false;
    private String mEmail;
    private String mPass;
    private String mToken;
    private String apiAction;
    private int mDesignId;
    private String mDesignName;
    private String mDesignImage;
    private String mStudentName;
    private Context mContext;
    public AsyncResponse delegate = null;


    public CallApi(String action, String email, String password, Context context) {
        this.apiAction = action;
        this.mEmail = email;
        this.mPass = password;
        this.mContext = context;
    }

    public CallApi(String action, int id, String design_name, String design_image, String student_name, Context context) {
        this.apiAction = action;
        this.mDesignId = id;
        this.mDesignName = design_name;
        this.mDesignImage = design_image;
        this.mStudentName = student_name;
        this.mContext = context;
    }

    public CallApi(String action, Context context) {
        this.apiAction = action;
        this.mContext = context;
    }

    protected StringBuilder doInBackground(String... arguments) {
        return connect(apiAction, arguments);
    }

    protected void onPostExecute(StringBuilder callApiResult) {
        delegate.processFinish(apiAction, callApiResult);
    }


    private StringBuilder connect(String action, String... arguments) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("signin_temp", Context.MODE_PRIVATE);
        mToken = sharedPreferences.getString("token", null);
        switch (action) {
            case "login":
                return getLoginResponse();
            case "put_design":
                return null;
            case "get_schools":
                return getSchoolsResponse();
            case "get_concepts":
                return getConceptsResponse();
            case "add_concept":
                return addConcept(arguments[0]);
            case "add_student":
                return addStudent(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
        }
        return null;
    }


    private StringBuilder getLoginResponse() {
        JSONObject json = new JSONObject();
        try {
            json.put("userName", mEmail);
            json.put("password", mPass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String data = json.toString();
        System.out.println(data);
        String urlString = BASE_URL + ACTION_LOGIN; // URL to call
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(METHOD_POST);
            urlConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(data);
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();
            return getResponse(urlConnection);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private StringBuilder getSchoolsResponse() {
        String urlString = BASE_URL + ACTION_GET_SCHOOLS; // URL to call
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(METHOD_GET);
            urlConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
            urlConnection.setRequestProperty("Authorization", AUTH_TYPE + " " + mToken);
            urlConnection.connect();
            return getResponse(urlConnection);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private StringBuilder addStudent(String firstName, String lastName, String stRoll, String classId, String admissionNumber) {
        String urlString = BASE_URL + ACTION_ADD_STUDENT; // URL to call
        JSONObject json = new JSONObject();
        try {
            json.put("firstName", firstName);
            json.put("lastName", lastName);
            json.put("rollNumber", stRoll);
            json.put("classId", classId);
            json.put("admissionNumber", admissionNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String data = json.toString();
        Log.e("add_student", data);
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(METHOD_PUT);
            urlConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
            urlConnection.setRequestProperty("Authorization", AUTH_TYPE + " " + mToken);
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(data);
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();
            return getResponse(urlConnection);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private StringBuilder getConceptsResponse() {
        String urlString = BASE_URL + ACTION_GET_CONCEPTS; // URL to call
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(METHOD_GET);
            urlConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
            urlConnection.setRequestProperty("Authorization", AUTH_TYPE + " " + mToken);
            urlConnection.connect();
            return getResponse(urlConnection);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private StringBuilder addConcept(String conceptName) {
        String urlString = BASE_URL + ACTION_ADD_CONCEPT; // URL to call
        JSONObject json = new JSONObject();
        try {
            json.put("conceptName", conceptName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String data = json.toString();
        Log.e("add_student", data);
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(METHOD_PUT);
            urlConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
            urlConnection.setRequestProperty("Authorization", AUTH_TYPE + " " + mToken);
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(data);
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();
            return getResponse(urlConnection);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private StringBuilder getResponse(HttpURLConnection urlConnection) {
        String line;
        StringBuilder response = new StringBuilder();
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while ((line = br.readLine()) != null) {
                response.append(line);
            }
            System.out.println(response.toString());
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
