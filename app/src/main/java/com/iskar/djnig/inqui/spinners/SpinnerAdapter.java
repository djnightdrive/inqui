package com.iskar.djnig.inqui.spinners;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class SpinnerAdapter extends ArrayAdapter {
    private Context context;
    private String[] objects;
    public SpinnerAdapter(Context context, int textViewResourceId,
                          String[] objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.objects = objects;
    }

    private View getCustomView(int position, View convertView,
                               ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View layout = inflater.inflate(R.layout.spinner_item, parent, false);
        TextView itemText = (TextView) layout
                .findViewById(R.id.itemText);
        itemText.setText(objects[position]);

        return layout;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}
