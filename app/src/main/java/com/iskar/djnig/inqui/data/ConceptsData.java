package com.iskar.djnig.inqui.data;

import java.util.ArrayList;

/**
 * Created by karasinboots on 26.11.2017.
 */

public class ConceptsData {
    ArrayList<ConceptsData> conceptsData = new ArrayList<>();

    private String conceptName;
    private String conceptId;

    public ConceptsData(String conceptName, String conceptId) {
        this.conceptName = conceptName;
        this.conceptId = conceptId;
    }

    public ConceptsData() {
    }
    public void clear() {
        conceptsData.clear();
    }
    public void addConceptsData(ConceptsData conceptsData) {
        this.conceptsData.add(conceptsData);
    }

    public ArrayList<ConceptsData> getConcepts() {
        return conceptsData;
    }

    public String getConceptName() {
        return conceptName;
    }

    public String getConceptId() {
        return conceptId;
    }
}
