package com.iskar.djnig.inqui.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.iskar.djnig.inqui.database.entities.Feedback;

import java.util.List;

/**
 * Created by karasinboots on 14.11.2017.
 */
@Dao
public interface FeedbackDAO {

    @Query("SELECT * FROM feedback WHERE design_id LIKE :productId")
    List<Feedback> getAll(int productId);

    @Query("SELECT * FROM feedback WHERE feedback_sync_flag LIKE '0'")
    List<Feedback> getUnsynced();

    @Insert
    void insertFeedback(Feedback feedback);

    @Update
    void updateFeedback(Feedback feedback);

    @Delete
    void deleteFeedback(Feedback feedback);
}
