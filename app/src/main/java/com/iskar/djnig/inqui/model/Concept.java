package com.iskar.djnig.inqui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Concept implements Parcelable {

    @SerializedName("conceptName")
    @Expose
    private String name;
    @SerializedName("conceptId")
    @Expose
    private String id;
    public final static Parcelable.Creator<Concept> CREATOR = new Creator<Concept>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Concept createFromParcel(Parcel in) {
            return new Concept(in);
        }

        public Concept[] newArray(int size) {
            return (new Concept[size]);
        }

    };

    public Concept(String name, String id) {
        this.name = name;
        this.id = id;
    }

    protected Concept(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Concept() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(id);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Concept.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Concept other = (Concept) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }
}

