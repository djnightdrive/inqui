package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Concepts;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.database.entities.Students;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Class;
import com.iskar.djnig.inqui.model.Concept;
import com.iskar.djnig.inqui.model.School;
import com.iskar.djnig.inqui.model.Student;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 27.11.2017.
 */

public class ConceptChooserFragment extends Fragment {
    @BindView(R.id.button_view_info)
    Button viewInfo;

    @BindView(R.id.spinner_school)
    Spinner spinnerSchool;

    @BindView(R.id.spinner_class)
    Spinner spinnerClassroom;
    @BindView(R.id.spinner_students)
    Spinner spinnerStudents;
    @BindView(R.id.spinner_concept)
    Spinner spinnerConcept;

    ArrayList<String> rollNumbers = new ArrayList<>();
    MyProgressDialog progress;
    List<School> mSchools = new ArrayList<>();
    List<Student> mStudents = new ArrayList<>();
    Context mContext;
    List<Concept> mConcepts = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.select_concept, container, false);
        ButterKnife.bind(this, v);
        String[] schoolsArray = getResources().getStringArray(R.array.spinner_schools);
        String[] classesArray = getResources().getStringArray(R.array.spinner_classes);
        String[] studentsArray = getResources().getStringArray(R.array.spinner_students);
        String[] concepts = getResources().getStringArray(R.array.spinner_concepts);
        spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                schoolsArray));
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                classesArray));
        spinnerStudents.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                studentsArray));
        spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                concepts));
        viewInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bundle bundle = new Bundle();
                    bundle.putString("studentId", rollNumbers.get(spinnerStudents.getSelectedItemPosition()));
                    bundle.putString("conceptId", mConcepts.get(spinnerConcept.getSelectedItemPosition()).getId());
                    FeedVariantsFragment feedVariantsFragment = new FeedVariantsFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    feedVariantsFragment.setArguments(bundle);
                    transaction.replace(R.id.root_for_fragments, feedVariantsFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        fillFromDb();
        fillConceptsFromDb();
    }

      private void fillConceptsFromDb() {
        mConcepts.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Concepts> concepts = AppDatabase.getInstance(mContext).getConceptsDao().getAll();
                for (Concepts concept : concepts) {
                    mConcepts.add(new Concept(concept.getConceptName(), concept.getConceptId()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillConceptSpinner();
            }
        };
        asyncTask.execute();
    }
    private void fillConceptSpinner() {
        try {
            ArrayList<String> concepts = new ArrayList<>();
            concepts.clear();
            for (Concept cl : mConcepts) {
                concepts.add(cl.getName());
            }
            spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    concepts.toArray(new String[concepts.size()])));
            Bundle arguments = getArguments();
            if (arguments != null && arguments.containsKey("spinners")) {
                spinnerConcept.setSelection(arguments.getIntArray("spinners")[2]);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    private void fillFromDb() {
        mSchools.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Schools> schools = AppDatabase.getInstance(mContext).getSchoolsDao().getAll();
                List<String> classNames;
                List<String> classIds;

                for (Schools tempSchool : schools) {
                    List<Class> classList = new ArrayList<>();
                    classNames = tempSchool.getClasses();
                    classIds = tempSchool.getClassesId();
                    for (int i = 0; i < classNames.size(); i++) {
                        classList.add(new Class(classNames.get(i), classIds.get(i)));
                    }
                    mSchools.add(new School(tempSchool.getSchoolName(), tempSchool.getSchoolId(), classList));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillSchoolSpinners();
            }
        };
        asyncTask.execute();
    }

    private void fillSchoolSpinners() {
        final ArrayList<String> schoolNames = new ArrayList<>();
        schoolNames.clear();
        try {
            for (School sc : mSchools) {
                schoolNames.add(sc.getName());
            }
            // Declaring an Adapter and initializing it to the data pump
            spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    schoolNames.toArray(new String[schoolNames.size()])));
            // Setting OnItemClickListener to the Spinner
            spinnerSchool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fillClassesSpinner(mSchools.get(position).getClasses());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void fillClassesSpinner(List<Class> classesList) {
        ArrayList<String> sections = new ArrayList<>();
        sections.clear();
        for (Class cl : classesList) {
            sections.add(cl.getSection());
        }
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                sections.toArray(new String[sections.size()])));
        spinnerClassroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fillStudentsFromDb(mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses().get(i).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void fillStudentsFromDb(final String classId) {
        mStudents.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Students> tempList = AppDatabase.getInstance(mContext).getStudentsDao().getStudentByClass(classId);
                for (Students students : tempList) {
                    mStudents.add(new Student(students.getFirstName(), students.getLastName(),
                            students.getRollId(), students.getClassId(), students.getAdmissionNumber()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupSpinner(mStudents);
            }
        };
        try {
            asyncTask.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void setupSpinner(List<Student> students) {
        try {
            rollNumbers.clear();
            String studentsList[] = new String[students.size()];
            for (int i = 0; i < students.size(); i++) {
                studentsList[i] = students.get(i).getFirstName() + " " + students.get(i).getLastName();
                rollNumbers.add(students.get(i).getRollNumber());
            }
            spinnerStudents.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    studentsList));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
