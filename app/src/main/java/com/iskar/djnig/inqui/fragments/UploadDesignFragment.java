package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Students;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.DesignModel;
import com.iskar.djnig.inqui.model.Student;
import com.iskar.djnig.inqui.onlineStorage.GoogleDriveAsyncResponse;
import com.iskar.djnig.inqui.onlineStorage.GoogleDrive;
import com.iskar.djnig.inqui.spinners.MultiSelectionSpinner;
import com.iskar.djnig.inqui.utils.NetworkManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UploadDesignFragment extends Fragment implements GoogleDriveAsyncResponse {
    public static final String DESIGN = "design";

    @BindView(R.id.select_students)
    MultiSelectionSpinner spinnerStudents;

    @BindView(R.id.button_upload)
    Button uploadButton;

    @BindView(R.id.button_cancel)
    Button buttonCancel;

    @BindView(R.id.myZoomageView)
    ImageView designPreview;

    Design design;
    //Dropbox dropbox;
    GoogleDrive googleDrive;
    MyProgressDialog progress;

    String studentsRoll[];
    String classId;
    List<String> selectedRolls;
    String conceptId;
    String schoolId;
    List<Student> mStudents = new ArrayList<>();
    Context mContext;
    Uri designUri;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.upload_design, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = view.getContext();

        //dropbox = new Dropbox();
        googleDrive = new GoogleDrive();
        sharedPreferences = mContext.getSharedPreferences("network", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean("sync", false);
        editor.apply();
        selectedRolls = new ArrayList();
        //dropbox.delegate = this;
        googleDrive.delegate = this;
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });
        conceptId = getArguments().getString("conceptId");
        schoolId = getArguments().getString("schoolId");
        design = (Design) getArguments().getSerializable("design");
        classId = design.getClassId();
        try {
            designUri = Uri.parse(getArguments().getString("design_uri"));
            designUri = moveFile(designUri);
            design.setDesign_image(getDesignPath(designUri.getPath()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        fillFromDb();
        setPreview(designUri);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Integer> selectedItems = spinnerStudents.getSelectedIndicies();
                if (selectedItems.size() < 1) {
                    Toast.makeText(mContext, "Select at least one student.", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (int i = 0; i < selectedItems.size(); i++) {
                    selectedRolls.add(mStudents.get(selectedItems.get(i)).getRollNumber());
                    Log.e("selected students", mStudents.get(selectedItems.get(i)).getRollNumber());
                }
                design.setRollIds(selectedRolls);
                if (NetworkManager.isConnected(mContext)) {
                    editor = sharedPreferences.edit();
                    editor.putBoolean("sync", false);
                    editor.apply();
                    design.setDesign_sync_flag(0);
                    putToDB(design);
                    Log.e("design path", design.getDesign_image());
                    showProgress();
                    //initDropbox();//
                    initGoogleDrive();
                    //dropbox.execute(designUri.toString(), DESIGN);//
                    googleDrive.execute(designUri.toString(), DESIGN);
                } else {
                    design.setDesign_sync_flag(0);
                    putToDB(design);
                    editor = sharedPreferences.edit();
                    editor.putBoolean("sync", true);
                    editor.apply();
                    Log.e("design path", design.getDesign_image());
                    showSorry("Your design is saved locally. It will be uploaded when the Internet appears.");
                }
            }
        });
    }

    private void initGoogleDrive() {
        googleDrive.initGoogleDrive(getContext());
    }

    private String getDesignPath(String path) {
        String[] folders = path.split("/");
        path = folders[folders.length - 2] + "/" + folders[folders.length - 1];
        Log.e("path", path);
        return path;
    }

    private Uri moveFile(Uri uri) {
        InputStream in = null;
        OutputStream out = null;
        String inputPath = uri.getPath();
        Log.e("input file", uri.getPath());
        String fileName = ("/"
                + getArguments().getString("schoolName") + "_"
                + getArguments().getString("className") + "_"
                + getArguments().getString("conceptName") + "_"
                + getArguments().getString("designName") + "_"
                + System.currentTimeMillis() + ".jpg")
                .replaceAll("\\s+", "_");
        String outputPath = Environment.getExternalStorageDirectory() + "/Inqui/Design" + fileName;
        Log.e("output file ", outputPath);
        try {
            //create output directory if it doesn't exist
            File dir = new File(Environment.getExternalStorageDirectory() + "/inqui/Design");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            // write the output file
            out.flush();
            out.close();
            out = null;
            return Uri.fromFile(new File(outputPath));
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
            return null;
        }
    }

    private void fillFromDb() {
        mStudents.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Students> tempList = AppDatabase.getInstance(mContext).getStudentsDao().getStudentByClass(getArguments().getString("classid"));
                for (Students students : tempList) {
                    mStudents.add(new Student(students.getFirstName(), students.getLastName(),
                            students.getRollId(), students.getClassId(), students.getAdmissionNumber()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupSpinner(mStudents);
            }
        };
        try {
            asyncTask.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void setupSpinner(List<Student> students) {
        if (students.size() > 0) {
            String studentsList[] = new String[students.size()];
            studentsRoll = new String[students.size()];
            for (int i = 0; i < students.size(); i++) {
                studentsList[i] = students.get(i).getFirstName() + " " + students.get(i).getLastName();
                studentsRoll[i] = students.get(i).getRollNumber();
                Log.e("roll", students.get(i).getFirstName() + " " + students.get(i).getLastName() + students.get(i).getRollNumber());
            }
            spinnerStudents.setItems(studentsList);
        }
    }

    private void setPreview(Uri uri) {
        Picasso.with(getContext())
                .load(uri)
                .resize(800, 800)
                .centerInside()
                .into(designPreview);
    }

    private void putToDB(final Design design) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(mContext).getDesignDao().insert(design);
                Log.e("inserted", design.getDesign_name());
            }
        }).start();
    }

    private void updateDB() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(mContext).getDesignDao().deleteByPath(design.getDesign_image());
                Log.e("deleted from database", String.valueOf(AppDatabase.getInstance(mContext).getDesignDao().deleteByPath(design.getDesign_image())));
            }
        }).start();
    }

    /*private void initDropbox() {
        try {
            dropbox.init();
        } catch (DbxException e) {
            e.printStackTrace();
        }
    }*/

    private void submitDesign(String path) {
        final DesignModel designModel = new DesignModel();
        designModel.setClassId(classId);
        designModel.setConceptId(conceptId);
        designModel.setDesignName(design.getDesign_name());
        designModel.setFilePath(path);
        Log.e("design model", path);
        designModel.setStudentList(selectedRolls);
        designModel.setSchoolId(schoolId);
        List<DesignModel> designModels = new ArrayList<>();
        designModels.add(designModel);
        HttpHelper.getApi().submitDesign(designModels).enqueue(new Callback<com.iskar.djnig.inqui.model.Response>() {
            @Override
            public void onResponse(Call<com.iskar.djnig.inqui.model.Response> call, Response<com.iskar.djnig.inqui.model.Response> response) {
                if (response.isSuccessful()) {
                    if ((response.body()).getMessage().contains("Successfully")) {
                        progress.dismiss();
                        updateDB();
                        deleteFiles(Environment.getExternalStorageDirectory() + "/Inqui/" + design.getDesign_image());
                        showDialog("Design was successfully uploaded to Google Drive and added to server. " +
                                "Do you want to upload new design for "
                                + getArguments().getString("schoolName") + " "
                                + getArguments().getString("className") + " "
                                + getArguments().getString("conceptName") + "?");
                    }
                }
            }

            @Override
            public void onFailure(Call<com.iskar.djnig.inqui.model.Response> call, Throwable t) {
                progress.dismiss();
                editor = sharedPreferences.edit();
                editor.putBoolean("sync", true);
                editor.apply();
                showSorry("Your design is saved locally. It will be uploaded when the Internet appears.");
            }
        });
    }

    private void deleteFiles(String path) {
        File file = new File(path);
        file.delete();
    }

    @Override
    public void processFinish(String path) {
        if (path != null) {
            submitDesign(path);
        } else {
            progress.dismiss();
            showSorry("Something goes wrong! Check internet connection and try again!");
            editor = sharedPreferences.edit();
            editor.putBoolean("sync", true);
            editor.apply();
        }
    }

    private void showProgress() {
        progress = new MyProgressDialog(mContext, "Uploading design..");
        progress.show();
    }

    private void showSorry(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Design uploading")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                showDialog("Do you want to upload new design for "
                                        + getArguments().getString("schoolName") + " "
                                        + getArguments().getString("className") + " "
                                        + getArguments().getString("conceptName") + "?");
                            }
                        }).show();
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Design uploading")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                SubmitDesignFragment submitDesignFragment = new SubmitDesignFragment();
                                Bundle bundle = new Bundle();
                                bundle.putIntArray("spinners", getArguments().getIntArray("spinners"));
                                submitDesignFragment.setArguments(bundle);
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.root_for_fragments, submitDesignFragment);
                                transaction.commit();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                HomePageFragment homePageFragment = new HomePageFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.root_for_fragments, homePageFragment);
                                transaction.commit();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
