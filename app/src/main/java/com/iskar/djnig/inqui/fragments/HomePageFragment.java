package com.iskar.djnig.inqui.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;
import com.google.common.io.Files;
import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Classes;
import com.iskar.djnig.inqui.database.entities.Concepts;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Feedback;
import com.iskar.djnig.inqui.database.entities.Links;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Class;
import com.iskar.djnig.inqui.model.Concept;
import com.iskar.djnig.inqui.model.DesignModel;
import com.iskar.djnig.inqui.model.School;
import com.iskar.djnig.inqui.model.Student;
import com.iskar.djnig.inqui.services.NoConnectionService;
import com.iskar.djnig.inqui.utils.FragmentUtil;
import com.iskar.djnig.inqui.utils.NetworkManager;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomePageFragment extends Fragment {

    List<School> mSchools = new ArrayList<>();
    List<Student> mStudents = new ArrayList<>();
    List<Concept> mConcepts = new ArrayList<>();
    List<DesignModel> mDesigns = new ArrayList<>();
    private static final String[] SCOPES = {DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE, DriveScopes.DRIVE_FILE};
    MyProgressDialog progress;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    //DbxClientV2 client;
    private Context mContext;
    private GoogleCredential googleCredential;
    private Drive googleService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_page, container, false);
        ButterKnife.bind(this, v);
        Intent serviceIntent = new Intent(getContext(), NoConnectionService.class);
        getActivity().startService(serviceIntent);
        requestMediaPermission();
        sharedPreferences = getContext().getSharedPreferences("timer", Context.MODE_PRIVATE);
        SharedPreferences prefs = getContext().getSharedPreferences("signin_temp", Context.MODE_PRIVATE);
        HttpHelper.setToken(prefs.getString("token", null));
        return v;
    }

    private void requestMediaPermission() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(getActivity(), perms))
            EasyPermissions.requestPermissions(this, "Permission needed to save design images!", 1, perms);
    }

    private void showProgress() {
        progress = new MyProgressDialog(mContext, "Loading data from server...");
        progress.show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        if (NetworkManager.isConnected(getContext())) {
            getNetworkData();
            showProgress();
        }
    }

    private void getNetworkData() {
        mSchools.clear();
        mDesigns.clear();
        mConcepts.clear();
        mStudents.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppDatabase.getInstance(getContext()).getSchoolsDao().clear();
                AppDatabase.getInstance(getContext()).getStudentsDao().clear();
                AppDatabase.getInstance(getContext()).getConceptsDao().clear();
                AppDatabase.getInstance(getContext()).getDesignDao().clearSynced();
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                HttpHelper.getApi().getSchools().enqueue(new Callback<List<School>>() {
                    @Override
                    public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            mSchools.addAll(response.body());
                            writeToDbSchools(mSchools);
                        } else dismissProgress();
                    }

                    @Override
                    public void onFailure(Call<List<School>> call, Throwable t) {
                        dismissProgress();
                    }
                });

                HttpHelper.getApi().getConcepts().enqueue(new Callback<List<Concept>>() {
                    @Override
                    public void onResponse(Call<List<Concept>> call, Response<List<Concept>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            mConcepts.addAll(response.body());
                            Log.e("concepts", String.valueOf(response.body().size()));
                            writeToDbConcepts(mConcepts);
                        } else dismissProgress();
                    }

                    @Override
                    public void onFailure(Call<List<Concept>> call, Throwable t) {
                        dismissProgress();
                    }
                });

            }

        };
        asyncTask.execute();
    }

    private void getStudents() {
        for (School school : mSchools) {
            for (final Class classs : school.getClasses()) {
                HttpHelper.getApi().getDesigns(classs.getId()).enqueue(new Callback<List<DesignModel>>() {
                    @Override
                    public void onResponse(Call<List<DesignModel>> call, Response<List<DesignModel>> response) {
                        List<DesignModel> designs = new ArrayList<>();
                        if (response.isSuccessful() && response.body() != null) {
                            designs.addAll(response.body());
                        } else dismissProgress();
                        Log.e("mDesigns count", String.valueOf(designs.size()));
                        writeToDbDesigns(designs);
                    }

                    @Override
                    public void onFailure(Call<List<DesignModel>> call, Throwable t) {
                        dismissProgress();
                    }

                });

                HttpHelper.getApi().getStudents(classs.getId()).enqueue(new Callback<List<Student>>() {
                    @Override
                    public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {
                        List<Student> students = new ArrayList<>();
                        if (response.isSuccessful() && response.body() != null) {
                            students.addAll(response.body());
                            for (int i = 0; i < students.size(); i++) {
                                students.get(i).setClassId(classs.getId());
                                Log.e("student loaded", students.get(i).getFirstName() + students.get(i).getLastName() + students.get(i).getClassId());
                            }
                            Log.e("students count", String.valueOf(students.size()));
                        } else dismissProgress();
                        writeToDbStudents(students);
                    }

                    @Override
                    public void onFailure(Call<List<Student>> call, Throwable t) {
                        dismissProgress();
                    }
                });

            }
        }

    }

    private void writeToDbStudents(final List<Student> students) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    for (Student student : students) {
                        AppDatabase.getInstance(mContext).getStudentsDao().insert(
                                new com.iskar.djnig.inqui.database.entities.Students(
                                        student.getFirstName(), student.getLastName(),
                                        student.getRollNumber(), student.getClassId(), student.getAdmissionNumber()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getNetworkData();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {

            }

        };
        asyncTask.execute();
    }


    private void writeToDbDesigns(final List<DesignModel> designs) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    for (DesignModel design : designs) {
                        AppDatabase.getInstance(mContext).getDesignDao().insert(
                                new com.iskar.djnig.inqui.database.entities.Design(
                                        design.getDesignName(), design.getFilePath(),
                                        1, design.getDesignId(), design.getConceptId(),
                                        design.getStudentList(), design.getClassId(),
                                        design.getSchoolId(), design.getStatus(), design.getDesignComments()));
                        Log.e("Design preload ", design.getDesignName());
                    }
                    Log.e("numbers of designs", String.valueOf(AppDatabase.getInstance(mContext).getDesignDao().getSynced().size()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                editor = sharedPreferences.edit();
                editor.putLong("time", System.currentTimeMillis());
                editor.apply();
                getDesignFromDb();
            }
        };
        asyncTask.execute();
    }

    private void getDesignFromDb() {

        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                return AppDatabase.getInstance(mContext).getDesignDao().getSynced();
            }

            @Override
            protected void onPostExecute(Object o) {
                dismissProgress();
                try {
                    getLinks();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        asyncTask.execute();
    }


    private void writeToDbConcepts(final List<Concept> concepts) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    AppDatabase.getInstance(mContext).getConceptsDao().clear();
                    for (Concept concept : concepts) {
                        AppDatabase.getInstance(mContext).getConceptsDao().insertConcept(
                                new Concepts(concept.getName(), concept.getId()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
            }
        };
        asyncTask.execute();
    }

    private void writeToDbSchools(final List<School> schools) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppDatabase.getInstance(mContext).getSchoolsDao().clear();
                AppDatabase.getInstance(mContext).getStudentsDao().clear();
                AppDatabase.getInstance(mContext).getClassesDao().clear();
                List<String> classNames = new ArrayList<>();
                List<String> classIds = new ArrayList<>();
                for (School school : schools) {
                    classNames.clear();
                    classIds.clear();
                    for (Class tempClasses : school.getClasses()) {
                        classNames.add(tempClasses.getSection());
                        classIds.add(tempClasses.getId());
                        AppDatabase.getInstance(getContext()).getClassesDao().insert(new Classes(tempClasses.getSection(), tempClasses.getId()));
                    }
                    AppDatabase.getInstance(mContext).getSchoolsDao().insertSchool(
                            new Schools(school.getName(), school.getId(), classNames, classIds));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                getStudents();
            }
        };
        asyncTask.execute();
    }

    /*public void init() throws DbxException {
        // Create Dropbox client
        DbxRequestConfig config = new DbxRequestConfig("Inqui", "en_US");
        if (client == null)
            client = new DbxClientV2(config, StaticConstants.ACCESS_TOKEN);
    }*/

    private void dismissProgress() {
        try {
            progress.dismiss();
        } catch (Exception e) {

        }
    }

    private void getLinks() throws Exception {
        List<Design> designs = AppDatabase.getInstance(mContext).getDesignDao().getSynced();
        Log.e("size", String.valueOf(designs.size()));
        for (final Design designs1 : designs) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String string = designs1.getDesign_image();
                    try {
                        //init();
                        initGoogleDrive();
                        if (AppDatabase.getInstance(mContext).getLinksDao().getByDesignId(designs1.getDesign_id()) == null) {
                            /*try {
                                //client.sharing().createSharedLinkWithSettings("/" + string);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                            //List<SharedLinkMetadata> links = client.sharing().listSharedLinks().getLinks();

                            FileList fileList = googleService.files().list()
                                    .setQ("name contains '" + designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1) + "'")
                                    .setFields("nextPageToken, files(id, name, parents)")
                                    .execute();
                            List<com.google.api.services.drive.model.File> files = fileList.getFiles();
                            if(files.size() > 0) {
                                AppDatabase.getInstance(getContext()).getLinksDao().insertLinks(
                                        new Links(designs1.getDesign_id(), files.get(0).getId()));
                            }
                            Log.e("inserted", designs1.getDesign_image().substring(
                                    designs1.getDesign_image().lastIndexOf('/') + 1));


                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            googleService.files().list()
                                    .setQ("name contains '" + designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1) + "'")
                                    .executeAndDownloadTo(outputStream);
                            byte[] bitmapdata = outputStream.toByteArray();
                            File file = File.createTempFile(designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1), "jpeg");
                            Files.write(bitmapdata, file);
                            Picasso.with(mContext).load(file)
                                    .resize(300, 300).centerCrop().fetch();


                        }
                    } /*catch (DbxException e) {
                        e.printStackTrace();
                    }*/ catch (IOException e) {
                        e.printStackTrace();
                    }
                    List<Design> unsynced = AppDatabase.getInstance(mContext).getDesignDao().getUnsynced();
                    List<Feedback> unsyncedFeedbacks = AppDatabase.getInstance(mContext).getFeedbackDao().getUnsynced();
                    if (unsynced.size() == 0) {
                        deleteFolder("/Inqui/Design");
                    }
                    if (unsyncedFeedbacks.size() == 0) {
                        deleteFolder("/Inqui/Feedback");
                    }
                }
            }).start();
        }
    }

    private void initGoogleDrive() {
        try {
            googleCredential = GoogleCredential.fromStream(getResources().openRawResource(R.raw.inquilab2b9e83128b90)).createScoped(Arrays.asList(SCOPES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        googleService = new com.google.api.services.drive.Drive.Builder(
                transport, jsonFactory, googleCredential)
                .setApplicationName("InquilabServiceAccount")
                .build();
    }

    private void deleteFolder(String dirName) {
        try {
            File dir = new File(Environment.getExternalStorageDirectory() + dirName);
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (String aChildren : children) {
                    new File(dir, aChildren).delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.button_view_designs)
    public void viewDesigns() {
        if (dataPresent())
            FragmentUtil.changeFragment(new DesignSelectorFragment(), this);
        else getNetworkData();
    }

    @OnClick(R.id.button_submit_design)
    public void submitDesign() {
        if (dataPresent())
            FragmentUtil.changeFragment(new SubmitDesignFragment(), this);
        else getNetworkData();
    }

    @OnClick(R.id.button_request_material)
    public void requestMaterials() {
        if (dataPresent())
            FragmentUtil.changeFragment(new RequestChooser(), this);
        else getNetworkData();
    }

    @OnClick(R.id.button_student_info)
    public void studentInfo() {

        if (dataPresent())
            FragmentUtil.changeFragment(new StudentInfoFragment(), this);
        else getNetworkData();
    }

    @OnClick(R.id.button_class_info)
    public void classInfo() {
        if (dataPresent())
            FragmentUtil.changeFragment(new ClassInfoFragment(), this);
        else getNetworkData();
    }

    @OnClick(R.id.button_submit_feed)
    public void submitFeed() {
        if (dataPresent())
            FragmentUtil.changeFragment(new ConceptChooserFragment(), this);
        else getNetworkData();
    }

    private boolean dataPresent() {
        if (AppDatabase.getInstance(mContext).getDesignDao().getAll().size() > 0 ||
                AppDatabase.getInstance(mContext).getStudentsDao().getAllStudents().size() > 0 ||
                AppDatabase.getInstance(mContext).getConceptsDao().getAll().size() > 0)
            return true;
        else return false;
    }
}
