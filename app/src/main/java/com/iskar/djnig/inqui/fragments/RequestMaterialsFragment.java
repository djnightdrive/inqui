package com.iskar.djnig.inqui.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.StaticConstants;
import com.iskar.djnig.inqui.api.HttpHelper;
import com.iskar.djnig.inqui.model.Material;
import com.iskar.djnig.inqui.model.MaterialBody;
import com.iskar.djnig.inqui.recyclers.materials.Materials;
import com.iskar.djnig.inqui.recyclers.materials.MaterialsAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by djnig on 11/14/2017.
 */

public class RequestMaterialsFragment extends Fragment {
    @BindView(R.id.materials_recycler)
    RecyclerView materialsRecycler;
    @BindView(R.id.button_add_material)
    Button btnAddMaterial;
    @BindView(R.id.button_remove_material)
    Button btnRemoveMaterial;
    @BindView(R.id.button_submit_in_rm)
    Button btnSubmit;
    @BindView(R.id.add_design_edit)
    EditText editText;
    @BindView(R.id.button_submit_in_rm2)
    Button submit;
    private String mConceptId;
    Materials materials;
    MaterialsAdapter materialsAdapter;
    List<Material> materialsTemp;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.request_materials, container, false);
        ButterKnife.bind(this, v);
        mConceptId = getArguments().getString("conceptId");
        getMaterials();

        materialsTemp = new ArrayList<>();
        materialsTemp.add(new Material());
        materialsTemp.get(0).setMaterialName("");
        setupRecycler(materialsTemp);
        btnSubmit.setActivated(false);
        btnRemoveMaterial.setActivated(false);
        btnAddMaterial.setActivated(false);
        btnAddMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materials.addMaterial("", 1, 0, 0);
                materialsAdapter.notifyItemInserted(materials.getMaterials().size() - 1);
            }
        });
        btnRemoveMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().length() > 0 && editText.isFocused()) editText.setText("");
                else if (materials.getMaterials().size() > 1) {
                    materials.removeLastItem();
                    materialsAdapter.notifyDataSetChanged();
                }
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String extra_text = "Request materials to:\nSchool: " + getArguments().getString("schoolName")
                        + "\nClass: " + getArguments().getString("className")
                        + "\nConcept: " + getArguments().getString("conceptName")
                        + "\nDesign: " + getArguments().getString("designName") + ":\n ";
                for (int i = 0; i < materials.getMaterials().size(); i++) {
                    String materialName = materials.getMaterials().get(i).getMaterialName();
                    int materialQuantity = materials.getMaterials().get(i).getMaterialQuantity();
                    extra_text = extra_text + (i + 1) + ". " + materialName + "       " + materialQuantity + " pcs" + " \n ";
                }
                editText.setText("");
                editText.requestFocus(View.FOCUS_LEFT);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + StaticConstants.EMAIL + " ?subject=" + "MATERIALS REQUEST" + "&body=" + extra_text);
                intent.setData(data);
                startActivity(intent);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().length() > 0) {
                    MaterialBody materialBody = new MaterialBody();
                    materialBody.setMaterialName(editText.getText().toString());
                    putMaterial(materialBody, mConceptId);
                    editText.setText("");
                    editText.requestFocus(View.FOCUS_LEFT);
                } else editText.setError("Write material name!");
            }
        });
        return v;
    }

    private void showMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add material")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
    }

    private void putMaterial(final MaterialBody material, String concept) {
        HttpHelper.getApi().putMaterials(concept, material).enqueue(new Callback<com.iskar.djnig.inqui.model.Response>() {
            @Override
            public void onResponse(Call<com.iskar.djnig.inqui.model.Response> call, Response<com.iskar.djnig.inqui.model.Response> response) {
                Log.e("response", response.toString());
                if (response.body().getErrorMessage() != null)
                    if (response.body().getErrorMessage().contains("already")) {
                        showMessage("Material already present in the Concept.");
                    }

                if (response.body().getMessage() != null)
                    if (response.body().getMessage().contains("Successfully")) {
                        showMessage("Successfully added to server!");
                        materialsTemp.add(new Material());
                        materialsTemp.get(materialsTemp.size() - 1).setMaterialName(material.getMaterialName());
                        materialsAdapter.notifyDataSetChanged();
                    }
            }

            @Override
            public void onFailure(Call<com.iskar.djnig.inqui.model.Response> call, Throwable t) {
            }
        });
    }

    void getMaterials() {
        HttpHelper.getApi().getMaterialsByConcept(mConceptId).enqueue(new Callback<List<Material>>() {
            @Override
            public void onResponse(Call<List<Material>> call, Response<List<Material>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    setupRecycler(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Material>> call, Throwable t) {

            }
        });
    }

    void setupRecycler(List<Material> materialList) {
        materialsTemp = materialList;
        materials = new Materials();
        materials.addMaterial("", 1, 0, 0);
        materialsAdapter = new MaterialsAdapter(materials.getMaterials(), getActivity(), materialsTemp);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        materialsRecycler.setLayoutManager(linearLayoutManager);
        materialsRecycler.setAdapter(materialsAdapter);
        btnSubmit.setActivated(true);
        btnRemoveMaterial.setActivated(true);
        btnAddMaterial.setActivated(true);
    }
}
