package com.iskar.djnig.inqui.onlineStorage;

import android.content.Context;
import android.os.AsyncTask;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.iskar.djnig.inqui.R;

import java.io.IOException;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;

import static com.iskar.djnig.inqui.StaticConstants.DESIGN;
import static com.iskar.djnig.inqui.StaticConstants.DESIGN_SERVICE;
import static com.iskar.djnig.inqui.StaticConstants.FEED_AUDIO;
import static com.iskar.djnig.inqui.StaticConstants.FEED_AUDIO_AFTEROFFLINE;
import static com.iskar.djnig.inqui.StaticConstants.FEED_TXT;
import static com.iskar.djnig.inqui.StaticConstants.FEED_TXT_AFTEROFFLINE;
import static com.iskar.djnig.inqui.StaticConstants.FEED_VIDEO;
import static com.iskar.djnig.inqui.StaticConstants.FEED_VIDEO_AFTEROFFLINE;

/**
 * Created by dell on 16.03.2018.
 */

public class GoogleDrive extends AsyncTask<String, Void, String> {
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE, DriveScopes.DRIVE_FILE};
    private static GoogleCredential googleCredential;
    private com.google.api.services.drive.Drive mService = null;
    public GoogleDriveAsyncResponse delegate = null;
    private boolean deleteFile = false;

    public GoogleDrive() {

    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            switch (strings[1]) {
                case DESIGN:
                    return uploadDesign(strings[0]);
                case DESIGN_SERVICE:
                    return uploadDesignAfterOffline(strings[0]);
                case FEED_TXT:
                    return uploadTxt(strings[0]);
                case FEED_VIDEO:
                    return uploadVideo(strings[0]);
                case FEED_AUDIO:
                    return uploadAudio(strings[0]);
                case FEED_AUDIO_AFTEROFFLINE:
                    return uploadAudioAfterOffline(strings[0]);
                case FEED_VIDEO_AFTEROFFLINE:
                    return uploadVideoAfterOffline(strings[0]);
                case FEED_TXT_AFTEROFFLINE:
                    return uploadTxtAfterOffline(strings[0]);
                default:
                    return null;
            }
        }
        catch (Exception e) {
            return null;
        }
    }

    private String uploadDesignAfterOffline(String string) throws Exception{
        //InputStream in = new FileInputStream(string.substring(7));
        String path = "Design/" + getFileName(string);

        String folderId = checkIsFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(string);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(string));
        //body.setParents(Arrays.asList(("Designs")));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(string);//path to file
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
        System.out.println("File ID: " + file.getId());

        deleteFile = true;
        return string;
    }

    private String uploadAudio(String path) throws Exception {
        String pathh = "/AudioFeed" + getFileName(path);

        String folderId = checkIsAudioFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(path);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(path));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(path);
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
        System.out.println("File ID: " + file.getId());

        return pathh;
    }

    private String uploadAudioAfterOffline(String path) throws Exception {
        String pathh = "/AudioFeed" + getFileName(path);

        String folderId = checkIsAudioFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(path);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(path));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(path);
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
        System.out.println("File ID: " + file.getId());

        deleteFile = true;
        return path;
    }

    private String checkIsAudioFolderExists() throws Exception{
        FileList result = mService.files().list()
                .setQ("mimeType = 'application/vnd.google-apps.folder'")
                .setFields("files(id, name)")
                .execute();

        List<File> files = result.getFiles();
        if (files != null) {
            for (File file : files) {
                if(file.getName().equals("AudioFeed")) {
                    return file.getId();
                }
            }
        }

        File fileMetadata = new File();
        fileMetadata.setName("AudioFeed");
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        File file = mService.files().create(fileMetadata)
                .setFields("name")
                .execute();
        System.out.println("Folder " + file.getName() + " created!");
        return file.getId();
    }

    private String uploadVideo(String path) throws Exception{
        String pathh = "/VideoFeed" + getFileName(path);

        String folderId = checkIsVideoFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(path);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(path));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(path);
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
        System.out.println("File ID: " + file.getId());

        return pathh;

    }

    private String uploadVideoAfterOffline(String path) throws Exception{
        String pathh = "/VideoFeed" + getFileName(path);

        String folderId = checkIsVideoFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(path);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(path));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(path);
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
        System.out.println("File ID: " + file.getId());

        deleteFile = true;
        return path;

    }

    private String checkIsVideoFolderExists() throws Exception{
        FileList result = mService.files().list()
                .setQ("mimeType = 'application/vnd.google-apps.folder'")
                .setFields("files(id, name)")
                .execute();

        List<File> files = result.getFiles();
        if (files != null) {
            for (File file : files) {
                if(file.getName().equals("VideoFeed")) {
                    return file.getId();
                }
            }
        }

        File fileMetadata = new File();
        fileMetadata.setName("VideoFeed");
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        File file = mService.files().create(fileMetadata)
                .setFields("name")
                .execute();
        System.out.println("Folder " + file.getName() + " created!");
        return file.getId();
    }

    private String uploadTxt(String path) throws Exception {
        String pathh = "/TextFeed" + getFileName(path);

        String folderId = checkIsTextFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(path);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(path));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(path);
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
        System.out.println("File ID: " + file.getId());

        return pathh;

    }

    private String uploadTxtAfterOffline(String path) throws Exception {
        String pathh = "/TextFeed" + getFileName(path);

        String folderId = checkIsTextFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(path);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(path));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(path);
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
        System.out.println("File ID: " + file.getId());

        deleteFile = true;
        return path;

    }

    private String checkIsTextFolderExists() throws Exception{
        FileList result = mService.files().list()
                .setQ("mimeType = 'application/vnd.google-apps.folder'")
                .setFields("files(id, name)")
                .execute();

        List<File> files = result.getFiles();
        if (files != null) {
            for (File file : files) {
                if(file.getName().equals("TextFeed")) {
                    return file.getId();
                }
            }
        }

        File fileMetadata = new File();
        fileMetadata.setName("TextFeed");
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        File file = mService.files().create(fileMetadata)
                .setFields("name")
                .execute();
        System.out.println("Folder " + file.getName() + " created!");
        return file.getId();
    }

    private String uploadDesign(String string) throws Exception {
        //InputStream in = new FileInputStream(string.substring(7));
        String path = "Design/" + getFileName(string);

        String folderId = checkIsFolderExists();

        String mimeType = URLConnection.guessContentTypeFromName(string);
        File body = new File();
        body.setParents(Arrays.asList(folderId));
        body.setName(getFileName(string));
        //body.setParents(Arrays.asList(("Designs")));
        body.setMimeType(mimeType);

        java.io.File fileContent = new java.io.File(string.substring(7));//path to file
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = mService.files().create(body, mediaContent).execute();

        // Uncomment the following line to print the File ID.
         System.out.println("File ID: " + file.getId());

        return path;
    }

    private String checkIsFolderExists() throws Exception{
        FileList result = mService.files().list()
                .setQ("mimeType = 'application/vnd.google-apps.folder'")
                .setFields("files(id, name)")
                .execute();

        List<File> files = result.getFiles();
        if (files != null) {
            for (File file : files) {
                if(file.getName().equals("Design")) {
                    return file.getId();
                }
            }
        }

        File fileMetadata = new File();
        fileMetadata.setName("Design");
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        File file = mService.files().create(fileMetadata)
                .setFields("name")
                .execute();
        System.out.println("Folder " + file.getName() + " created!");
        return file.getId();
    }


    public void initGoogleDrive(Context context) {
        try {
            googleCredential = GoogleCredential.fromStream(context.getResources().openRawResource(R.raw.inquilab2b9e83128b90)).createScoped(Arrays.asList(SCOPES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new com.google.api.services.drive.Drive.Builder(
                transport, jsonFactory, googleCredential)
                .setApplicationName("InquilabServiceAccount")
                .build();
    }

    private String getFileName(String path) {
        if (null != path && path.length() > 0) {
            int endIndex = path.lastIndexOf("/");
            if (endIndex != -1) {
                return path.substring(endIndex + 1); // not forgot to put check if(endIndex != -1)
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String path) {
        System.out.println("Loaded: " + path);
        if(delegate != null)
            delegate.processFinish(path);
        if(deleteFile) {
            java.io.File file = new java.io.File(path);
            file.delete();
            deleteFile = false;
        }
    }

    @Override
    protected void onCancelled() {

    }
}
