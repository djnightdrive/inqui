package com.iskar.djnig.inqui.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.iskar.djnig.inqui.services.NoConnectionService;


public class ServiceRestarterBroadcastReceiver extends BroadcastReceiver {

    public static String URI_FOR_DROPBOX = "uri_dropbox";
    public static String FILE_TYPE = "type";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("uploadingservice", "RESTART!!!!!");
        String uri = intent.getStringExtra(URI_FOR_DROPBOX);
        String type = intent.getStringExtra(FILE_TYPE);

        context.startService(new Intent(context, NoConnectionService.class)
                .putExtra(URI_FOR_DROPBOX, uri)
                .putExtra(FILE_TYPE, type));
    }

}
