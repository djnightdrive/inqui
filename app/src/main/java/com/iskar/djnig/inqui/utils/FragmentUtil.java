package com.iskar.djnig.inqui.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.iskar.djnig.inqui.R;

/**
 * Created by djnig on 11/23/2017.
 */

public class FragmentUtil {
    public static void changeFragment(Fragment fragment, Fragment current) {
        FragmentTransaction transaction = current.getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public static void attachFragmentWithoutBS(Fragment fragment, Context context) {
        FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.root_for_fragments, fragment);
        transaction.commit();
    }
}
