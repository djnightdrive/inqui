package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Class;
import com.iskar.djnig.inqui.model.School;
import com.iskar.djnig.inqui.model.Student;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by djnig on 11/14/2017.
 */

public class ClassInfoFragment extends Fragment {

    @BindView(R.id.button_view_info)
    Button viewInfo;

    @BindView(R.id.spinner_top)
    Spinner spinnerSchool;

    @BindView(R.id.spinner_bottom)
    Spinner spinnerClassroom;

    MyProgressDialog progress;
    List<School> mSchools = new ArrayList<>();
    List<Student> mStudents = new ArrayList<>();
    Context mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.class_info, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        mContext = getContext();
        mSchools = new ArrayList<>();
        mStudents = new ArrayList<>();
        String[] schoolsArray = getResources().getStringArray(R.array.spinner_schools);
        String[] classesArray = getResources().getStringArray(R.array.spinner_classes);
        spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                schoolsArray));
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                classesArray));
        viewInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int schoolPos = spinnerSchool.getSelectedItemPosition();
                    int classPos = spinnerClassroom.getSelectedItemPosition();
                    String classId = mSchools.get(schoolPos).getClasses().get(classPos).getId();
                    String schoolName = mSchools.get(schoolPos).getName();
                    String className = mSchools.get(schoolPos).getClasses().get(classPos).getSection();
                    Bundle bundle = new Bundle();
                    bundle.putString("classId", classId);
                    bundle.putString("className", className);
                    bundle.putString("schoolName", schoolName);
                    ClassInfoListFragment classInfoListFragment = new ClassInfoListFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    classInfoListFragment.setArguments(bundle);
                    transaction.replace(R.id.root_for_fragments, classInfoListFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        fillFromDb();
    }

    private void fillFromDb() {
        mSchools.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Schools> schools = AppDatabase.getInstance(mContext).getSchoolsDao().getAll();
                List<String> classNames;
                List<String> classIds;
                for (Schools tempSchool : schools) {
                    List<Class> classList = new ArrayList<>();
                    classNames = tempSchool.getClasses();
                    classIds = tempSchool.getClassesId();
                    for (int i = 0; i < classNames.size(); i++) {
                        classList.add(new Class(classNames.get(i), classIds.get(i)));
                    }
                    mSchools.add(new School(tempSchool.getSchoolName(), tempSchool.getSchoolId(), classList));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillSchoolSpinners();
            }
        };
        asyncTask.execute();
    }


    private void fillSchoolSpinners() {
        final ArrayList<String> schoolNames = new ArrayList<>();
        try {
            for (School sc : mSchools) {
                schoolNames.add(sc.getName());
            }
            spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    schoolNames.toArray(new String[schoolNames.size()])));
            spinnerSchool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fillClassesSpinner(mSchools.get(position).getClasses());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void fillClassesSpinner(List<Class> classesList) {
        ArrayList<String> sections = new ArrayList<>();
        for (Class cl : classesList) {
            sections.add(cl.getSection());
        }
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                sections.toArray(new String[sections.size()])));
        spinnerClassroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
