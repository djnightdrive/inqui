package com.iskar.djnig.inqui.onlineStorage;

/**
 * Created by karasinboots on 25.11.2017.
 */

public interface GoogleDriveAsyncResponse {
    void processFinish(String path);
}
