package com.iskar.djnig.inqui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;


public class StartActivity extends AppCompatActivity {
    SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        sPref = getSharedPreferences("signin_temp", MODE_PRIVATE);
        if (sPref.getBoolean("sign_in_flag", false)) {
            Intent intent = new Intent(StartActivity.this, DrawerActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(StartActivity.this, SignInActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (sPref.getBoolean("sign_in_flag", false)) {
            System.exit(0);
        }
    }
}
