package com.iskar.djnig.inqui.recyclers.approve;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karasinboots on 18.11.2017.
 */

public class Approves {
    private String studentName;
    private String studentId;
    private String schoolName;
    private String className;
    private String rollNumber;

    private List<Approves> approvesList = new ArrayList<>();

    public Approves(String studentName, String schoolName, String className, String rollNumber) {
        this.studentName = studentName;
        this.schoolName = schoolName;
        this.className = className;
        this.rollNumber = rollNumber;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<Approves> getApprovesList() {
        return approvesList;
    }

    public void setApprovesList(List<Approves> approvesList) {
        this.approvesList = approvesList;
    }
}
