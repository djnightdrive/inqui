package com.iskar.djnig.inqui.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.iskar.djnig.inqui.database.entities.Schools;

import java.util.List;

/**
 * Created by karasinboots on 23.11.2017.
 */

@Dao
public interface SchoolsDAO {

    @Query("SELECT * FROM schools")
    List<Schools> getAll();

    @Query("DELETE FROM schools")
    void clear();

    @Query("SELECT * FROM schools WHERE classesId LIKE :classId")
    Schools getByClass(String classId);

    @Insert
    void insertSchool(Schools schools);

    @Update
    void updateFeedback(Schools schools);

    @Delete
    void deleteFeedback(Schools schools);
}