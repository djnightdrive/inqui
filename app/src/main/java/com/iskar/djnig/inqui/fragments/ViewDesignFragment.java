package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.onlineStorage.GoogleDriveAsyncResponse;
import com.iskar.djnig.inqui.recyclers.RecyclerViewClickListener;
import com.iskar.djnig.inqui.recyclers.design.Designs;
import com.iskar.djnig.inqui.recyclers.design.DesignsAdapter;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by karasinboots on 27.11.2017.
 */

public class ViewDesignFragment extends Fragment implements RecyclerViewClickListener {
    @BindView(R.id.designs_recycler)
    RecyclerView designRecycler;
    @BindView(R.id.button_cancel)
    Button cancel;
    @BindView(R.id.spinner_status)
    Spinner spinnerStatus;
    @BindView(R.id.reviewSelected)
    Button review;
    public GoogleDriveAsyncResponse delegate = null;

    DesignsAdapter designsAdapter;
    Context mContext;
    Designs mDesigns = new Designs();
    Designs mFiltered = new Designs();
    SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_design, container, false);
        ButterKnife.bind(this, v);
        mContext = getContext();
        sharedPreferences = mContext.getSharedPreferences("design", Context.MODE_PRIVATE);
        String[] status = getResources().getStringArray(R.array.spinner_status);
        spinnerStatus.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                status));
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupRecycler(mDesigns);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    //all
                    case 0:
                        getDesignsByFilter(sharedPreferences.getString("class_icon", ""), sharedPreferences.getString("concept", ""), 0);
                        break;
                    //approved
                    case 1:
                        getDesignsByFilter(sharedPreferences.getString("class_icon", ""), sharedPreferences.getString("concept", ""), 1);
                        break;
                    //rejected
                    case 2:
                        getDesignsByFilter(sharedPreferences.getString("class_icon", ""), sharedPreferences.getString("concept", ""), 2);
                        break;
                    //submitted
                    case 3:
                        getDesignsByFilter(sharedPreferences.getString("class_icon", ""), sharedPreferences.getString("concept", ""), 3);
                        break;
                    case 4:
                        getDesignsByFilter(sharedPreferences.getString("class_icon", ""), sharedPreferences.getString("concept", ""), 4);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0)
                    getFragmentManager().popBackStack();
            }
        });
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Designs forReview = new Designs();
                for (Designs designs : designsAdapter.getDesignsList()) {
                    if (designs.isCheckedStatus()) {
                        forReview.getDesignsList().add(designs);
                    }
                }
                if (forReview.getDesignsList().size() > 0) {
                    ReviewFragment reviewFragment = new ReviewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("mDesigns", forReview);
                    reviewFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.root_for_fragments, reviewFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else showDialog("Select at least one design to review!");
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Design approvation")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
    }

    private void getDesignsByFilter(final String classId, final String conceptId, final int filter) {
        mDesigns.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Design> list = new ArrayList<>();
                if (conceptId.equals("all")) {
                    list = AppDatabase.getInstance(mContext).getDesignDao().getByClass(classId);
                } else {
                    list = AppDatabase.getInstance(mContext).getDesignDao().getByFilter(classId,
                            conceptId);
                }
                for (Design design : list) {
                    mDesigns.getDesigns().add(new Designs(design.getDesign_name(),
                            design.getDesign_image(), design.getRollIds(), design.getApproved(),
                            design.getDesign_id(), design.getComment()));
                }

                switch (filter) {
                    case 0:
                        return mDesigns;
                    case 1:
                        mFiltered.clear();
                        for (Designs design : mDesigns.getDesigns()) {
                            if (design.getStatus().equals("Approve")) {
                                mFiltered.getDesigns().add(design);
                            }
                        }
                        return mFiltered;
                    case 2:
                        mFiltered.clear();
                        for (Designs design : mDesigns.getDesigns()) {
                            if (design.getStatus().equals("Reject")) {
                                mFiltered.getDesigns().add(design);
                            }
                        }
                        return mFiltered;
                    case 3:
                        mFiltered.clear();
                        for (Designs design : mDesigns.getDesigns()) {
                            if (design.getStatus().equals("Submitted")) {
                                mFiltered.getDesigns().add(design);
                            }
                        }
                        return mFiltered;
                    case 4:
                        mFiltered.clear();
                        for (Designs design : mDesigns.getDesigns()) {
                            if (design.getStatus().equals("In review")) {
                                mFiltered.getDesigns().add(design);
                            }
                        }
                        return mFiltered;
                }
                return mFiltered;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupRecycler((Designs) o);
            }
        };
        asyncTask.execute();
    }

    public void setupRecycler(Designs designs) {
        designsAdapter = new DesignsAdapter(designs.getDesigns(), getContext(), this, 1, getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        designRecycler.setLayoutManager(linearLayoutManager);
        designRecycler.setAdapter(designsAdapter);
        designsAdapter.notifyDataSetChanged();
    }

    @Override
    public void recyclerViewListClicked(View v, int position, String dropLink) {
        //set up adapter and pass clicked listener this
        goToApproveFragment(position, dropLink);
    }



    private void goToApproveFragment(int position, String dropLink) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("design", designsAdapter.getDesignsList().get(position));
        bundle.putInt("position", position);
        bundle.putString("dropLink", dropLink);
        ApproveDesignFragment approveDesignFragment = new ApproveDesignFragment();
        approveDesignFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, approveDesignFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
