
package com.iskar.djnig.inqui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaterialBody implements Parcelable
{

    @SerializedName("materialName")
    @Expose
    private String materialName;
    public final static Creator<MaterialBody> CREATOR = new Creator<MaterialBody>() {


        @SuppressWarnings({
            "unchecked"
        })
        public MaterialBody createFromParcel(Parcel in) {
            return new MaterialBody(in);
        }

        public MaterialBody[] newArray(int size) {
            return (new MaterialBody[size]);
        }

    }
    ;

    protected MaterialBody(Parcel in) {
        this.materialName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MaterialBody() {
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(materialName);
    }

    public int describeContents() {
        return  0;
    }

}
