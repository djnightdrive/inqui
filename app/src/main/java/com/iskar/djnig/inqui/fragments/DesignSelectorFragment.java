package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Concepts;
import com.iskar.djnig.inqui.database.entities.Design;
import com.iskar.djnig.inqui.database.entities.Links;
import com.iskar.djnig.inqui.database.entities.Schools;
import com.iskar.djnig.inqui.dialogs.MyProgressDialog;
import com.iskar.djnig.inqui.model.Class;
import com.iskar.djnig.inqui.model.Concept;
import com.iskar.djnig.inqui.model.School;
import com.iskar.djnig.inqui.spinners.SpinnerAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 11.12.2017.
 */

public class DesignSelectorFragment extends Fragment {
    @BindView(R.id.spinner_school)
    Spinner spinnerSchool;

    @BindView(R.id.spinner_class)
    Spinner spinnerClassroom;

    @BindView(R.id.spinner_concept)
    Spinner spinnerConcept;

    @BindView(R.id.button_view_info3)
    Button selectFilter;
    List<School> mSchools = new ArrayList<>();
    List<Concept> mConcepts = new ArrayList<>();
    MyProgressDialog progress;
    Context mContext;
    //DbxClientV2 client;
    private GoogleCredential googleCredential;
    private static final String[] SCOPES = {DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE, DriveScopes.DRIVE_FILE};
    private Drive googleService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.design_selector, container, false);
        ButterKnife.bind(this, v);
        mContext = getContext();
        try {
            getLinks();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mSchools.clear();
        mConcepts.clear();
        String[] schools = getResources().getStringArray(R.array.spinner_schools);
        String[] classrooms = getResources().getStringArray(R.array.spinner_classes);
        String[] concepts = getResources().getStringArray(R.array.spinner_concepts);
        spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                schools));
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                classrooms));
        spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                concepts));
        selectFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    getDesignsByFilter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        fillFromDb();
        fillConceptsFromDb();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
    }

    private void fillFromDb() {
        mSchools.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Schools> schools = AppDatabase.getInstance(mContext).getSchoolsDao().getAll();
                List<String> classNames;
                List<String> classIds;
                for (Schools tempSchool : schools) {
                    List<Class> classList = new ArrayList<>();
                    classNames = tempSchool.getClasses();
                    classIds = tempSchool.getClassesId();
                    for (int i = 0; i < classNames.size(); i++) {
                        classList.add(new Class(classNames.get(i), classIds.get(i)));
                    }
                    mSchools.add(new School(tempSchool.getSchoolName(), tempSchool.getSchoolId(), classList));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillSchoolSpinners();
            }
        };
        asyncTask.execute();
    }

    private void fillConceptsFromDb() {
        mConcepts.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    List<Concepts> concepts = AppDatabase.getInstance(mContext).getConceptsDao().getAll();
                    for (Concepts concept : concepts) {
                        mConcepts.add(new Concept(concept.getConceptName(), concept.getConceptId()));
                    }
                } catch (Exception e) {
                    int count = getFragmentManager().getBackStackEntryCount();
                    if (count > 0)
                        getFragmentManager().popBackStack();
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                fillConceptSpinner();
            }
        };
        asyncTask.execute();
    }

    private void fillSchoolSpinners() {
        final ArrayList<String> schoolNames = new ArrayList<>();
        schoolNames.clear();
        try {
            for (School sc : mSchools) {
                schoolNames.add(sc.getName());
            }
            spinnerSchool.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    schoolNames.toArray(new String[schoolNames.size()])));
            Bundle arguments = getArguments();
            if (arguments != null && arguments.containsKey("spinners")) {
                spinnerSchool.setSelection(arguments.getIntArray("spinners")[0]);
                fillClassesSpinner(mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses());
            }
            spinnerSchool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fillClassesSpinner(mSchools.get(position).getClasses());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /*public void init() throws DbxException {
        // Create Dropbox client
        DbxRequestConfig config = new DbxRequestConfig("Inqui", "en_US");
        if (client == null)
            client = new DbxClientV2(config, StaticConstants.ACCESS_TOKEN);
    }*/

    private void getLinks() throws Exception {
        List<Design> designs = AppDatabase.getInstance(mContext).getDesignDao().getSynced();
        Log.e("size", String.valueOf(designs.size()));
        for (final Design designs1 : designs) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String string = designs1.getDesign_image();
                    try {
                        //init();
                        initGoogleDrive();
                        if (AppDatabase.getInstance(mContext).getLinksDao().getByDesignId(designs1.getDesign_id()) == null) {
                           /* try {
                                client.sharing().createSharedLinkWithSettings("/" + string);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            List<SharedLinkMetadata> links = client.sharing().listSharedLinks().getLinks();*/
                            FileList fileList = googleService.files().list()
                                    .setQ("name contains '" + designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1) + "'")
                                    .setFields("nextPageToken, files(id, name, parents)")
                                    .execute();
                            List<File> files = fileList.getFiles();
                            if(files.size() > 0) {
                                AppDatabase.getInstance(getContext()).getLinksDao().insertLinks(
                                        new Links(designs1.getDesign_id(), files.get(0).getId()));
                                Log.e("inserted", designs1.getDesign_image().substring(
                                        designs1.getDesign_image().lastIndexOf('/') + 1));
                            }
                            /*for (SharedLinkMetadata link : links) {
                                String linkTemp = link.getName();
                                Log.e("prepared", designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1));
                                if (linkTemp.equals(designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1))) {
                                    String finalLink = link.getUrl().substring(0, link.getUrl().lastIndexOf('?')) + "?raw=1";
                                    AppDatabase.getInstance(getContext()).getLinksDao().insertLinks(
                                            new Links(designs1.getDesign_id(), finalLink));
                                    Log.e("inserted", designs1.getDesign_image().substring(
                                            designs1.getDesign_image().lastIndexOf('/') + 1) + " " + finalLink);
                                }
                            }*/
                        }
                    } /*catch (DbxException e) {
                        e.printStackTrace();
                    }*/ catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private void initGoogleDrive() {
        try {
            googleCredential = GoogleCredential.fromStream(getResources().openRawResource(R.raw.inquilab2b9e83128b90)).createScoped(Arrays.asList(SCOPES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        googleService = new com.google.api.services.drive.Drive.Builder(
                transport, jsonFactory, googleCredential)
                .setApplicationName("InquilabServiceAccount")
                .build();
    }

    private void fillClassesSpinner(List<Class> classesList) {
        ArrayList<String> sections = new ArrayList<>();
        sections.clear();
        for (Class cl : classesList) {
            sections.add(cl.getSection());
        }
        spinnerClassroom.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                sections.toArray(new String[sections.size()])));
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("spinners"))
            spinnerClassroom.setSelection(arguments.getIntArray("spinners")[1]);
    }

    private void fillConceptSpinner() {
        try {
            ArrayList<String> concepts = new ArrayList<>();
            concepts.clear();
            concepts.add("All concepts");
            for (Concept cl : mConcepts) {
                concepts.add(cl.getName());
            }
            spinnerConcept.setAdapter(new SpinnerAdapter(getContext(), R.layout.spinner_item,
                    concepts.toArray(new String[concepts.size()])));

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void getDesignsByFilter() throws Exception {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("design", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("class_icon", mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses()
                .get(spinnerClassroom.getSelectedItemPosition()).getId());
        Log.e("class_icon", mSchools.get(spinnerSchool.getSelectedItemPosition()).getClasses()
                .get(spinnerClassroom.getSelectedItemPosition()).getId());
        if (spinnerConcept.getSelectedItemPosition() == 0) {
            editor.putString("concept", "all");
        } else
            editor.putString("concept", mConcepts.get(spinnerConcept.getSelectedItemPosition() - 1).getId());
        editor.apply();
        ViewDesignFragment viewDesignFragment = new ViewDesignFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.root_for_fragments, viewDesignFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
