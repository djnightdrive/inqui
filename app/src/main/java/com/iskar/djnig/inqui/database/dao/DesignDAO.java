package com.iskar.djnig.inqui.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.iskar.djnig.inqui.database.entities.Design;

import java.util.List;

/**
 * Created by karasinboots on 14.11.2017.
 */

@Dao
public interface DesignDAO {

    @Query("SELECT * FROM design")
    List<Design> getAll();

    @Query("SELECT * FROM design WHERE design_sync_flag LIKE '1'")
    List<Design> getSynced();

    @Query("SELECT * FROM design WHERE design_id LIKE :designId")
    Design getById(String designId);

    @Query("SELECT * FROM design WHERE classId LIKE :classId AND design_sync_flag LIKE '1'")
    List<Design> getByClass(String classId);

    @Query("SELECT * FROM design WHERE rollIds LIKE :rollIds")
    List<Design> getByRoll(String rollIds);

    @Query("SELECT * FROM design WHERE conceptId LIKE :conceptId")
    List<Design> getByConcept(String conceptId);

    @Query("SELECT * FROM design WHERE approved LIKE 'Approve' AND conceptId LIKE :conceptId AND classId LIKE :classId  AND design_sync_flag LIKE '1'")
    List<Design> getByConceptApproved(String conceptId, String classId);

    @Query("SELECT * FROM design WHERE design_sync_flag LIKE '0'")
    List<Design> getUnsynced();

    @Query("SELECT * FROM design WHERE approved LIKE 'Approve'")
    List<Design> getApproved();

    @Query("SELECT * FROM design WHERE approved LIKE 'Approve' AND classId LIKE :classId")
    List<Design> getApprovedByClass(String classId);

    @Query("SELECT * FROM design WHERE design_name LIKE :name")
    Design getByName(String name);

    @Query("SELECT * FROM design WHERE classId LIKE :classId AND conceptId LIKE :conceptId  AND design_sync_flag LIKE '1' ORDER BY design_name ASC")
    List<Design> getByFilter(String classId, String conceptId);

    @Query("DELETE FROM design WHERE design_image LIKE :path")
    int deleteByPath(String path);

    @Insert
    void insert(Design design);

    @Update
    void update(Design design);

    @Query("DELETE FROM design")
    void clear();

    @Query("DELETE FROM design WHERE design_sync_flag LIKE '1'")
    void clearSynced();

    @Delete
    void delete(Design design);
}
