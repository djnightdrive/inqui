package com.iskar.djnig.inqui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.iskar.djnig.inqui.R;
import com.iskar.djnig.inqui.database.AppDatabase;
import com.iskar.djnig.inqui.database.entities.Students;
import com.iskar.djnig.inqui.model.Student;
import com.iskar.djnig.inqui.recyclers.students.StudentsAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 27.11.2017.
 */

public class ClassInfoListFragment extends android.support.v4.app.Fragment {
    @BindView(R.id.classList)
    RecyclerView classList;
    @BindView(R.id.button_back)
    Button cancel;
    @BindView(R.id.className)
    TextView classNameTv;
    @BindView(R.id.schoolName)
    TextView schoolNameTv;
    Context mContext;
    List<Student> mStudents = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.class_info_list, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        Bundle bundle = getArguments();
        String className = bundle.getString("className");
        String schoolName = bundle.getString("schoolName");
        schoolNameTv.setText(schoolName);
        classNameTv.setText(className);
        cancel.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          int count = getFragmentManager().getBackStackEntryCount();
                                          if (count > 0)
                                              getFragmentManager().popBackStack();
                                      }
                                  }
        );
        fillFromDb(bundle.getString("classId"));
    }

    private void fillFromDb(final String classId) {
        mStudents.clear();
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                List<Students> tempList = AppDatabase.getInstance(mContext).getStudentsDao().getStudentByClass(classId);
                for (Students students : tempList) {
                    mStudents.add(new Student(students.getFirstName(), students.getLastName(),
                            students.getRollId(), students.getClassId(), students.getAdmissionNumber()));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupRecycler(mStudents);
            }
        };
        try {
            asyncTask.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }


    private void setupRecycler(List<Student> students) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        classList.setLayoutManager(linearLayoutManager);
        classList.setAdapter(new StudentsAdapter(students));
    }

}
