To build project you need install last version of AndroidStudio, then open and build project. Thats all.
To migrate to you user account in Dropbox you must register app in Dropbox developer console and make folder structure like
	Inqui
		/Design
		/Feedback
			/Audio
			/Video
			/Text
then you need get secret token from Dropbox developer console and change ACCESS_TOKEN in StaticConstants to your's.

If you want to change email for Submit Materials you need to change it in StaticConstants EMAIL.

activities package:
	
	StartActivity
Activity without interface.
Created to check login status and open DrawerActivity or SignInActivity

	SignInActivity
In this activity implemented login process, testing user credentials, getting authorization token and saving login status and token in preferences

	DrawerActivity
Main interface activity. 
Menu drawer and layout for fragments are represented.
Each fragments in app will be displayed there if selected.
Also in DrawerActivity overrided method onBackPressed() to navigate in fragments thru "Back" button.

api package:

	AsyncResponse 
interface that called when we get answer from server and forward action and response.
	
	CallApi
this class created to provide autologin functionality and adding new concepts and new students to server. 
when process done call interface and forward action and response of server. Also used to getting list of available concepts
in AddConceptFragment.
	
	HttpHelper
in this class we configuring OkHttpCliewnt and Retrofit library to use it in fragments where needed to get/put/post requests
on server

	Service
class with description of needed get/put/post requests and description of server responses.

broadcastReceivers package:
	
	ServiceRestarterBroadcastReciever 
if service destroyed by system receive broadcast message from service and restaert it

data package:
	
	ConceptsData
used like dummy in early builds and contains concepts data.

	SchoolData
used like temporary storage of schools data like schoolName, schoolId, classNames and classesId.

database package:

	AppDatabase
used for configuring and instantinating local database. Used Google Room Persistence Library. Getting instance of database is expencive operation so we use singleton for accessing it.
	
	ListStringConverter
data converter. Some entities in database have relations "one-to-many", so we need to convert lists (for example) of students roll numbers to string with dividers to write in database and convert back to list of rolls when we read.
	
	dao package:
	all classes in this package used like DAO's and made for accessing to local database(like queries, inserting, deleting)
	
	entities package:
	all classes in package is description of entities of local database. 
	
dialogs package:

	ImageDialog
extending DialogFragment to provide full screen dialog when its possible(like in design view it used to display zoommable image in dialog)

	MyProgressDialog
created to build and display custom progress dialog(like dialogs when we getting data from server or uploading designs)

fragments package:

	AddConceptFragment
used to check and add new concepts to server. get preloaded concepts from database, compares new concept whith exicting by name, and if we haven't equals send it to server. Then shows dialog with succes or not message

	AddStudentFragment
get schools and classes from server, fills spinners and check edittext fields to be filled. If filled make request to server to put new student. Then show dialog with success or not message. 

	ApproveDesignFragment
in this fragment we recieve link to picture and needed to approve/reject data from recyclerview of ViewDesignFragment and displaying recyclerview with students who linked to selected design. Also will be displayed zoomable image(using ImageDialog) when we touch dialog image. When we pressing on Approove/Reject button we check comment field, and if it filled make request to server. Then show dialog with success or not message. If request successful, updating local database to provide actual information in ViewDesignFragment.
	
	CameraFragment
receiving data from SubmitDesignFragment. User can make photo or select from recents then sending URI of photo to modified library and automaticaly determine sheets of paper, or user can manually set borders of sheet and click next. Image was cutted and copied to /Inqui/Design folder. After that app displays prompt to make new image or upload design. If we select Yes, user can make another photo. This action can be many times, but time needed to cut and merge images every time was bigger and bigger. If user select No, Uri of file and other needed data sends to UploadDesignFragment.
	
	ClassInfoFragment
filling spinners from local database and when pressing button sending selections to ClassInfoListFragment
	
	ClassInfoListFragment
receiving selection from ClassInfoFragment and displaying recyclerview with students 
	
	ConceptChooserFragment
fill spinners from local database and send this data to FeedVariantsFragment
	
	DesignSelectorFragment
	
	FeedAudioFragment
receiving data from FeedVariantsFragment and records audio by holding red round button. If we touch "Audio recorder tap to listen", audio is playing and we can submit feedback or record new audio. By pressing Send Feedback button, audio record will be loaded to folder in dropbox and feedback was submitted to server. If we havent internet or it's gone when we sending feednback, it will be saved locally and submitted by NoConnectionService when internet appears. Also show dialog of success/failure. Audio temporary holds in /Inqui/Feedback/Audio folder on your phone. It will be deleted after all feedbacks was uploaded successfuly.

	FeedTextFragment
receiving data from FeedVariantsFragment and we can write message in edittext. By pressing Send Feedback button, audio record will be loaded to folder in dropbox like txt file and feedback was submitted to server. If we havent internet or it's gone when we sending feednback, it will be saved locally and submitted by NoConnectionService when internet appears. Also show dialog of success/failure. Text file temporary holds in /Inqui/Feedback/Text folder on your phone. It will be deleted after all feedback was uploaded successfuly.

	FeedVariantsFragment
receiving data from ConceptChooserFragment and send data and open FeedAudioFragment, FeedTextFragment, FeedVideoCaptureFragment when we selecting variant of feedback
	
	FeedVideoCaptureFragment
receiving data from FeedVariantsFragment and we record vide. Also we can change camera to frontal. By first pressing record button vdeo start capturing, by second - stops. If we need to record another video, just press record again and video will be overwritted. By pressing Send Feedback button, video record will be loaded to folder in dropbox and feedback was submitted to server. If we havent internet or it's gone when we sending feednback, it will be saved locally and submitted by NoConnectionService when internet appears. Also show dialog of success/failure. Video temporary holds in /Inqui/Feedback/Video folder o your phone. It will be deleted after all feedbacks was uploaded successfuly.

	HomePageFragment
home fragment. In this fragment we preloading information about schools, students, classes, designs and links to dropbox folder. Also we start preloading of images for ViewDesignFragment and StudentInfoListFragment to make it pussible to view design images in offline mode.	
	
	RequestChooser
in this fragment we fill spinners from local database, choosing approved concept and sending this data to RequestMaterialsFragment
	
	RequestMaterialsFragment
we receive data from RequestChooser, fill spinners from server and can choose needed material and quantity. If needed material not presented in spinner, we can write name of material to edittext and send it to server. After selecting needed materials and quantity we press button "Request" and app forming email message with school, class, concept and design and selected material and quantity. It will be sended by standart email application to bindupriya117@gmail.com.
to change email you need to change constant EMAIL in StaticConstants to needed.
	
	ReviewFragment
in review we receive checked in ViewDesignFragment designs and can select approve or reject and write comment to each design. when review button pressed app make request to server, and display message of success/failure. If success changes writed to loca databases to view actual information in ViewDesignFragment.
	
	StudentInfoFragment
fills spinners from local database and when button pressed send this data to StudentInfoListFragment
	
	StudentInfoListFragment
get data from StudentInfoFragment and display information about student and fills recyclerview with designs linked by student
	
	SubmitDesignFragment
fills spinners from local database. we need to fill design name edittext and then, when we pressed button send this data to CameraFragment to take picture from camera or select from recents.
	
	UploadDesignFragment
recieve data from CameraFragment, displaying preview and when user press button, send upload image to dropbox and submitting design to server. If we don't have internet, or its gone while submitting, design was saved to local database with unsynced flag, an when internet apperas will be uploaded. Images temporary holds in /Inqui/Design folder o your phone. It will be deleted after all design was uploaded successfuly.
	
	ViewDesignFragment
receive data about filters for design from DesignSelectorFragment and setup recyclerview with designs. Also we can filter lists of designs by approve/regect/submitted/all spinner. When we touch design, sending needed data to ApproveDesignFragment and open it. If we select one or more designs, we can press Submit selected button, and opens ReviewFragment.

model package:
	at this package we store description of all entities and responses what can be sended or received to/from server. 
	
spinners package:
	MultiSelectionSpinner
custom spinner for selecting multiple students in UploadDesignFragment

	SpinnerAdapter
custom spinner that used in all spinners over app

utils package:
	MyApplication 
app with all libraries have more than 65k methods, and we need to declare multidex support
	
	StaticConstants
we store all constants of application
	
onlineStorage package:
	
	Dropbox
class needed to interact with Dropbox api. 
to access to dropbox account we use token in StaticConstants:
	
	public static final String ACCESS_TOKEN = "AgdDrXL1B_QAAAAAAABUfMxMe3XVKG2vVz4w2qrBZXXeF44oRnrLPfn7IibUlauj";

	if you want to use another account, you need to make preparations in Dropbox developers console, get another token from console and change existing. 

	first of all we need to configure Dropbox client

public void init() throws DbxException {

        // Create Dropbox client
        DbxRequestConfig config = new DbxRequestConfig("Inqui", "en_US");
        if (client == null)
            client = new DbxClientV2(config, ACCESS_TOKEN);
    }
	
then we selecting what we want to do in switch:

case DESIGN:
                return uploadDesign(strings[0]);
            case FEED_TXT:
                return uploadTxt(strings[0]);
            case FEED_VIDEO:
                return uploadVideo(strings[0]);
            case FEED_AUDIO:
                return uploadAudio(strings[0]);
				
then we do uploading of needed files, for example, audio:

 private String uploadAudio(String string) {
        try {  
			//creating inputstream from file
            InputStream in = new FileInputStream(new File(string));
			//defining path in Dropbox folder
            String path = "Design/" + getFileName(string);
            {
				//uploading data to dropbox. if success, we getting metadata of file an send it to log to debug if needed
                FileMetadata metadata = client.files().uploadBuilder("/Feedback/Audio/" + getFileName(string))
                        .uploadAndFinish(in);
                Log.e("metadata", metadata.getName());
				//return path to put it in interface for future needs
                return path;
            }
        } catch (DbxException | IOException e) {
            e.printStackTrace();
			//if something get wrong, return null and check whenever we need in future
            return null;
        }
    }

	DropboxAsyncResponse
interface that provide path if uploud success or null if not
	
recyclers package:
in this package we have descriptions ov entities for recyclerview(like designs, students, materials) and adapters and viewholders to work with it. It's ui part and all based on standart patterns of recyclerview usage. I make description only gor DesignAdapter like most complexity element.
	
	DesignsAdapter
first of all we need to determine in what case used adapter:
 
 case 0:
				//if 0 - minimal usage(advanced controls and checkbox is invisible). we use it in StudentInfoListFragment
                holder.checkBox.setVisibility(View.INVISIBLE);
                holder.advanced.setVisibility(View.GONE);
                break;
            case 1:
				
				//if 0 - checkBox is visible - we use it in ViewDesignFragment
                holder.advanced.setVisibility(View.GONE);
				//if item has status approved or rejected we also make checkBox invisible(we dont need to make selection //of reviewed design)
                if (!item.getStatus().equals("Submitted"))
                    holder.checkBox.setVisibility(View.INVISIBLE);
                else holder.checkBox.setVisibility(View.VISIBLE);
                //setting action on checkBox checked status changed
				holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        designsList.get(holder.getAdapterPosition()).setCheckedStatus(b);
                    }
                });
                break;
            case 2:
				
				//if 2 - most complexity usage of adapter, all elements are visible
                holder.comment.setTag(position);
                holder.comment.setText(item.getDesignComments());
				//set textchangelistener to edittext to see when user write comment to design
                holder.comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        designsList.get(holder.getAdapterPosition()).setDesignComments(editable.toString());
                    }
                });
                holder.approveRadio.setTag(position);
                holder.approveRadio.setChecked(item.isCheckedStatus());
				
				//set onCheckedChangedListener to see when user select approve or reject
                holder.approveRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked())
                            designsList.get(holder.getAdapterPosition()).setReviewStatus(true);
                    }
                });
                holder.rejectRadio.setTag(position);
                holder.rejectRadio.setChecked(item.isCheckedStatus());
				//set onCheckedChangedListener to see when user select approve or reject
                holder.rejectRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked())
                            designsList.get(holder.getAdapterPosition()).setReviewStatus(false);
                    }
                });
                holder.checkBox.setChecked(item.isCheckedStatus());
				//set onCheckedChangedListener to checkbox. it's made for case when user select multiplw designs and don't //want to review some of them
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        designsList.get(holder.getAdapterPosition()).setCheckedStatus(b);
                    }
                });
                break;
				
then we need to set Approve/Reject image to design:
		 holder.designName.setText(item.getDesignName());
				switch (item.getStatus()) {
					case "Approve":
						Picasso.with(context).load(R.drawable.approved).resize(300, 300).centerInside().into(holder.approve);
						break;
					case "Reject":
						Picasso.with(context).load(R.drawable.rejected).resize(300, 300).centerInside().into(holder.approve);
						break;
					default:
						Picasso.with(context).load(R.drawable.circle).resize(300, 300).centerInside().into(holder.approve);
						break;
				}
then we getting links to images on dropbox from local database and set images to imageview(if image cached it loaded from cahche, if not - loaded from link):

try {
            final String link = getLink(item.getDesignId());
            if (link != null) {
                Picasso.with(context).load(link).resize(300, 300).centerCrop().into(holder.design);
                Log.e("link adapter", link);
            } else
                Picasso.with(context).load(R.drawable.image_not_available_300).resize(300, 300).into(holder.design);
         // ------------------------
        } catch (Exception e) {
            e.printStackTrace();
        }

services package:
	
	NoConnectionService
in NoConnectionService we have registered broadcast reciever that register network changes(like wi-fi on or gprs on)
when this happens, we call asynctask to get unsynced designs/feedbacks and upload it to dropbox and server

            @Override
            public void onReceive(Context context, Intent intent) {
                final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetInfo != null) {
                    if (firstConnect) {
                        if (NetworkManager.isConnected(mContext)) {
                            Log.i("APP_TAG", "Mobile - CONNECTED");
                            upload = new Upload(getApplicationContext());
                            initDropbox();
                            upload.execute();
                        }
                        firstConnect = false;
                    }
                } else {
                    firstConnect = true;
                }
            }
        };
		
upload method:
		 private void uploadFeedback() {
					//get all unsynced feedbacks
					List<Feedback> unsynced = AppDatabase.getInstance(mContext).getFeedbackDao().getUnsynced();
					for (Feedback feedback : unsynced) {
						try {
							//creating stream of data
							InputStream in = new FileInputStream(feedback.getFeedback_path());
							{	//uploading feedback files to dropbox
								FileMetadata metadata = client.files().uploadBuilder("/Feedback/" + feedback.getFeedback_type() + "/" + getFileName(feedback.getFeedback_path()))
										.uploadAndFinish(in);
								//deleting files from feedback folder
								deleteFiles(feedback.getFeedback_path());
								//submitting feedback to server
								submitFeedback(feedback);
								Log.e("metadata", metadata.getName());
							}
						} catch (DbxException | IOException e) {
							e.printStackTrace();
						}
					}
				}
				
then we need to delete uploaded feedback from database 				
			private void updateUnsyncedFeedback(final Feedback feedback) {
            new Thread(new Runnable() {
                public void run() {
                    AppDatabase.getInstance(mContext).getFeedbackDao().deleteFeedback(feedback);
                    editor = sharedPreferences.edit();
                    editor.putBoolean("sync", false);
                    editor.apply();
                }
            }).start();

        }
		
method to sumbit feedback to server:
	
	 public void submitFeedback(final Feedback feedback) {
				//generating body of put method
				FeedbackBody body = new FeedbackBody();
				body.setStudentId(feedback.getStudent_id());
				body.setFeedBackType(feedback.getFeedback_type());
				body.setFeedBackPath(feedback.getFeedback_path());
				Log.e("feedback path", feedback.getFeedback_path());
				body.setDesignId("");
				body.setConceptId(feedback.getConcept_id());
				
				//submitting feedback 
				HttpHelper.getApi().submitFeedback(body).enqueue(new Callback<Response>() {
					@Override
					public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
						//if response was success, update database and make notification
						if (response.isSuccessful()) {
							if ((response.body()).getMessage().contains("Successfully")) {
								updateUnsyncedFeedback(feedback);
							}
							notifySuccess();
						}
					}

					@Override
					public void onFailure(Call<Response> call, Throwable t) {

					}
				});
			}
			
notification method:

 private void notifySuccess() {
		//check api version, if API>=26, we need to use another builder to crate notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "my_channel_01";
            CharSequence name = "Inqui";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            Notification.Builder builder = new Notification.Builder(this, "")
                    .setContentTitle("Inqui")
                    .setContentText("Uploading successful")
                    .setChannelId("my_channel_01")
                    .setSmallIcon(R.drawable.ic_upload);
            Notification notification = builder.build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            notificationManager.notify(1, notification);
        } else {
			//for API<26
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Uploading successful")
                    .setSmallIcon(R.drawable.ic_upload)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            Notification notification = builder.build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(1, notification);
        }
    }

	GoogleDrive

Class needed for uploading data to Google Drive.
To get access we use GoogleCredential class that should contain or account data.
First we should init it:
            public void initGoogleDrive(Context context) {
                    try {
                        googleCredential = GoogleCredential.fromStream(context.getResources().openRawResource(R.raw.inquilab2b9e83128b90)).createScoped(Arrays.asList(SCOPES));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    HttpTransport transport = AndroidHttp.newCompatibleTransport();
                    JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
                    mService = new com.google.api.services.drive.Drive.Builder(
                            transport, jsonFactory, googleCredential)
                            .setApplicationName("InquilabServiceAccount")
                            .build();
                }
	Our googleCredential we read from JSON and then we init our connection using variable Drive mService;

	For uploading we use one of the methods:
                         switch (strings[1]) {
                                        case DESIGN:
                                            return uploadDesign(strings[0]);
                                        case FEED_TXT:
                                            return uploadTxt(strings[0]);
                                        case FEED_VIDEO:
                                            return uploadVideo(strings[0]);
                                        case FEED_AUDIO:
                                            return uploadAudio(strings[0]);
                                        case DESIGN_SERVICE:
                                            return uploadDesignAfterOffline(strings[0]);
                                        case FEED_AUDIO_AFTEROFFLINE:
                                            return uploadAudioAfterOffline(strings[0]);
                                        case FEED_VIDEO_AFTEROFFLINE:
                                            return uploadVideoAfterOffline(strings[0]);
                                        case FEED_TXT_AFTEROFFLINE:
                                            return uploadTxtAfterOffline(strings[0]);
                                        default:
                                            return null;
                                    }
     First 4 we use when we online, next 4 when we send after being offline.

     Each method similar to each other, there difference only in folder we want to save to.
     For example:
                  private String uploadDesign(String string) throws Exception {
                         String path = "Design/" + getFileName(string);

                         String folderId = checkIsFolderExists();

                         String mimeType = URLConnection.guessContentTypeFromName(string);
                         File body = new File();
                         body.setParents(Arrays.asList(folderId));
                         body.setName(getFileName(string));
                         //body.setParents(Arrays.asList(("Designs")));
                         body.setMimeType(mimeType);

                         java.io.File fileContent = new java.io.File(string.substring(7));//path to file
                         FileContent mediaContent = new FileContent(mimeType, fileContent);

                         File file = mService.files().create(body, mediaContent).execute();

                         // Uncomment the following line to print the File ID.
                          System.out.println("File ID: " + file.getId());

                         return path;
                     }

      First we get our folder ID:  String folderId = checkIsFolderExists();
                                  private String checkIsFolderExists() throws Exception{
                                          FileList result = mService.files().list()
                                                  .setQ("mimeType = 'application/vnd.google-apps.folder'")
                                                  .setFields("files(id, name)")
                                                  .execute();

                                          List<File> files = result.getFiles();
                                          if (files != null) {
                                              for (File file : files) {
                                                  if(file.getName().equals("Design")) {
                                                      return file.getId();
                                                  }
                                              }
                                          }

                                          File fileMetadata = new File();
                                          fileMetadata.setName("Design");
                                          fileMetadata.setMimeType("application/vnd.google-apps.folder");

                                          File file = mService.files().create(fileMetadata)
                                                  .setFields("name")
                                                  .execute();
                                          System.out.println("Folder " + file.getName() + " created!");
                                          return file.getId();
                                      }
      If it donesn't exist we create it and use it's id.

      Then we create our file:
                   String mimeType = URLConnection.guessContentTypeFromName(string);
                   File body = new File();
                   body.setParents(Arrays.asList(folderId));
                   body.setName(getFileName(string));
                   //body.setParents(Arrays.asList(("Designs")));
                   body.setMimeType(mimeType);

                   java.io.File fileContent = new java.io.File(string.substring(7));//path to file
                   FileContent mediaContent = new FileContent(mimeType, fileContent);
      And send it to google drive:
                   File file = mService.files().create(body, mediaContent).execute();

      To get file from Drive we use next code:
                                googleService.files().list()
                                          .setQ("name contains '" + designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1) + "'")
                                          .setFields("nextPageToken, files(id, name, parents)")
                                          .execute();
      googleService.files().list(): get all files from drive, but next to this code we use conditions:
                  .setQ("name contains '" + designs1.getDesign_image().substring(designs1.getDesign_image().lastIndexOf('/') + 1) + "'")
                  .setFields("nextPageToken, files(id, name, parents)")
      setFields tell our Drive what fields we want to get; There we get only file's id, name and folder.

      To get file we use next code:
                              final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                              googleService.files().get(dropLink)
                                      //.setQ("name contains '" + title + "'")
                                      .executeMediaAndDownloadTo(outputStream);
                              byte[] bitmapdata = outputStream.toByteArray();
                              final File file = File.createTempFile("temp", "jpeg");
                              Files.write(bitmapdata, file);
      First we create ByteArrayOutputStream outputStream where we'll save the file and then execute request.
      When we got data we convert it to byte array. byte[] bitmapdata = outputStream.toByteArray();
      And then we create temp file to save our byte array.
                final File file = File.createTempFile("temp", "jpeg");
                Files.write(bitmapdata, file);


    Creating JSON file with account data.

    To generate service-account credentials, or to view the public credentials that you've already generated, do the following:
    1. Open https://console.developers.google.com/permissions/serviceaccounts. If prompted, select a project.
    2. Click Create service account.
    3. In the Create service account window, type a name for the service account, and select Furnish a new private key. If you want to grant G Suite domain-wide authority to the service account, also select Enable G Suite Domain-wide Delegation. Then click Create.
    Your new public/private key pair is generated and downloaded to your machine; it serves as the only copy of this key. You are responsible for storing it securely.

    You can return to the API Console at any time to view the email address, public key fingerprints, and other information, or to generate additional public/private key pairs.
    For more details about service account credentials in the API Console, see Service accounts in the API Console help file.

